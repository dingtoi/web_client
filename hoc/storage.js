import React from "react";
import UserModel from "../models/user";
import {SERVER_ANONYMOUS} from "../config";

const withStorage = (WrappedComponent) => {
    class HOC extends React.Component{
        constructor(props){
            super(props);
            this.state = {
                auth: '',
                anonymous: false
            }
        }
        componentDidMount(){
            const email = localStorage.getItem('email') ? localStorage.getItem('email') : null;
            const auth = email ? email : null;
            if(!auth){
                //$.getJSON(SERVER_ANONYMOUS, (data) => {
                    //const ip = data.ip;
                    const ip = 123457;
                    UserModel.loginAnonymous({ip})
                    .then((result) => {
                        localStorage.setItem('email', ip);
                        localStorage.setItem('anonymous', true);
                        setTimeout(() => {
                            this.setState({auth: ip, anonymous: true});
                        }, 500);
                    })
                    .catch(() => {

                    })
                //});
            }else{
                if(is.email(auth))
                    this.setState({auth: auth, anonymous: false});
                else
                    this.setState({auth: auth, anonymous: true});
            }
        }
        render(){
            const props = {
                ...this.props,
                auth: this.state.auth,
                anonymous: this.state.anonymous
            }
            if(this.state.auth)
                return <WrappedComponent {...props} ref={instance => this.wrappedComponentRef = instance}/>
            else
                return null
        }
    }
    return HOC;
}

export default withStorage;