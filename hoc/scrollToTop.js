import React from "react";
import {withRouter} from "react-router-dom";
import withStorage from "../hoc/storage";

class ScrollToTop extends React.Component {
    componentDidMount(){
        this.handlerRouter();
    }
    componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location)
            window.scrollTo(0, 0);
        this.handlerRouter();
    }
    handlerRouter(){
        /*const pathName = this.props.location.pathname;
        if(!this.props.auth || (this.props.auth && this.props.anonymous)){
            if(pathName.indexOf('account') > -1 || pathName.indexOf('myaccount') > -1)
                this.props.history.push('/login');
        }else if(this.props.auth && !this.props.anonymous){
            if(pathName.indexOf('login') > -1)
                this.props.history.push('/');
        }*/
    }
    render() {
        return this.props.children;
    }
}

export default withRouter(withStorage(ScrollToTop));