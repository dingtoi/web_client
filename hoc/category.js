import React from "react";
import DeviceModel from "../models/device";

const withCategory = (WrappedComponent) => {
    class HOC extends React.Component{
        constructor(props){
            super(props);
            this.filter = {
                type: [],
                category: [],
                brand: [],
                color: [],
                capacity: [],
                ram: [],
                orderBy: 1
            }
            this.state = {
                categoryList: [],
                brandList: [],
                colorList: [],
                capacityList: [],
                ramList: []
            }
        }
        componentDidMount(){
            DeviceModel.getCategoryList()
            .then((result) => {
                this.setState({categoryList: result});
            })
            DeviceModel.getBrandList()
            .then((result) => {
                this.setState({brandList: result});
            })
            DeviceModel.getColorList()
            .then((result) => {
                this.setState({colorList: result});
            })
            DeviceModel.getCapacityList()
            .then((result) => {
                this.setState({capacityList: result});
            })
            DeviceModel.getRamList()
            .then((result) => {
                this.setState({ramList: result});
            })
        }

        render(){
            const typeList = [
                {id: 1, name: 'Sale'},
                {id: 2, name: 'Exchange'}
            ]
            const filterMatchList = [
                {id: 1, name: 'Best Match'},
                {id: 2, name: 'Title: A->Z'},
                {id: 3, name: 'Title: Z->A'}
            ]
            const props = {
                typeList: typeList,
                categoryList: this.state.categoryList,
                brandList: this.state.brandList,
                colorList: this.state.colorList,
                capacityList: this.state.capacityList,
                ramList: this.state.ramList,
                filterMatchList: filterMatchList,
                filter: this.filter,
                ...this.props
            }

            if(this.state.categoryList.length > 0 && this.state.brandList.length && this.state.colorList.length > 0
                && this.state.capacityList.length > 0 && this.state.ramList.length > 0)
                return <WrappedComponent {...props} ref={instance => this.wrappedComponentRef = instance}/>;
            else
                return null;
        }
    }
    return HOC;
}

export default withCategory;