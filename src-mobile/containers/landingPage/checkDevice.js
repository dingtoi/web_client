import React from "react";
import {withRouter} from "react-router-dom";
import {Cover, Spinner2} from "../../../components/common";
import {SERVER_IMG} from "../../../config";

class CheckDevice extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: false
        }
        this.handlerMessage = this.handlerMessage.bind(this);
    }
    componentDidMount(){
        $('#uk-main-app').css({display: 'none'});
        //window.addEventListener("message", this.handlerMessage);
        //document.addEventListener("message", this.handlerMessage);
    }
    componentWillUnmount(){
        //window.addEventListener("message", this.handlerMessage);
        //document.removeEventListener("message", this.handlerMessage);
    }
    handlerCheck(){
        this.onBridgeReady(() => {
            alert('troi oi roi');
            console.log('asasas', window.postMessage);
            window.postMessage('chet tiet troi oi', '*');
        })
    }
    onBridgeReady(cb){
        if (window.postMessage.length !== 1) {
            setTimeout(() => {
              onBridgeReady(cb)
            }, 100);
        }else{
            cb();
        }
    }
    handlerMessage(data){
        alert('vo roi ne');
    }
    render(){
        return(
            <div>
                <div className="uk-inline uk-width-1-1">
                    {this.state.isLoading && <Cover/>}
                    {this.state.isLoading && <Spinner2/>}
                        <div data-uk-height-viewport className="uk-background-blend-multiply uk-background-cover uk-padding-large uk-text-center uk-landingpage uk-flex uk-flex-column uk-flex-center" style={{backgroundImage: 'url('+SERVER_IMG+'check-device-bg.jpg)'}}>
                        <h1 className="uk-text-white uk-text-title-landingpage uk-margin-remove-top">Swap-ez</h1>
                        <h2 className="uk-text-subtitle-landingpage uk-margin-top">Thank you for downloading our Check-Ez App!</h2>
                        <button type="button" className="uk-button uk-button-secondary uk-button-large uk-text-button-landingpage uk-margin-top" onClick={this.handlerCheck.bind(this)}>Check your device</button>
                    </div>
                </div>
            </div>
        )
    }
}
export default withRouter(CheckDevice);