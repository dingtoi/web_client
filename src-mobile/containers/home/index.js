import React from "react";

import ListType from "../../../components/mobile/listType";
import ListBrand from "../../../components/mobile/listBrand";
import ListFeatured from "../../../components/mobile/listFeatured";
import ListPurpose from "../../../components/mobile/purpose";
import {SERVER_IMG} from "../../../config";

class Home extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isShowModal: false
        }
    }
    render(){
        return (
            <div>
                <div className="slider">
                    <div className="uk-position-relative uk-visible-toggle uk-light" uk-slideshow="animation: scale;autoplay:true">
                        <ul className="uk-slideshow-items">
                            <li>
                                <img src={SERVER_IMG+"mobile1.jpg"} alt="" uk-cover="true"/>
                            </li>
                            <li>
                                <img src={SERVER_IMG+"mobile2.jpg"} alt="" uk-cover="true"/>
                            </li>
                            <li>
                                <img src={SERVER_IMG+"mobile3.jpg"} alt="" uk-cover="true"/>
                            </li>
                        </ul>
                    </div>
                </div>
                <div>
                    <ListType/>
                </div>
                <div>
                    <ListBrand/>
                </div>
                <div>
                    <ListFeatured/>
                </div>
                <div>
                    <ListPurpose/>
                </div>
            </div>
        )
    }
}

export default Home;