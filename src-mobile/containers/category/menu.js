import React from "react";

import {Select2, ModalDefault} from "../../../components/common";

import Filter from "./filter";

class CategoryMenu extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            filter: {},
            isModalMenu: false
        }
    }
    componentDidMount(){
        this.setState({filter: this.props.filter}, () => {
        });
        this.selectRef.refreshList(this.props.filterMatchList);
        setTimeout(() => {
            if(is.existy(this.selectRef))
                this.selectRef.triggerSelect(1);
        }, 500)
    }
    render(){
        return (
            <div>
                <ModalDefault isShow={this.state.isModalMenu}
                    onHidden={() => this.setState({isModalMenu: false})}>
                    <Filter {...this.props}
                        onTriggerList={filter => {
                            this.setState({isModalMenu: false, filter: filter}, () => {
                                this.props.onTriggerList(filter);
                            })
                        }}
                    />
                </ModalDefault>
                <div className="uk-box-shadow-medium uk-text-uppercase uk-height-small uk-flex uk-flex-middle uk-margin-bottom">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Category List</div>
                </div>
                <div className="uk-container">
                    <div className="uk-flex uk-flex-between uk-flex-middle">
                        <div>
                            <a className="uk-link-reset"
                                onClick={() => this.setState({isModalMenu: true})}>
                                <i className="fa fa-filter"/> Filter
                            </a>
                        </div>
                        <div className="uk-width-small">
                            <Select2 ref={instance => this.selectRef = instance} noClear
                                onChange={(value) => {
                                    this.state.filter.orderBy = value.id;
                                    this.props.onTriggerList(this.state.filter);
                                }}/>
                        </div>
                    </div>
                </div>
                <div className="uk-container">
                    <div className="uk-grid uk-grid-medium" uk-grid="true">
                        <div className="uk-width-1-1">
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CategoryMenu;