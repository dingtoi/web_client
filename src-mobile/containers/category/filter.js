import React from "react";

import {Checkbox} from "../../../components/common";

class Filter extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            filter: {},
        }
        this.handlerCheck = this.handlerCheck.bind(this);
    }
    componentDidMount(){
        this.setState({filter: this.props.filter}, () => {
            this.triggerInit(this.props.queryString);
        });
    }
    triggerInit(model){
        if(is.existy(model.brand))
            this.handlerCheckNotReact('brand', model.brand);
        if(is.existy(model.device))
            this.handlerCheckNotReact('category', model.device);
        if(is.existy(model.type))
            this.handlerCheckNotReact('type', model.type);
    }
    handlerCheckNotReact(name, value){
        for(let i = 0; i < this.props[name+'List'].length; i++){
            if(this[name+this.props[name+'List'][i].id]){
                if(value == this.props[name+'List'][i].id){
                    this[name+this.props[name+'List'][i].id].setSelected();
                    break;
                }
            }
        }
        this.state.filter[name] = [Number(value)];
    }
    handlerCheck(name, target){
        const {checked, value} = target
        if(checked)
            this.setState(
                (prevState) => {
                    prevState.filter[name].push(Number(value));
                    this.props.onTriggerList(prevState.filter);
                    return {filter: prevState.filter};
                }
            );
        else{
            const index = this.state.filter[name].indexOf(Number(value));
            if(index > -1){
                this.setState(
                    (prevState) => {
                        prevState.filter[name].splice(index, 1);
                        this.props.onTriggerList(prevState.filter);
                        return {filter: prevState.filter};
                    }
                );
            }
        }
    }
    render(){
        return (
            <div className="uk-container uk-margin-xlarge-top uk-margin-large-bottom">
                <ul uk-accordion="multiple: true" aria-hidden="false">
                    <li className="uk-open">
                        <a className="uk-accordion-title uk-text-small uk-text-bold">Type</a>
                        <div className="uk-accordion-content">
                            <ul className="uk-list">
                                {
                                    this.props.typeList.map((type, key) => {
                                        return (
                                            <li key={key}>
                                                <Checkbox value={type.id} name="type" label={type.name} fontSize="12px"
                                                    onChange={(event) => this.handlerCheck('type', event.target)}
                                                    ref={instance => this['type'+type.id] = instance}/>
                                            </li>
                                        )
                                    })
                                }
                            </ul>
                        </div>
                    </li>
                    <li className="uk-open">
                        <a className="uk-accordion-title uk-text-small uk-text-bold">Device</a>
                        <div className="uk-accordion-content">
                            <ul className="uk-list">
                            {
                                this.props.categoryList.map((category, key) => {
                                    return (
                                        <li key={key}>
                                            <Checkbox value={category.id} name="category" label={category.name} fontSize="12px"
                                                onChange={(event) => this.handlerCheck('category', event.target)}
                                                ref={instance => this['category'+category.id] = instance}/>
                                        </li>
                                    )
                                })
                            }
                            </ul>
                        </div>
                    </li>
                    <li className="uk-open">
                        <a className="uk-accordion-title uk-text-small uk-text-bold">Brand</a>
                        <div className="uk-accordion-content">
                            <ul className="uk-list">
                            {
                                this.props.brandList.map((brand, key) => {
                                    return (
                                        <li key={key}>
                                            <Checkbox value={brand.id} name="brand" label={brand.name} fontSize="12px"
                                                onChange={(event) => this.handlerCheck('brand', event.target)}
                                                ref={instance => this['brand'+brand.id] = instance}/>
                                        </li>
                                    )
                                })
                            }
                            </ul>
                        </div>
                    </li>
                    <li className="uk-open">
                        <a className="uk-accordion-title uk-text-small uk-text-bold">Color</a>
                        <div className="uk-accordion-content">
                            <ul className="uk-list">
                            {
                                this.props.colorList.map((color, key) => {
                                    return (
                                        <li key={key}>
                                            <Checkbox value={color.id} name="color" label={color.name} fontSize="12px"
                                                onChange={(event) => this.handlerCheck('color', event.target)}
                                                ref={instance => this['color'+color.id] = instance}/>
                                        </li>
                                    )
                                })
                            }
                            </ul>
                        </div>
                    </li>
                    <li className="uk-open">
                        <a className="uk-accordion-title uk-text-small uk-text-bold">Capacity</a>
                        <div className="uk-accordion-content">
                            <ul className="uk-list">
                            {
                                this.props.capacityList.map((capacity, key) => {
                                    return (
                                        <li key={key}>
                                            <Checkbox value={capacity.id} name="capacity" label={capacity.name} fontSize="12px"
                                                onChange={(event) => this.handlerCheck('capacity', event.target)}
                                                ref={instance => this['capacity'+capacity.id] = instance}/>
                                        </li>
                                    )
                                })
                            }
                            </ul>
                        </div>
                    </li>
                    <li className="uk-open">
                        <a className="uk-accordion-title uk-text-small uk-text-bold">Ram</a>
                        <div className="uk-accordion-content">
                            <ul className="uk-list">
                            {
                                this.props.ramList.map((ram, key) => {
                                    return (
                                        <li key={key}>
                                            <Checkbox value={ram.id} name="ram" label={ram.name} fontSize="12px"
                                                onChange={(event) => this.handlerCheck('ram', event.target)}
                                                ref={instance => this['ram'+ram.id] = instance}/>
                                        </li>
                                    )
                                })
                            }
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        )
    }
}

export default Filter;