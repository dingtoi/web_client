import React from "react";

class SaleRetund extends React.Component{
    render(){
        return(
            <div>
                <div className="uk-container uk-padding">
                    <h3>Sale and Retunds</h3>
                    <p>Thanks for shopping at Swap-ez. We appreciate the fact that you like to buy the cool stuff we build. We also want to make sure you have a rewarding experience while you’re exploring, evaluating, and purchasing our products, whether you’re at the Swap-ez Online Store, in an Swap-ez Retail Store, or on the phone with the Swap-ez Contact Center. (To make it visually easier on both of us, we’ll refer to these entities as the “Swap-ez Store” in this policy.)</p>

                    <h3>Standard Return Policy</h3>
                    <p>We fundamentally believe you will be thrilled with the products you purchase from the Swap-ez Store. That’s because we go out of our way to ensure that they’re designed and built to be just what you need. We understand, however, that sometimes a product may not be what you expected it to be. In that unlikely event, we invite you to review the following terms related to returning a product.</p>
                    <ul>
                        <li>Products can be returned only in the country or region in which they were originally purchased.</li>
                        <li>The following products are not eligible for return: electronic software downloads, subscriptions to the Software-Up-To-Date program, Swap-ez Store Gift Cards, and any Swap-ez Developer Connection products.</li>
                        <li>For returns to an Swap-ez Retail Store for cash, cash equivalent, and check transactions over $750, Swap-ez will mail a refund check to you within 10 business days.</li>
                        <li>Should you wish to return ten or more of the same product, you must return to the Swap-ez Store where originally purchased.</li>
                        <li>In the case of items returned with a gift receipt, Swap-ez will offer you an Swap-ez Gift Card.</li>
                        <li>Opened software cannot be returned if it contained a seal with the software license on the outside of the package and you could read the software license before opening its packaging. As an exception, you may return Swap-ez-branded software if you do not agree to the licensing terms; however, you may not retain or otherwise use any copies of returned software.</li>
                        <li>Swap-ez provides security features to enable you to protect your product in case of loss or theft. &nbsp;If these features have been activated and cannot be disabled by the person in possession of the&nbsp;phone, Swap-ez may refuse the return or exchange.</li>
                    </ul>
                </div>
            </div>
        )
    }
}
export default SaleRetund;