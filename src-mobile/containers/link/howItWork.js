import React from "react";
import {SERVER_IMG} from "../../../config";

class HowItWork extends React.Component{
    constructor(props){
        super(props);
    }
   
    render(){
        return (
            <div>
                <div className="uk-container uk-padding">
                    <h3>How it work</h3>
                    <div className="uk-grid uk-grid-margin uk-grid-match" uk-grid="true">
                        <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-3@l">
                            <div className="uk-text-center uk-card uk-card-default uk-card-body uk-cursor">
                                <h2>Buying Support</h2>
                                <i className="fa fa-life-ring fa-5x" aria-hidden="true"></i>
                                <div className="uk-margin-top">
                                If you need help before, during or after your purchase, this is the place to be.                  
                                </div>
                            </div>
                        </div>
                        <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-3@l">
                            <div className="uk-text-center uk-card uk-card-default uk-card-body uk-cursor">
                                <h2>Licensing</h2>
                                <i className="fa fa-sticky-note-o fa-5x" aria-hidden="true"></i>
                                <div className="uk-margin-top">
                                Have a question about licensing? Check out our frequently asked questions to find your answer.                
                                </div>
                            </div>
                        </div>
                        <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-3@l">
                            <div className="uk-text-center uk-card uk-card-default uk-card-body uk-cursor">
                                <h2>Your Account</h2>
                                <i className="fa fa-user-circle-o fa-5x" aria-hidden="true"></i>
                                <div className="uk-margin-top">
                                Set up your account and keep it safe and sound.                
                                </div>
                            </div>
                        </div>
                        <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@l">
                            <div className="uk-text-center uk-card uk-card-default uk-card-body uk-cursor">
                                <h2>Copyright and Trademarks</h2>
                                <i className="fa fa-copyright fa-5x" aria-hidden="true"></i>
                                <div className="uk-margin-top">
                                Find out about Copyright and Trademark protection, what Intellectual Property is, and why it is important to you.             
                                </div>
                            </div>
                        </div>
                        <div className="uk-width-1-1@s uk-width-1-1@m uk-width-1-2@l">
                            <div className="uk-text-center uk-card uk-card-default uk-card-body uk-cursor">
                                <h2>Tax & Compliance</h2>
                                <i className="fa fa-calculator fa-5x" aria-hidden="true"></i>
                                <div className="uk-margin-top">
                                Information to help you understand tax and compliance on Envato Market.               
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div className="uk-padding uk-padding-remove-left uk-padding-remove-right">
                    <h1>Getting started</h1>
                    <div className="uk-grid uk-grid-margin" uk-grid="true">
                        <div className="uk-width-1-2@m uk-flex uk-flex-middle">
                            <i className="fa fa-question-circle fa-2x uk-margin-xsmall-right"></i>
                            <a className="uk-text-xlarge uk-link-reset border-breadcrumb uk-padding-small uk-padding-remove-left uk-width-1-1">How do I purchase an item?</a>
                        </div>
                        <div className="uk-width-1-2@m uk-flex uk-flex-middle">
                            <i className="fa fa-question-circle fa-2x uk-margin-xsmall-right"></i>
                            <a className="uk-text-xlarge uk-link-reset border-breadcrumb uk-padding-small uk-padding-remove-left uk-width-1-1">How to Download your items?</a>
                        </div>
                        <div className="uk-width-1-2@m uk-flex uk-flex-middle">
                            <i className="fa fa-question-circle fa-2x uk-margin-xsmall-right"></i>
                            <a className="uk-text-xlarge uk-link-reset border-breadcrumb uk-padding-small uk-padding-remove-left uk-width-1-1">View and Download invoices?</a>
                        </div>
                        <div className="uk-width-1-2@m uk-flex uk-flex-middle">
                            <i className="fa fa-question-circle fa-2x uk-margin-xsmall-right"></i>
                            <a className="uk-text-xlarge uk-link-reset border-breadcrumb uk-padding-small uk-padding-remove-left uk-width-1-1">Licenses Overview?</a>
                        </div>
                        <div className="uk-width-1-2@m uk-flex uk-flex-middle">
                            <i className="fa fa-question-circle fa-2x uk-margin-xsmall-right"></i>
                            <a className="uk-text-xlarge uk-link-reset border-breadcrumb uk-padding-small uk-padding-remove-left uk-width-1-1">Do I need a Regular License or an Extended License?</a>
                        </div>
                        <div className="uk-width-1-2@m uk-flex uk-flex-middle">
                            <i className="fa fa-question-circle fa-2x uk-margin-xsmall-right"></i>
                            <a className="uk-text-xlarge uk-link-reset border-breadcrumb uk-padding-small uk-padding-remove-left uk-width-1-1">How Does The Envato Market Affiliate Program Work?</a>
                        </div>
                    </div>
                        <h1 >Popular articles</h1>
                        <div className="uk-grid uk-grid-margin" uk-grid="true">
                            <div className="uk-width-1-2@m uk-flex uk-flex-middle">
                                <i className="fa fa-question-circle fa-2x uk-margin-xsmall-right"></i>
                                <a className="uk-text-xlarge uk-link-reset border-breadcrumb uk-padding-small uk-padding-remove-left uk-width-1-1">Where Is My Purchase Code?</a>
                            </div>
                            <div className="uk-width-1-2@m uk-flex uk-flex-middle">
                                <i className="fa fa-question-circle fa-2x uk-margin-xsmall-right"></i>
                                <a className="uk-text-xlarge uk-link-reset border-breadcrumb uk-padding-small uk-padding-remove-left uk-width-1-1">What is Item Support?</a>
                            </div>
                            <div className="uk-width-1-2@m uk-flex uk-flex-middle">
                                <i className="fa fa-question-circle fa-2x uk-margin-xsmall-right"></i>
                                <a className="uk-text-xlarge uk-link-reset border-breadcrumb uk-padding-small uk-padding-remove-left uk-width-1-1">Bundled Plugins</a>
                            </div>
                            <div className="uk-width-1-2@m uk-flex uk-flex-middle">
                                <i className="fa fa-question-circle fa-2x uk-margin-xsmall-right"></i>
                                <a className="uk-text-xlarge uk-link-reset border-breadcrumb uk-padding-small uk-padding-remove-left uk-width-1-1">Theme is missing the style.css stylesheet error</a>
                            </div>
                            <div className="uk-width-1-2@m uk-flex uk-flex-middle">
                                <i className="fa fa-question-circle fa-2x uk-margin-xsmall-right"></i>
                                <a className="uk-text-xlarge uk-link-reset border-breadcrumb uk-padding-small uk-padding-remove-left uk-width-1-1">How to contact an author?</a>
                            </div>
                            <div className="uk-width-1-2@m uk-flex uk-flex-middle">
                                <i className="fa fa-question-circle fa-2x uk-margin-xsmall-right"></i>
                                <a className="uk-text-xlarge uk-link-reset border-breadcrumb uk-padding-small uk-padding-remove-left uk-width-1-1">I've Forgotten My Username Or Password</a>
                            </div>
                        </div>
                    </div>
                    
                    <div className="uk-grid uk-grid-margin uk-grid-match " uk-grid="true">
                        <div className="uk-width-1-2@m">
                            <div className="uk-card uk-card-default uk-card-body">
                                <h2>Ask in the Forums</h2>
                                <button className="uk-button uk-button-primary uk-button-small">JOIN</button>
                                <div className="uk-margin-top">
                                Join the conversation! We think you would love our community and it's a great place to find Envato announcements or general help.
                                </div>
                            </div>
                        </div>
                        <div className="uk-width-1-2@m">
                            <div className="uk-card uk-card-default uk-card-body">
                                <h2>Visit Our Blog</h2>
                                <button className="uk-button uk-button-primary uk-button-small">Visit</button>
                                <div className="uk-margin-top">
                                We love to share ideas! Visit our blog if you're looking for great articles or inspiration to get you going.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="uk-text-center uk-card uk-card-body">
                        <h2>Still no luck? We can help!</h2>
                        <div className="uk-margin-bottom"><i className="fa fa-life-ring fa-5x" aria-hidden="true"></i></div>
                        <button className="uk-button uk-button-primary uk-button-small">Submit a request</button>
                        <div className="uk-margin-top">
                        Contact us and we’ll get back to you as soon as possible.
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default HowItWork;