import React from "react";
import DeviceWishlist from "../../../components/mobile/wishlistDevices";
import TopDevice from "./topDevice";
class Wishlist extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div>
                <TopDevice/>
                <DeviceWishlist/>
            </div>
        )
    }
}

export default Wishlist;