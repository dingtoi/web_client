import React from "react";
import withStorage from "../../../hoc/storage";
import { Route } from "react-router-dom";
import {SERVER_IMG} from "../../../config";
import { MICRO_AUTH, MICRO_DOMAIN} from "../../../config";
import UserModel from "../../../models/user";

class Account extends React.Component{
    constructor(props){
        super(props);
        this.state={
            avatar: null,
            billing_name:null
        }
    }
    componentDidMount(){
        this.handlerAvatar();
    }
    handlerAvatar(){
        UserModel.userAvatar({email: this.props.auth})
        .then((rs) => {
            this.setState({avatar:rs[0].image, billing_name:rs[0].billing_name})
        })
        .catch(error => {
        })
    }
    render(){
        return (
            <div>
                <div className="uk-text-center uk-padding-large uk-background-blend-multiply uk-background-account uk-background-cover" >
                    <div className="uk-margin-small-bottom" className="uk-cursor" onClick={() => this.props.history.push('/account/avatar')}>
                    {
                            this.state.avatar
                            ?
                            <img className="uk-border-circle" src={MICRO_DOMAIN+MICRO_AUTH+"/users/"+this.state.avatar}
                                width="80" height="80"/>
                            :
                            <img className="uk-border-circle" src={MICRO_DOMAIN+MICRO_AUTH+"/no-image.png"}
                                width="80" height="80"/>
                        }
                    </div>
                    <h2 className="uk-text-bold uk-text-white uk-text-large uk-margin-remove-top uk-margin-large-bottom">{this.props.auth}</h2>
                </div>
                
                {
                    this.props.routes.map((route, i) => (
                        <Route exact path={route.path} key={route.path}
                            render={
                                props => (
                                    <route.component {...props} root={this} socket={this.props.socket}/>
                                )
                        }/>
                    ))
                }
            </div>
        )
    }
}

export default withStorage(Account);