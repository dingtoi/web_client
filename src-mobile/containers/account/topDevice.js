import React from "react";
import {withRouter} from "react-router-dom";

class TopDevice extends React.Component{
    constructor(props){
        super(props);
    }
   
    render(){
        return (
            <div>
                <div className="uk-padding-small uk-padding-remove-top">
                    <div className="uk-flex uk-flex-middle uk-flex-between uk-text-center uk-box-shadow-medium uk-background-default uk-margin-top-negative">
                        <a className="uk-padding-small uk-link-reset"
                            onClick={() => this.props.history.push('/account/device')}>
                            <div>
                                <i className="uk-inline uk-margin-xsmall-bottom">
                                    <img data-src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAB5ElEQVRoQ+2a7VHDMAyGpSwAG1A2gA3YAEbA9gJlAsoEdIHYjAAbsEHZgDABsEDEuUfuiuMk/mrOtO7fOpIev68UpynCgXzwQDiggOSmpFURpdSibdtHRLwAgEUmRTdE9FZV1R1jrDFr6oFoCCLaAMBpJgBmGV+IeGnC9ECklM8AcJ0pRFfWC+f8ZrdGG8hnxmp0tTec8/MpEMpcjW15nPM/ItgU6YGYF80NKqWcrKmAzKlKUcTc7dIjifxXrHUQ1krkhqRhnKyllLoiIpX4lNsgImOMvaYgcgKRUr4nhhg8D4VCuYLs7WyVamwfN0jMLpo7FxNr14ZBisQk3xeIS29hyuSxsQIn6HZCZgUSMUGb3ECCJ2gBsTVgbI/4XG+uzVqRsQlaQFxmuY81Yq05uyK2u7INQtvIZyMKSKi1iiKWn0XHDpJevpxSxebxIUVso/Vf9kgB+bXF8UytqT7w+d52bxi63metLUav2YcSEdE3AKwBYImIJy5APsWNrXXJ7QPyIIRY1XW9QsT7mUEmczuDAIB+m7omoqXrO0bf5/+Rm+dkbqzrWi9ysoqLCt0aIvoQQni9ow+tRedC/cDftu0TIp75FDq2VgeuqurW9yfTkFq6XOW/KKnUSxWnKJJqJ1PF+QF70BU9U3rM4gAAAABJRU5ErkJggg==" width="25" height="25"  uk-img="true" />
                                </i>				
                                <p className="uk-margin-remove uk-text-meta uk-text-muted">MY DEVICE</p>
                            </div>
                        </a>
                        <a className="uk-padding-small uk-link-reset"
                            onClick={() => this.props.history.push('/account/wishlist')}>
                            <div>
                                <i className="uk-inline uk-text-muted uk-margin-xsmall-bottom">
                                    <span uk-icon="heart"></span>
                                </i>
                                <p className="uk-margin-remove uk-text-meta uk-text-muted">WISHLIST</p>
                            </div>
                        </a>
                        <a className="uk-padding-small uk-link-reset"
                            onClick={() => this.props.history.push('/account/received_proposal')}>
                            <div>
                                <i className="uk-inline uk-text-muted uk-margin-xsmall-bottom">
                                    <span uk-icon="file"></span>
                                </i>
                                <p className="uk-margin-remove uk-text-meta uk-text-muted">PROPOSAL</p>
                            </div>
                        </a>
                    </div>
                </div>
                
            </div>
        )
    }
}

export default withRouter(TopDevice);