import React from "react";
import DeviceModel from "../../../models/device";
import withStorage from "../../../hoc/storage";
import Item from "../item";
import { Cover, Spinner } from "../../common";

import {connect} from "react-redux";

import ReactPaginate from 'react-paginate';
class ListType extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            list: [],
            type: 1,
            isLoading: false,
            offset: 0,
            total: 0,
            currentPage: 0
        }
        this.handlerTabItem = this.handlerTabItem.bind(this);
        this.pageDisplay = 4;
    }
    handlePageClick = data => {
        let selected = data.selected;
        let offset = Math.ceil(selected * this.pageDisplay);
    
        this.setState({ offset: offset, currentPage: selected }, () => {
            this.refreshList();
        });
    };
    componentDidMount(){
        this.refreshList();
        this.tab = UIkit.tab(this.tabRef);
    }
    componentWillReceiveProps(nextProps){
        this.refreshList();
    }
    componentWillUnmount(){
        this.tab.$destroy(true);
    }
    refreshList(){
        this.setState({isLoading: true});
        DeviceModel.getDeviceWithType({email: this.props.auth, type: this.state.type, offset: this.state.offset, limit: this.pageDisplay})
        .then(result => {
            this.setState({list: result.list, isLoading: false, total:Math.ceil(result.total / this.pageDisplay)});
        })
        this.tab = UIkit.tab(this.tabRef);
    }
    componentWillUnmount(){ 
        this.tab.$destroy(true);
    }
    handlerTabItem(type){
        this.setState({type: type, currentPage: 0, offset: 0}, () => {
            this.refreshList();
            DeviceModel.getDeviceWithType({email: this.props.auth, type: this.state.type, offset: this.state.offset, limit: this.pageDisplay})
            .then(result => {
                this.setState({list: result.list, isLoading: false, total:Math.ceil(result.total / this.pageDisplay)});
            })
        });
    }
    render(){
        return (
            <div>
                <div className="uk-flex uk-flex-between uk-flex-middle uk-margin-bottom">
                    <h3 className="uk-text-uppercase">Product List</h3>
                    <div>
                        <ul uk-tab="true" className="uk-tab" ref={instance => this.tabRef = instance}>
                            <li className="uk-active"><a onClick={() => this.handlerTabItem(1)}>Sale</a></li>
                            <li><a onClick={() => this.handlerTabItem(2)}>Exchange</a></li>
                            <li><a onClick={() => this.handlerTabItem(3)}>Sale & Exchange</a></li>
                        </ul>
                    </div>
                </div>
                <div className="uk-inline uk-width-1-1">
                    {this.state.isLoading && <Cover/>}
                    {this.state.isLoading && <Spinner/>}
                    {
                        this.state.list.length > 0
                        ?
                        <div className="uk-grid uk-grid-match dingtoi_product_list" uk-grid="true">
                            {
                                this.state.list.map((item, key) => {
                                    return (
                                        <div className="uk-width-1-4@l uk-width-1-3@m uk-width-1-2@s dingtoi_product_list_item" key={key}>
                                            <Item item={item}
                                                onAddCart={() => this.handlerTabItem(this.state.type)}/>
                                        </div>
                                    )
                                })
                            }
                        </div>
                        :
                        <div className="uk-placeholder uk-text-center">There are no items.</div>
                    }
                    {
                        this.state.list.length > 0
                        ?
                        <div >
                            <ReactPaginate
                            previousLabel={'Previous'}
                            nextLabel={'Next'}
                            breakLabel={'...'}
                            breakClassName={'break-me'}
                            pageCount={this.state.total}
                            forcePage={this.state.currentPage}
                            onPageChange={this.handlePageClick}
                            containerClassName={'pagination uk-flex uk-flex-center uk-padding-remove'}
                            subContainerClassName={'pages pagination'}
                            activeClassName={'active'}
                            />
                        </div>
                        :
                        null
                    }
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const ConnectListType = connect(mapStateToProps)(ListType);

export default withStorage(ConnectListType);