import React from "react";
import {withRouter} from "react-router-dom";
import withStorage from "../../../hoc/storage";
import SubMenu from "./submenu";
import Purpose from "./purpose";
import MenuSearch from "./search";
import axios from "axios";

import {addCart} from "../../../src/containers/home/actions";
import {connect} from "react-redux";

import {Cover, Spinner} from "../../../components/common";

import CartModel from "../../../models/cart";
import {SERVER_IMG, SERVER_DOMAIN, MICRO_DEVICE, MICRO_DOMAIN, MICRO_EVENT} from "../../../config";

class Menu extends React.Component{
    constructor(props){
        super(props);
        this.navigateToLogin = this.navigateToLogin.bind(this);
        this.navigateToHome = this.navigateToHome.bind(this);
        this.navigateToAccount = this.navigateToAccount.bind(this);
        this.navigateToBag = this.navigateToBag.bind(this);
        this.navigateToSupport = this.navigateToSupport.bind(this);
        this.handlerLogout = this.handlerLogout.bind(this);
        this.state = {
            count: 0,
            list: [],
            isLoading: false
        }
        this.__unmount = false;
    }
    componentDidMount(){
        this.__unmount = false;
        $(this.menuSearch).on('hidden', () => {
            this.search.reset();
        });
        this.refreshListCart();
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.count){
            this.refreshListCart();
        }
    }
    componentWillUnmount(){
        this.__unmount = true;
    }
    refreshListCart(){
        setTimeout(() => {
            if(!this.__unmount){
                if(this.props.auth){
                    CartModel.getListCartByUser({email: this.props.auth})
                    .then((list) => {
                        this.setState({count: list.length, list: list, isLoading: false});
                    })
                    .catch(() => {
                        this.setState({count: 0, list: [], isLoading: false});
                    })
                }else{
                    this.setState({count: 0, list: [], isLoading: false});
                }
            }
        }, 500)
    }
    navigateToHome(){
        this.props.history.push('/');
    }
    navigateToLogin(){
        this.props.history.push('/login');
    }
    navigateToRegister(){
        this.props.history.push('/register');
    }
    navigateToAccount(){
        this.props.history.push('/account/device');
    }
    navigateToBag(){
        this.props.history.push('/bag');
    }
    navigateToSupport(){
        this.props.history.push('/support');
    }
    handlerLogout(){
        localStorage.removeItem('email');
        localStorage.removeItem('anonymous');
        setTimeout(() => {
            this.props.history.push('/login');
            UIkit.notification({message: 'Logout Successfully !', status: 'primary', pos: 'top-left'});
            this.props.app.setState({isMenu: false}, () => {
                this.props.app.setState({isMenu: true}, () => {
                });
            });
        }, 500)
    }
    handlerGoToDevice(l){
        this.props.history.push('/device/'+l.device_id);
    }
    handlerDeleteCartItem(l){
        this.setState({isLoading: true});
        axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/add',{name: 'Remove item cart', key: 'REMOVE_ITEM_CART', desc: ''})
        .then((created) => {
            CartModel.deleteItemCart({id: l.id})
            .then(() => {
                this.props.addCart();
                this.refreshListCart();
            })
            .then(()=>{
                axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/changeStatus',{id: created})
                .then(() => {
                })
                .catch(() => {
            })
            })
            .catch(() => {
                this.setState({isLoading: false});
            })
        })
        .catch(() => {}) 
    }
    render(){
        return (
            <div className="uk-background-primary uk-zindex-highest" uk-sticky="true" id="menu-container">
                <div className="uk-container">
                    <div className="uk-flex uk-text-white uk-flex-between">
                        <div className="uk-hidden@m uk-cursor uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-left uk-padding-remove-top uk-padding-remove-bottom uk-link-reset">
                            <div className="uk-inline">
                                <i className="fa fa-navicon"/>
                                <div uk-dropdown="mode: click">
                                    <ul className="uk-list uk-list-large uk-list-divider">
                                        <li>
                                            <a href="#">Smart Phones</a>    
                                        </li>
                                        <li>
                                            Tablets
                                        </li>
                                        <li>
                                            Smart Watches
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <a className="uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-left uk-padding-remove-top uk-padding-remove-bottom"
                            onClick={this.navigateToHome}>
                            <div>
                                <img data-src={SERVER_IMG+"logo.png"} width="30" height="30" uk-img="true"/>
                            </div>
                        </a>
                        <div className="uk-cursor uk-visible@l uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-link-reset uk-text-small">
                            <div className="uk-item-height">Your Purpose</div>
                            <div uk-dropdown="mode: click; boundary: #menu-container; boundary-align: true"
                                className="uk-width-1-1 uk-margin-remove uk-padding-remove">
                                <Purpose/>
                            </div>
                        </div>
                        <div className="uk-cursor uk-visible@m uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-link-reset uk-text-small">
                            <div className="uk-item-height">Smart Phones</div>
                            <div uk-dropdown="mode: click; boundary: #menu-container; boundary-align: true"
                                className="uk-width-1-1 uk-margin-remove uk-padding-remove">
                                <SubMenu type={1}/>
                            </div>
                        </div>
                        <div className="uk-cursor uk-visible@m uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-link-reset uk-text-small">
                            <div className="uk-item-height">Tablets</div>
                            <div uk-dropdown="mode: click; boundary: #menu-container; boundary-align: true"
                                className="uk-width-1-1 uk-margin-remove uk-padding-remove">
                                <SubMenu type={2}/>
                            </div>
                        </div>
                        <div className="uk-cursor uk-visible@m uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-link-reset uk-text-small">
                            <div className="uk-item-height">Smart Watches</div>
                            <div uk-dropdown="mode: click; boundary: #menu-container; boundary-align: true"
                                className="uk-width-1-1 uk-margin-remove uk-padding-remove">
                                <SubMenu type={3}/>
                            </div>
                        </div>
                        <a className="uk-visible@l uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-link-reset uk-text-small"
                        onClick={this.navigateToSupport}>
                            Support
                        </a>
                        {
                            !this.props.auth || (this.props.auth && this.props.anonymous)
                            ?
                            <a className="uk-visible@m uk-background-muted uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-link-reset uk-text-small"
                                onClick={this.navigateToRegister.bind(this)}>
                                Register
                            </a>
                            : null
                        }
                        <div className="uk-visible@m uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-link-reset">
                            <a className="uk-item-height">
                                <i className="fa fa-search"/>
                            </a>
                            <div uk-dropdown="mode: click; boundary: #menu-container; boundary-align: true"
                                className="uk-width-1-1 uk-margin-remove uk-padding-remove" ref={instance => this.menuSearch = instance}>
                                <MenuSearch ref={instance => this.search = instance} {...this.props}
                                    onClose={() => UIkit.dropdown($(this.menuSearch)).hide()}/>
                            </div>
                        </div>
                        <div className="uk-height-small uk-cursor uk-flex uk-flex-middle uk-padding-small uk-padding-remove-right uk-padding-remove-top uk-padding-remove-bottom uk-link-reset">
                            <div className="uk-inline" id="shopping-cart-icon">
                                <i className="fa fa-shopping-bag"/>
                                {
                                    this.state.count > 0
                                    ? <span className="uk-badge uk-position-top-right uk-position-cart">{this.state.count}</span>
                                    : null
                                }
                            </div>
                            <div uk-dropdown="pos: top-center;">
                                {
                                    this.state.list.length > 0 &&
                                    <div className="uk-inline uk-width-1-1">
                                        {this.state.isLoading && <Cover/>}
                                        {this.state.isLoading && <Spinner/>}
                                    {
                                        this.state.list.map((l, key) => {
                                            return (
                                                <React.Fragment key={key}>
                                                    <div className="uk-dropdown-grid" uk-grid="true">
                                                        <div className="uk-width-1-3">
                                                            <div className="uk-flex uk-flex-center uk-flex-middle">
                                                            {
                                                                l.thumb
                                                                ?
                                                                <a href="#" onClick={() => this.handlerGoToDevice(l)}>
                                                                    <img className="uk-img uk-cursor" src={MICRO_DOMAIN+MICRO_DEVICE + '/devices/'+l.thumb} width="105px" uk-img="true"/>
                                                                </a>
                                                                :
                                                                <a href="#" onClick={() => this.handlerGoToDevice(l)}>
                                                                    <img src={SERVER_DOMAIN+'no-image.png'} width="105px"/>
                                                                </a>
                                                            }
                                                            </div>
                                                        </div>
                                                        <div className="uk-width-2-3">
                                                            <a href="#" onClick={() => this.handlerGoToDevice(l)}>
                                                                <div className="uk-text-truncate uk-text-bold">{l.device_name}</div>
                                                            </a>
                                                            <div>
                                                                {
                                                                    Number(l.cart_type) === 1
                                                                    ? <div className="uk-text-small">Sale</div>
                                                                    :
                                                                    <div className="uk-text-small">Exchange</div>
                                                                }
                                                            </div>
                                                            <a className="uk-text-success uk-text-small"
                                                                onClick={() => this.handlerDeleteCartItem(l)}>Remove Item</a>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                </React.Fragment>
                                            )
                                        })
                                    }
                                    </div>
                                }
                                <div className="uk-flex uk-flex-column">
                                    <a className="uk-flex uk-flex-middle uk-height-small" href="#" onClick={this.navigateToBag}>
                                        <i className="fa fa-shopping-bag"/>
                                        <div className="uk-margin-xsmall-left">Bag {
                                            this.state.count > 0 && <span className="uk-text-bold">({this.state.count})</span>
                                        }</div>
                                    </a>
                                    {
                                        this.props.auth && !this.props.anonymous
                                        ? 
                                        <a className="uk-flex uk-flex-middle uk-height-small" href="#" onClick={this.navigateToAccount}>
                                            <i className="fa fa-user"/>
                                            <div className="uk-margin-xsmall-left">My Account</div>
                                        </a>
                                        : null
                                    }
                                    {
                                        !this.props.auth || (this.props.auth && this.props.anonymous)
                                        ? 
                                        <a className="uk-flex uk-flex-middle uk-height-small" href="#" onClick={this.navigateToLogin}>
                                            <i className="fa fa-sign-out"/>
                                            <div className="uk-margin-xsmall-left">Login</div>
                                        </a>
                                        : null
                                    }
                                    {
                                        this.props.auth && !this.props.anonymous
                                        ? 
                                        <a className="uk-flex uk-flex-middle uk-height-small" href="#" onClick={this.handlerLogout}>
                                            <i className="fa fa-sign-out"/>
                                            <div className="uk-margin-xsmall-left">Logout</div>
                                        </a>
                                        : null
                                    }
                                </div>                         
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const mapDispatchToProps = {
    addCart
}

const ConnectMenu = connect(mapStateToProps, mapDispatchToProps)(Menu);

export default withRouter(withStorage(ConnectMenu));