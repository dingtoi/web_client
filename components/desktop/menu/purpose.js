import React from "react";
import {withRouter} from "react-router-dom";
import {SERVER_IMG} from "../../../config";

class Purpose extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div>
                <div className="uk-container-extend uk-background-default">
                    <div className="uk-container uk-position-relative uk-visible-toggle uk-light " uk-slider="true">
                        <ul className="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@m uk-child-width-1-4@l uk-flex uk-flex-middle uk-flex-center">
                            <a className="uk-padding-small uk-padding-remove-left uk-padding-remove-right"
                                href="#"
                                onClick={() => this.props.history.push('/category?type=1')}>
                                <li className="uk-text-center">
                                    <img src={SERVER_IMG+"sale.png"} alt="" width="45px" />
                                </li>
                            </a>
                           <a className="uk-padding-small uk-padding-remove-left uk-padding-remove-right"
                                href="#"
                                onClick={() => this.props.history.push('/category?type=2')}>
                                <li className="uk-text-center">
                                    <img src={SERVER_IMG+"exchange.png"} alt="" width="45px"/>
                                </li>
                            </a>                           
                           <a className="uk-padding-small uk-padding-remove-left uk-padding-remove-right"
                                href="#"
                                onClick={() => this.props.history.push('/category?type=1')}>
                                <li className="uk-text-center">
                                    <img src={SERVER_IMG+"buy.png"} alt="" width="45px"/>
                                </li>
                            </a>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Purpose);