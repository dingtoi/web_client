import React from "react";
import DeviceModel from "../../../models/device";

import {Cover, Spinner} from "../../../components/common";

class MenuSearch extends React.Component{
    constructor(props){
        super(props);
        this.state={
            isLoading: false,
            list:[],
            device_name: ''
        }
        this.searchObj = null;
    }
    componentWillUnmount(){
        this.searchObj = null;
        this.reset();
    }
    handlerAutocomplete(){
        this.searchObj = $(this.search).flexdatalist({
            cache: false
        });
       
        this.handlerChangeAuto();
    }
    handlerSearch(){
        this.setState({isLoading: true});
        setTimeout(() => {
            if(is.not.undefined(this.props.onClose))
                this.props.onClose();
            if($(this.search).val()=='')
                this.props.history.push('/');
            else
            {
                this.props.history.push('/search/'+this.state.device_name);
                this.reset();
            }
        }, 200);
        setTimeout(() => {
            this.setState({isLoading: false});
        }, 300);
    }
    componentDidMount(){
        this.handlerAutocomplete();
    }
    reset(){
        this.setState({list: [], device_name: ''}, () => {
            $(this.search).flexdatalist('reset');
            this.handlerChangeAuto();
            $(this.search).val('');
        });
    }
    handlerChangeAuto(){
        $(this.search).on('change:flexdatalist', (event, set, options) => {
            this.setState({device_name: set.value}, () => {
                if(set.value.length > 1){
                    this.setState({isLoading:true});
                    DeviceModel.getSearchByName({device_name:this.state.device_name, email: this.props.auth})   
                    .then((list)=>{
                        this.setState({isLoading:false,list:list},()=>{
                        });
                    })
                    .catch(() => {
                        this.setState({ isLoading: true });
                    })
                }
            });
        },
        );
        $(this.search).on('select:flexdatalist', (event, set, options) => {
           this.props.history.push('/device/'+set.value);
        });
    }
    render(){
        return (
            <div className="uk-inline uk-width-1-1">
                {this.state.isLoading && <Cover/>}
                {this.state.isLoading && <Spinner/>}
                <div className="uk-container-extend uk-background-default">
                    <div className="uk-container uk-padding-small">
                        <div className="uk-flex">
                            <input className="uk-input" ref={instance => this.search = instance} placeholder='Search by name' list="languages"/>
                            <button type="button" className="uk-button uk-button-primary uk-width-small uk-padding-remove"
                                onClick={this.handlerSearch.bind(this)}><i className="fa fa-search uk-margin-xsmall-right"></i>Search</button>
                        </div>
                        <datalist id="languages">
                        {
                            this.state.list.map((item, key) => {
                                return (
                                    <option key={key} value={item.id} >{item.device_name} - {item.ram_name} - {item.color_name}</option>
                                )
                            })
                        }
                        </datalist>
                    </div>
                </div>
            </div>
        )
    }
}

export default MenuSearch;