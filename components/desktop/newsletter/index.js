import React from "react";
import {SERVER_IMG} from "../../../config";
import { Form, Field } from 'react-final-form';
import {Cover, Spinner, Button } from "../../../components/common";

class Newsletter extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading:false
        }
        this.handlerSubmit = this.handlerSubmit.bind(this);       
        this.resetForm = null;
    }
    handlerSubmit(values){
        this.setState({isLoading:true});
        if(values.email){
            UIkit.notification({message: 'Subcribe Successfully !', status: 'primary', pos: 'top-left'});
            this.setState({isLoading:false});
            this.resetForm();
        }
        else
            UIkit.notification({message: 'Must Be Input Email !', status: 'danger', pos: 'top-left'});
    }
    render(){
        return (
            <div>
                <div className="uk-flex uk-flex-middle uk-flex-center uk-height-large uk-flex-column uk-background-blend-multiply" style={{backgroundImage: 'url('+SERVER_IMG+'banner-newletter.jpg)'}}>
                    <h2 className="uk-margin-remove-bottom uk-text-white">NEWSLETTER</h2>
                    <p className="uk-text-white">Send us your email, we'll make sure you never miss a thing !</p>
                    <Form
                        onSubmit={this.handlerSubmit}
                        render={({validating, handleSubmit, form, submitting}) => {
                            this.resetForm = form.reset;
                            return <form onSubmit={handleSubmit} >
                                <div className="uk-inline uk-width-1-1 uk-flex uk-flex-row@m uk-flex-column@s uk-flex-center uk-flex-middle">
                                    {submitting && <Cover/>}
                                    {submitting && <Spinner/>}
                                    <Field name="email">
                                        {({ input}) => (
                                            <div >
                                                <input type="email" {...input} className="uk-input uk-form-width-large@s uk-form-width-large@m uk-form-width-large@l" placeholder="Please input your email" />
                                            </div>
                                        )}
                                    </Field>
                                    <div>
                                        <Button className="uk-margin-xsmall-left uk-button" type="submit" disabled={submitting || validating} color="primary">
                                            Subscribe
                                        </Button>
                                    </div>
                                </div>
                            </form>
                        }}>
                    </Form>
                </div>
            </div>
        )
    }
}

export default Newsletter;