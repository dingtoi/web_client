import React from "react";
import {withRouter} from "react-router-dom";
import {SERVER_IMG} from "../../../config";
import withStorage from "../../../hoc/storage";

import {addCart} from "../../../src/containers/home/actions";
import {connect} from "react-redux";

class Footer extends React.Component{
    constructor(props){
        super(props);
        this.navigateToNewsroom = this.navigateToNewsroom.bind(this);
        this.navigateToLeadership= this.navigateToLeadership.bind(this);
        this.navigateToJob = this.navigateToJob.bind(this);
        this.navigateToInvestors = this.navigateToInvestors.bind(this);
        this.navigateToContact = this.navigateToContact.bind(this);
       
    }
   
    navigateToNewsroom(){
        this.props.history.push('/newsroom');
    }
    navigateToLeadership(){
        this.props.history.push('/leadership');
    }
    navigateToJob(){
        this.props.history.push('/job-opportunities');
    }
    navigateToInvestors(){
        this.props.history.push('/investors');
    }
    navigateToContact(){
        this.props.history.push('/contact');
    }
    
    componentDidMount(){
        if ($('.scroll').length) {
            var scrollTrigger = 100, // px
                backToTop = function () {
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > scrollTrigger) {
                        $('.scroll').addClass('show');
                    } else {
                        $('.scroll').removeClass('show');
                    }
                };
            backToTop();
            $(window).on('scroll', function () {
                backToTop();
            });
           
        }
    }
    render(){
   
        return (
            <div className="uk-background-primary">
                <div className="uk-container uk-padding-top uk-padding">
                    <div className="uk-grid uk-grid-medium" uk-grid="true">
                        <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-4@l">
                            <div className="uk-flex uk-flex-middle uk-height-small">
                                <img data-src={SERVER_IMG+"logo.png"}
                                    width="20" uk-img="true"
                                    className="uk-margin-xsmall-right"/>
                                <h5 className="uk-heading-line uk-margin-remove uk-text-white">DINGTOI</h5>
                            </div>
                            <ul className="uk-list uk-list-large">
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <i className="fa fa-phone uk-margin-xsmall-right"/>
                                    <span>
                                        (098)-686-666888
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <i className="fa fa-envelope uk-margin-xsmall-right"/>
                                    <span>
                                        email@dingtoi.com
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <i className="fa fa-map uk-margin-xsmall-right"/>
                                    <span>
                                        248 Avenue, Quebec, Canada
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-4@l">
                            <div className="uk-flex uk-flex-middle uk-height-small">
                                <span className="uk-heading-border uk-margin-xsmall-right"/>
                                <h5 className="uk-heading-line uk-light uk-margin-remove uk-text-white">PRODUCTS</h5>
                            </div>
                            <ul className="uk-list uk-list-large">
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor" onClick={() => this.props.history.push('/category?device=1')}>
                                    <span>
                                        Smart Phones
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor" onClick={() => this.props.history.push('/category?device=2')}>
                                    <span>
                                        Tablets
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor" onClick={() => this.props.history.push('/category?device=3')}>
                                    <span>
                                        Smart Watches
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor" onClick={() => this.props.history.push('/category?type=1')}> 
                                    <span>
                                        Sale Products
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor" onClick={() => this.props.history.push('/category?type=2')}>
                                    <span>
                                        Exchange Products
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-4@l">
                            <div className="uk-flex uk-flex-middle uk-height-small">
                                <span className="uk-heading-border uk-margin-xsmall-right"/>
                                <h5 className="uk-heading-line uk-light uk-margin-remove uk-text-white">MY ACCOUNT</h5>
                            </div>
                            <ul className="uk-list uk-list-large">    
                                {
                                    !this.props.auth || (this.props.auth && this.props.anonymous)
                                    ?
                                    <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor" onClick={() => this.props.history.push('/register')}>
                                        <span>
                                            Registration
                                        </span>
                                        </li>
                                    : null
                                }
                                
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor" onClick={() => this.props.history.push('/bag')}>
                                    <span>
                                        My Cart
                                    </span>
                                </li>
                                {
                                    !this.props.auth || (this.props.auth && this.props.anonymous)
                                    ?
                                    <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor" onClick={() => this.props.history.push('/login')}>
                                        <span>
                                            Checkout
                                        </span>
                                    </li>
                                    : 
                                    <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor" onClick={() => this.props.history.push('/preview')}>
                                        <span>
                                            Checkout
                                        </span>
                                    </li>
                                }
                                {
                                    !this.props.auth || (this.props.auth && this.props.anonymous)
                                    ?
                                    <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor" onClick={() => this.props.history.push('/login')}>
                                        <span>
                                            My Account
                                        </span>
                                    </li>
                                    : 
                                    <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor" onClick={() => this.props.history.push('/account/device')}>
                                        <span>
                                            My Account
                                        </span>
                                    </li>
                                }
                                {
                                    !this.props.auth || (this.props.auth && this.props.anonymous)
                                    ?
                                    <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor" onClick={() => this.props.history.push('/login')}>
                                        <span>
                                            My Wishlist
                                        </span>
                                        </li>
                                    : 
                                    <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor" onClick={() => this.props.history.push('/account/wishlist')}>
                                        <span>
                                            My Wishlist
                                        </span>
                                    </li>
                                }
                            </ul>
                        </div>
                        <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-4@l">
                            <div className="uk-flex uk-flex-middle uk-height-small">
                                <span className="uk-heading-border uk-margin-xsmall-right"/>
                                <h5 className="uk-heading-line uk-light uk-margin-remove uk-text-white">ABOUT DINGTOI</h5>
                            </div>
                            <ul className="uk-list uk-list-large">
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor"
                                onClick={this.navigateToNewsroom}>
                                    <span>
                                        Newsroom
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor"
                                onClick={this.navigateToLeadership}>
                                    <span>
                                        Leadership
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor"
                                onClick={this.navigateToJob}>
                                    <span>
                                        Job Opportunities
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor"
                                onClick={this.navigateToInvestors}>
                                    <span>
                                        Investors
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle uk-cursor"
                                onClick={this.navigateToContact}>
                                    <span>
                                        Contact Dingtoi
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <hr className="uk-divider-small"/>
                    <div className="uk-flex uk-flex-column uk-flex-column@s uk-flex-row@l uk-flex-between@l">
                        <div className="uk-text-small uk-text-white uk-flex uk-flex-middle">
                            © Dingtoi 2018. All Rights Reserved.
                        </div>
                        <div className="uk-text-small uk-text-white uk-flex uk-flex-middle uk-flex-left@m uk-flex-center@l uk-visible@m uk-margin-remove-top@l uk-margin-top@s">
                            <div>
                                <a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-xsmall-right" onClick={() => this.props.history.push('/privacy')}>Privacy Policy</a>
                            </div>
                            <div>
                                <a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-xsmall-right" onClick={() => this.props.history.push('/term-of-use')}>Term Of Us</a>
                            </div>
                            <div>
                                <a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-xsmall-right" onClick={() => this.props.history.push('/sale-and-retunds')}>Sales And Refunds</a>
                            </div>
                            <div>
                                <a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-xsmall-right" onClick={() => this.props.history.push('/legal')}>Legal</a>
                            </div>
                            <div>
                                <a className="uk-link-reset uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left" onClick={() => this.props.history.push('/sitemap')}>Sitemap</a>
                            </div>
                        </div>
                        <div className="uk-text-small uk-text-white uk-flex uk-flex-middle uk-flex-left@m uk-flex-right@l uk-margin-remove-top@l uk-margin-top@s">
                            <img data-src={SERVER_IMG+'canada.png'}
                                width="20" height="20" uk-img="true"/>
                            <span className="uk-margin-xsmall-left">Canada</span>
                        </div>
                    </div>
                </div>
                <a className="uk-position-fixed scroll uk-button uk-button-primary uk-button-small" href="#target" uk-scroll="true" ><i className="fa fa-chevron-up" aria-hidden="true"></i></a>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}
const mapDispatchToProps = {
    addCart
}
const ConnectFooter = connect(mapStateToProps, mapDispatchToProps)(withRouter(withStorage(Footer)));

export default ConnectFooter;