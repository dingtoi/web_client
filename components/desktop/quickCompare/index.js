import React from "react";
import DeviceModel from "../../../models/device";
import withStorage from "../../../hoc/storage";
import Item from "../../../components/desktop/item";

class QuickCompare extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            detail: {},
            detailCompare: {}
        }
        this.next = 0;
        this.list = [];
    }
    componentWillMount(){
        this.id = this.props.id;
    }
    componentDidMount(){
        this.refreshDetail();
    }
    refreshDetail(){
        this.setState({isLoading: true});
        DeviceModel.getDeviceCompare({id: this.id, email: this.props.auth})
        .then((result) => {
            this.list = result.listCompare;
            this.setState({detail: result.main, detailCompare: result.listCompare[this.next], isLoading: false});
        })
        .catch(() => this.setState({isLoading: false}))
    }
    handlerNext(){
        if(this.list.length > Number(this.next)+1){
            this.next++;
        }else this.next = 0;
        this.setState({detailCompare: this.list[this.next]});
    }
    render(){
        return (
            <div>
                <div className="uk-padding-small border-breadcrumb">
                    <h3 className="uk-h3 uk-margin-remove">Quick Compare</h3>
                </div>
                <div className="uk-padding-small uk-flex uk-flex-center uk-flex-middle uk-flex-row@m uk-flex-column@s">
                    <div className="uk-padding-small uk-border-quick-comapre uk-width-medium">
                        <div style={{height: '500px'}} className="uk-flex uk-flex-center uk-flex-middle">
                            <Item item={this.state.detail} mode="compare"
                                onAddCart={this.props.onAddCart}/>
						</div>
                        <div className="uk-text-center">
                            <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detail.category_name}</div>
                            <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detail.brand_name}</div>
                            <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detail.color_name}</div>
                            <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detail.capacity_name}</div>
                            <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detail.ram_name}</div>
                        </div>
                    </div>
                    <div className="uk-padding-small uk-width-medium uk-inline">
                        <div className="uk-position-top-right"><a className="uk-link-reset"
                            onClick={this.handlerNext.bind(this)}><i className="fa fa-close fa-2x"></i></a></div>
                        <div style={{height: '500px'}} className="uk-flex uk-flex-center uk-flex-middle">
                            <Item item={this.state.detailCompare} mode="compare"
                                onAddCart={this.props.onAddCart}/>
						</div>
                        <div className="uk-text-center">
                            <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detailCompare.category_name}</div>
                            <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detailCompare.brand_name}</div>
                            <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detailCompare.color_name}</div>
                            <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detailCompare.capacity_name}</div>
                            <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detailCompare.ram_name}</div>
                        </div>
                    </div>
                   
                </div>
                {/*<div className="uk-text-center uk-padding-small"><a className="uk-text-underline uk-text-primary">VIEW MORE COMPARE</a></div>*/}
            </div>
        )
    }
}

export default withStorage(QuickCompare);