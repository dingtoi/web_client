import React from "react";
import { SERVER_DOMAIN, MICRO_DOMAIN, MICRO_DEVICE, MICRO_EVENT } from "../../../config";
import axios from "axios";
import {connect} from "react-redux";
import {addCart} from "../../../src/containers/home/actions";

import CartModel from "../../../models/cart";

import withStorage from "../../../hoc/storage";
import {withRouter} from "react-router-dom";

import {Modal, Cover, Spinner} from "../../../components/common";
import QuickCompare from "../../../components/desktop/quickCompare";
import QuickView from "../../../components/desktop/quickView";

class Item extends React.Component{
    constructor(props){
        super(props);
        this.handlerAddCart = this.handlerAddCart.bind(this);
        this.state = {
            isModalQuickView: false,
            isModalQuickCompare: false,
            isLoading: false
        }
    }
    
    handlerAddCart(type){
        this.setState({isLoading: true});
        const cart = $('#shopping-cart-icon');
        const imgtodrag = $(this.image);
        if (imgtodrag) {
            const imgclone = imgtodrag.clone()
            .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
            .css({
                'opacity': '0.5',
                'position': 'absolute',
                'height': '150px',
                'width': '150px',
                'z-index': '9999'
            })
            .appendTo($('body'))
            .animate({
                'top': cart.offset().top + 10,
                'left': cart.offset().left + 10,
                'width': 75,
                'height': 75
            }, 1000);

            imgclone.animate({
                'width': 0,
                'height': 0
            }, () => {
                $(this.refs.atcRef).detach();
                const {available_id, id} = this.props.item;

                axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/add',{name: 'Add to cart', key: 'ADD_TO_CART', desc: ''})
                .then((created) => {
                    CartModel.postItemToCart({available_id, device_id: id, email: this.props.auth, type})
                    .then(() => {
                        this.props.addCart();
                        this.setState({isLoading: false});
                        if(is.function(this.props.onAddCart))
                            this.props.onAddCart();
                    })
                    .then(()=>{
                        axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/changeStatus',{id: created})
                        .then(() => {
                        })
                        .catch(() => {
                    })
                    })
                    .catch(() => {})
                })
                .catch(() => {}) 
            });
        }
    }
    renderPrice(){
        const {type, sale_price, exchange_price, exchange_name} = this.props.item;
        if(type === 1){
            return (
                <React.Fragment>
                    <li>
                        <div className="uk-flex uk-flex-between">
                            <div className="uk-text-bold">Sale</div>
                            <div>{accounting.formatMoney(sale_price)}</div>
                        </div>
                    </li>
                </React.Fragment>
            )
        }else if(type === 2){
            return (
                <React.Fragment>
                    <li>
                        <div className="uk-flex uk-flex-between">
                            <div className="uk-text-bold">Exchange</div>
                            <div>{accounting.formatMoney(exchange_price)}</div>
                        </div>
                        <div className="uk-flex uk-text-meta uk-flex-middle uk-margin-xsmall-top">
                            <i className="fa fa-exchange"/>
                            <div className="uk-margin-xsmall-left uk-text-meta uk-text-truncate">Exchange with {exchange_name}</div>
                        </div>
                    </li>
                </React.Fragment>
            )
        }else if(type === 3){
            return (
                <React.Fragment>
                    <li>
                        <div className="uk-flex uk-flex-between">
                            <div className="uk-text-bold">Sale</div>
                            <div>{accounting.formatMoney(sale_price)}</div>
                        </div>
                    </li>
                    <li>
                        <div className="uk-flex uk-flex-between">
                            <div className="uk-text-bold">Exchange</div>
                            <div>{accounting.formatMoney(exchange_price)}</div>
                        </div>
                        <div className="uk-flex uk-text-meta uk-flex-middle uk-margin-xsmall-top">
                            <i className="fa fa-exchange"/>
                            <div className="uk-margin-xsmall-left uk-text-meta uk-text-truncate">Exchange With: {exchange_name}</div>
                        </div>
                    </li>
                </React.Fragment>
            )
        }
        else return null;
    }
    renderBtn(){
        const {type} = this.props.item;
        if(type === 1){
            return (
                <div className="uk-width-1-1">
                    <button className="uk-button uk-button-primary uk-width-1-1 uk-padding-remove"
                        onClick={() => this.handlerAddCart(1)} ref={instance => this.atcRef = instance}>Buy</button>
                </div>
            )
        }else if(type === 2){
            return (
                <div className="uk-width-1-1">
                    <button className="uk-button uk-button-secondary uk-width-1-1 uk-padding-remove"
                        onClick={() => this.handlerAddCart(2)} ref={instance => this.atcRef = instance}>Exchange</button>
                </div>
            )
        }else if(type === 3){
            return (
                <React.Fragment>
                    <div className="uk-width-1-2">
                        <button className="uk-button uk-button-primary uk-width-1-1 uk-padding-remove"
                            onClick={() => this.handlerAddCart(1)} ref={instance => this.atcRef = instance}>Buy</button>
                    </div>
                    <div className="uk-width-1-2">
                        <button className="uk-button uk-button-secondary uk-width-1-1 uk-padding-remove"
                            onClick={() => this.handlerAddCart(2)} ref={instance => this.atcRef = instance}>Exchange</button>
                    </div>
                </React.Fragment>
            )
        }else null;
    }
    render(){
        const {device_name, thumb} = this.props.item;
        const {isModalQuickView, isModalQuickCompare} = this.state;
        return (
            <div className="uk-inline uk-width-1-1">
                {this.state.isLoading && <Cover/>}
                {this.state.isLoading && <Spinner/>}
                <div className="uk-transition-toggle">
                    {
                        this.props.mode !== 'compare'
                        ?
                        <React.Fragment>
                            <Modal isShow={isModalQuickView} onHidden={() => this.setState({isModalQuickView: false})}>
                                <QuickView id={this.props.item.id}
                                    onAddCart={() => this.setState({isModalQuickView: false})}/>
                            </Modal>
                            <Modal isShow={isModalQuickCompare} onHidden={() => this.setState({isModalQuickCompare: false})}>
                                <QuickCompare id={this.props.item.id}
                                    onAddCart={() => this.setState({isModalQuickCompare: false})}/>
                            </Modal>
                        </React.Fragment>
                        : null
                    }
                    <div className="uk-card uk-card-small uk-card-body uk-inline">
                        {
                            this.props.mode !== 'compare'
                            ?
                            <div className="uk-position-left uk-padding-xlarge-top uk-padding-small uk-transition-slide-bottom-small ">
                                <div className="uk-margin-xsmall-bottom">
                                    <button className="uk-button uk-button-primary uk-padding-remove uk-button-item"
                                        onClick={() => {this.setState({isModalQuickView: true})}}>
                                        <i className="fa fa-search"></i>
                                    </button>
                                </div>
                                <div>
                                    <button className="uk-button uk-button-primary uk-button-item uk-padding-remove"
                                        onClick={() => {this.setState({isModalQuickCompare: true})}}>
                                        <i className="fa fa-arrows"></i>
                                    </button>
                                </div>
                            </div>
                            : null
                        }
                        <div className="uk-flex-center">
                            {
                                thumb 
                                ?
                                <a onClick={() => this.props.history.push('/device/'+this.props.item.id)}>
                                    <img className="uk-img uk-cursor" src={MICRO_DOMAIN+MICRO_DEVICE+'/devices/'+thumb} uk-img="true" ref={instance => this.image = instance}/>
                                </a>
                                :
                                <a onClick={() => this.props.history.push('/device/'+this.props.item.id)} ref={instance => this.image = instance}>
                                    <img src={SERVER_DOMAIN+'no-image.png'}/>
                                </a>
                            }
                        </div>
                        <h4 className="uk-cursor uk-text-truncate uk-margin-small-top" onClick={() => this.props.history.push('/device/'+this.props.item.id)}>
                            {device_name}
                        </h4>
                        <ul className="uk-list">
                            {this.renderPrice()}
                        </ul>
                        <div className={`uk-card-footer uk-padding-remove `+(this.props.mode !== 'compare' ? `uk-transition-slide-bottom-small`: '')}>
                            <div className="uk-grid uk-grid-collapse" uk-grid="true">
                                {this.renderBtn()}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const mapDispatchToProps = {
    addCart
}

const ConnectItem = connect(mapStateToProps, mapDispatchToProps)(Item);

export default withRouter(withStorage(ConnectItem));