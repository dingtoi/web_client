import React from "react";
import {SERVER_IMG} from "../../../config";
import {withRouter} from "react-router-dom";
class Ad extends React.Component{
    render(){
        return (
            <div>
                <div className="uk-child-width-1-2 uk-child-width-1-2@m uk-child-width-1-1@s uk-grid-match uk-grid-medium" uk-grid="true">
                    <div>
                        <div className="uk-inline-clip uk-transition-toggle uk-box-shadow-medium uk-cursor" onClick={() => this.props.history.push('/category?device=1')}>
                            <img className="uk-transition-scale-up uk-transition-opaque" src={SERVER_IMG+"ad-1.jpg"} alt="" />
                            <div className="uk-position-center-left uk-margin-left">
                                 <div className="uk-padding-small uk-background-third uk-border-compare">
                                    <h2 className="uk-h3 uk-text-white uk-text-uppercase uk-margin-remove uk-border">Smartphone</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="uk-inline-clip uk-transition-toggle uk-box-shadow-medium uk-cursor" onClick={() => this.props.history.push('/category?type=1')}>
                            <img className="uk-transition-scale-up uk-transition-opaque" src={SERVER_IMG+"ad-2.jpg"} alt="" />
                            <div className="uk-position-center-right uk-margin-right">
                                <div className="uk-padding-small uk-background-third uk-border-compare">
                                    <h2 className="uk-h3 uk-text-white uk-text-uppercase uk-margin-remove uk-border">Top sale</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Ad);