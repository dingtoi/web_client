import React from "react";
import { SERVER_DOMAIN, MICRO_DOMAIN, MICRO_DEVICE} from "../../../config";

import {connect} from "react-redux";
import {addCart} from "../../../src/containers/home/actions";

import UserModel from "../../../models/user";

import withStorage from "../../../hoc/storage";
import {withRouter} from "react-router-dom";

import {Cover, Spinner} from "../../../components/common";

class Item extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: false
        }
    }
    
    renderPrice(){
        const {type, sale_price, exchange_price, exchange_name} = this.props.item;
        if(type === 1){
            return (
                <React.Fragment>
                    <li>
                        <div className="uk-flex uk-flex-between">
                            <div className="uk-text-bold">Sale</div>
                            <div>{accounting.formatMoney(sale_price)}</div>
                        </div>
                    </li>
                </React.Fragment>
            )
        }else if(type === 2){
            return (
                <React.Fragment>
                    <li>
                        <div className="uk-flex uk-flex-between">
                            <div className="uk-text-bold">Exchange</div>
                            <div>{accounting.formatMoney(exchange_price)}</div>
                        </div>
                        <div className="uk-flex uk-text-meta uk-flex-middle uk-margin-xsmall-top">
                            <i className="fa fa-exchange"/>
                            <div className="uk-margin-xsmall-left uk-text-meta uk-text-truncate">Exchange with {exchange_name}</div>
                        </div>
                    </li>
                </React.Fragment>
            )
        }else if(type === 3){
            return (
                <React.Fragment>
                    <li>
                        <div className="uk-flex uk-flex-between">
                            <div className="uk-text-bold">Sale</div>
                            <div>{accounting.formatMoney(sale_price)}</div>
                        </div>
                    </li>
                    <li>
                        <div className="uk-flex uk-flex-between">
                            <div className="uk-text-bold">Exchange</div>
                            <div>{accounting.formatMoney(exchange_price)}</div>
                        </div>
                        <div className="uk-flex uk-text-meta uk-flex-middle uk-margin-xsmall-top">
                            <i className="fa fa-exchange"/>
                            <div className="uk-margin-xsmall-left uk-text-meta uk-text-truncate">Exchange With: {exchange_name}</div>
                        </div>
                    </li>
                </React.Fragment>
            )
        }
        else return null;
    }
    renderBtn(){
        const {type} = this.props.item;
        if(type === 1){
            return (
                <div className="uk-width-1-1">
                    <button className="uk-button uk-button-primary uk-width-1-1 uk-padding-remove"
                        onClick={() => this.handlerAddCart(1)} ref={instance => this.atcRef = instance}>Buy</button>
                </div>
            )
        }else if(type === 2){
            return (
                <div className="uk-width-1-1">
                    <button className="uk-button uk-button-secondary uk-width-1-1 uk-padding-remove"
                        onClick={() => this.handlerAddCart(2)} ref={instance => this.atcRef = instance}>Exchange</button>
                </div>
            )
        }else if(type === 3){
            return (
                <React.Fragment>
                    <div className="uk-width-1-2">
                        <button className="uk-button uk-button-primary uk-width-1-1 uk-padding-remove"
                            onClick={() => this.handlerAddCart(1)} ref={instance => this.atcRef = instance}>Buy</button>
                    </div>
                    <div className="uk-width-1-2">
                        <button className="uk-button uk-button-secondary uk-width-1-1 uk-padding-remove"
                            onClick={() => this.handlerAddCart(2)} ref={instance => this.atcRef = instance}>Exchange</button>
                    </div>
                </React.Fragment>
            )
        }else null;
    }
    render(){
        const {device_name, thumb} = this.props.item;
        return (
            <div className="uk-inline uk-width-1-1">
                {this.state.isLoading && <Cover/>}
                {this.state.isLoading && <Spinner/>}
                <div className="uk-transition-toggle">
                    <div className="uk-card uk-card-small uk-card-body uk-inline">
                        <div className="uk-flex-center">
                            {
                                thumb 
                                ?
                                <a onClick={() => this.props.history.push('/device/'+this.props.item.id)}>
                                    <img className="uk-img uk-cursor" src={MICRO_DOMAIN+MICRO_DEVICE+'/devices/'+thumb} uk-img="true" ref={instance => this.image = instance}/>
                                </a>
                                :
                                <a onClick={() => this.props.history.push('/device/'+this.props.item.id)} ref={instance => this.image = instance}>
                                    <img src={SERVER_DOMAIN+'no-image.png'}/>
                                </a>
                            }
                        </div>
                        <h4 className="uk-cursor uk-text-truncate uk-margin-small-top" onClick={() => this.props.history.push('/device/'+this.props.item.id)}>
                            {device_name}
                        </h4>
                        <ul className="uk-list">
                            {this.renderPrice()}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const mapDispatchToProps = {
    addCart
}

const ConnectItem = connect(mapStateToProps, mapDispatchToProps)(Item);

export default withRouter(withStorage(ConnectItem));