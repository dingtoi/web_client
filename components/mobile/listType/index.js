import React from "react";
import DeviceModel from "../../../models/device";
import withStorage from "../../../hoc/storage";
import Item from "../item";
import { Cover, Spinner } from "../../common";

import {connect} from "react-redux";

class ListType extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            list: [],
            type: 4,
            isLoading: false
        }
        this.handlerTabItem = this.handlerTabItem.bind(this);
    }
    componentDidMount(){
        this.refreshList();
        this.tab = UIkit.tab(this.tabRef);
    }
    componentWillReceiveProps(nextProps){
        this.refreshList();
    }
    componentWillUnmount(){
        this.tab.$destroy(true);
    }
    handlerTabItem(type){
        this.setState({type: type}, () => {
            this.refreshList();
        });
    }
    refreshList(){
        this.setState({isLoading: true});
        DeviceModel.getDeviceWithTypeMobile({email: this.props.auth, type: this.state.type})
        .then(result => {
            this.setState({list: result, isLoading: false});
        })
        this.tab = UIkit.tab(this.tabRef);
    }
    componentWillUnmount(){
        this.tab.$destroy(true);
    }
    handlerTabItem(type){
        this.setState({type: type}, () => {
            DeviceModel.getDeviceWithTypeMobile({email: this.props.auth, type: this.state.type})
            .then(result => {
                this.setState({list: result});
            })
        });
    }
    render(){
        return (
            <div>
                <div className="uk-margin-xsmall-top">
                    <ul uk-tab="true" className="uk-tab uk-child-width-expand" ref={instance => this.tabRef = instance}>
                        <li className="uk-active"><a onClick={() => this.handlerTabItem(4)}>All</a></li>
                        <li><a onClick={() => this.handlerTabItem(1)}>Sale</a></li>
                        <li><a onClick={() => this.handlerTabItem(2)}>Exchange</a></li>
                    </ul>
                </div>
                <div className="uk-inline uk-width-1-1">
                    {this.state.isLoading && <Cover/>}
                    {this.state.isLoading && <Spinner/>}
                    {
                        this.state.list.length > 0
                        ?
                        <div className="uk-position-relative" uk-slider="true">
                            <ul className="uk-slider-items uk-grid uk-grid-match">
                                {
                                    this.state.list.map((item, key) => {
                                        return (
                                            <li key={key} className="uk-width-1-4@l uk-width-1-3@m uk-width-1-2@s">
                                                <Item item={item}
                                                    onAddCart={() => this.refreshList()}/>
                                            </li>
                                        )
                                    })
                                }
                            </ul>
                        </div>
                        :
                        <div className="uk-placeholder uk-text-center">There are no items.</div>
                    }
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const ConnectListType = connect(mapStateToProps)(ListType);

export default withStorage(ConnectListType);