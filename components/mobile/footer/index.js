import React from "react";
import {SERVER_IMG} from "../../../config";
import {withRouter} from "react-router-dom";

class Footer extends React.Component{
    constructor(props){
        super(props);
    }
    componentDidMount(){
        if ($('.scroll').length) {
            var scrollTrigger = 100, // px
                backToTop = function () {
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > scrollTrigger) {
                        $('.scroll').addClass('show');
                    } else {
                        $('.scroll').removeClass('show');
                    }
                };
            backToTop();
            $(window).on('scroll', function () {
                backToTop();
            });
           
        }
    }
    render(){
        return (
            <footer className="uk-padding-small uk-margin-top">
                <div>
                    <p className="uk-text-bold uk-text-muted uk-text-small"><span className="uk-text-emphasis">Contact</span> Dingtoi customer service professionals by phone or email.</p>
                </div>
                <hr className="uk-divider-icon"/>
                <div>
                    <img src={SERVER_IMG+'canada.png'} width="18px"/>
                    <span className="uk-text-meta uk-text-primary uk-margin-xsmall-left">Canada</span>
                </div>
                <div>
                    <p className="uk-text-bold uk-text-muted uk-text-small">Copyright © 2017 Dingtoi Inc. All rights reserved.</p>
                </div>
                <div className="uk-flex uk-flex-wrap">
                    <div>
                        <a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-small-right" onClick={() => this.props.history.push('/privacy')}>Privacy</a>
                    </div>
                    <div><a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-small-right" onClick={() => this.props.history.push('/term-of-use')}>Terms of Use</a></div>
                    <div><a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-small-right" onClick={() => this.props.history.push('/sale-and-retunds')}>Sales and Retunds</a></div>
                    <div><a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-small-right" onClick={() => this.props.history.push('/legal')}>Legal</a></div>
                    <div><a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-small-right" onClick={() => this.props.history.push('/sitemap')}>Site Map </a></div>
                </div>
                <a className="uk-position-fixed scroll uk-button uk-button-primary uk-button-small" href="#target" uk-scroll="true" ><i className="fa fa-chevron-up" aria-hidden="true"></i></a>
            </footer>
        )
    }
}

export default withRouter(Footer);