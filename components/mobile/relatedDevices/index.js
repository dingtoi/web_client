import React from "react";
import { Cover, Spinner } from "../../common";
import DeviceModel from "../../../models/device";

import withStorage from "../../../hoc/storage";

import Item from "../item";

import {connect} from "react-redux";

class RelatedDevices extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            list: []
        }
    }
    componentDidMount(){
        this.refreshList();
    }
    componentWillReceiveProps(nextProps){
        this.refreshList();
    }
    refreshList(){
        this.setState({isLoading: true});
        DeviceModel.getDeviceRelated({id: this.props.device_id, email: this.props.auth})
        .then((result) => {
            this.setState({list: result, isLoading: false})
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    render(){
        return (
            <div>
                
                <div className="uk-inline uk-width-1-1">
                    {this.state.isLoading && <Cover/>}
                    {this.state.isLoading && <Spinner/>}
                    {
                        this.state.list.length > 0
                        ?
                        <div className="uk-position-relative" uk-slider="true">
                            <ul className="uk-slider-items uk-grid uk-grid-match">
                                {
                                    this.state.list.map((item, key) => {
                                        return (
                                            <li key={key} className="uk-width-1-4@l uk-width-1-3@m uk-width-1-2@s">
                                                <Item item={item}
                                                    onAddCart={() => {
                                                        this.refreshList();
                                                        if(is.function(this.props.onAddCart))
                                                            this.props.onAddCart(); 
                                                    }}/>
                                            </li>
                                        )
                                    })
                                }
                            </ul>
                        </div>
                        :
                        <div className="uk-placeholder uk-text-center">There are no items.</div>
                    }
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const ConnectFeatureDevices = connect(mapStateToProps)(RelatedDevices);

export default withStorage(ConnectFeatureDevices);