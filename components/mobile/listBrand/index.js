import React from "react";

import DeviceModel from "../../../models/device";
import {SERVER_IMG,MICRO_DOMAIN,MICRO_DEVICE} from "../../../config";

import {Spinner, Cover} from "../../../components/common";

import {withRouter} from "react-router-dom";

class ListBrand extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            list: []
        }
    }
    componentDidMount(){
        this.setState({isLoading: true});
        DeviceModel.getBrandList()
        .then(result => {
            this.setState({list: result, isLoading: false});
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    render(){
        return (
            <div>
                <div className="uk-inline uk-width-1-1">
                    {this.state.isLoading && <Cover/>}
                    {this.state.isLoading && <Spinner/>}
                    <div className="uk-container-extend uk-background-default">
                        <div className="uk-container uk-position-relative uk-visible-toggle uk-light" uk-slider="true">
                            <ul className="uk-slider-items uk-child-width-1-4 uk-flex uk-flex-middle uk-flex-center">
                            {
                                this.state.list.map((item, key) => {
                                    return (
                                        <a className="uk-padding-small uk-padding-remove-left uk-padding-remove-right" key={key}
                                            onClick={() => {
                                                this.props.history.push('/category?brand='+item.id)}
                                            }>
                                            <li className="uk-text-center">
                                                <img src={MICRO_DOMAIN+MICRO_DEVICE+"/brands/"+item.image} alt="" width="45px" />
                                            </li>
                                        </a>
                                    )
                                })
                            }
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(ListBrand);