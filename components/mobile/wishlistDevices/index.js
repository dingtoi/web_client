import React from "react";

import withStorage from "../../../hoc/storage";
import { Form, Field } from 'react-final-form';
import { Select2, Cover, Spinner, Button} from "../../../components/common";
import {connect} from "react-redux";
import Item from "../item";
import { MICRO_DEVICE, MICRO_DOMAIN } from "../../../config";
import axios from "axios";
import DeviceModel from "../../../models/device";
class WishlistDevices extends React.Component{
    constructor(props){
        super(props);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.handlerApiCategory = this.handlerApiCategory.bind(this);
        this.handlerChangeParentModel = this.handlerChangeParentModel.bind(this);
        this.handlerDeleteSearchItem = this.handlerDeleteSearchItem.bind(this);
        this.state = {
            isServerError: false,
            isLoading: false,
            list: [],
            handlerListSearch: [],
            listWL:[],
            isActive: 0
        }
        this.form = null;
        this.category = 0;
        this.brand = 0;
    }
   
    componentWillMount(){
        this.setState({isLoading: true});
    }
    componentDidMount(){
        this.refreshList();
       
        this.handlerApiCategory();
        this.handlerApiBrand();
        this.handlerApiRam();
        this.handlerApiCapacity();
        this.handlerApiColor();
        
        this.handlerListSearch();
    }
    componentWillReceiveProps(nextProps){
        this.refreshList();
    }
    refreshList(){
        this.setState({isLoading: true});
        DeviceModel.getDeviceWishlist({email: this.props.auth})
        .then((result) => {
            this.setState({list: result, isLoading: false})
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    componentWillUnmount(){
        this.form = null;
        this.category = 0;
        this.brand = 0; 
    }
    handlerSubmit(values){
        return new Promise((resolve, reject) => {
            DeviceModel.checkExitsSearch({category_id:values.category, brand_id:values.brand, model_id:values.model, capacity_id:values.capacity, color_id:values.color, ram_id:values.ram, email: this.props.auth})
            .then((result) => {
                if(Number(result) === 0){
                    DeviceModel.addSearchDevice({category_id:values.category, brand_id:values.brand, model_id:values.model, capacity_id:values.capacity, color_id:values.color, ram_id:values.ram, email: this.props.auth})
                    .then(result => {
                        this.setState({isLoading: false});
                        this.handlerListSearch();
                        this.form.reset();
                        resolve();
                    })
                    .catch(error => {
                        resolve();
                    })
                }else {
                    UIkit.notification({message: 'Search Exists', status: 'danger', pos: 'top-left'}); 
                    this.form.reset();
                    resolve();
                }
            })
            .catch(error => {
                resolve();
            })
        })
    }

    handlerListSearch(){
        this.setState({isLoading: true});
        DeviceModel.getSearchList({email: this.props.auth})
        .then((result) => {
            this.setState({handlerListSearch: result, isLoading: false})
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    handlerSearchWishList(item){
        this.setState({isLoading: true, isActive: item.id});
        DeviceModel.searchWishList({email: this.props.auth, id:item.id})
        .then(result => {
            this.setState({listWL: result, isLoading: false});
        })
        .catch((error) => {
            this.setState({isLoading: false});
        })
    }
    handlerDeleteSearchItem(item){ 
        this.setState({isLoading: true});
        DeviceModel.deleteSearchItem({id: item.id})
        .then(() => {
            this.setState({isLoading:false}, ()=>{
                if(this.state.isActive == item.id){
                    this.setState({isActive: 0});
                }
                this.handlerListSearch();
            })
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    handlerApiCategory(){
        this.categoryRef.beforeRefresh();
        axios.get(MICRO_DOMAIN+MICRO_DEVICE + '/api/category/list')
        .then(result => {
            this.categoryRef.refreshList(result);
        })
        .catch(error => {
            this.categoryRef.afterRefresh();
        })
    }
    handlerApiBrand(){
        this.brandRef.beforeRefresh();
        axios.get(MICRO_DOMAIN+MICRO_DEVICE + '/api/brand/list')
        .then(result => {
            this.brandRef.refreshList(result);
        })
        .catch(error => {
            this.brandRef.afterRefresh();
        })
    }
    handlerApiRam(){
        this.ramlRef.beforeRefresh();
        axios.get(MICRO_DOMAIN+MICRO_DEVICE + '/api/ram/list/')
        .then(result => {
            this.ramlRef.refreshList(result);
        })
        .catch(error => {
            this.ramRef.afterRefresh();
        })
    }
    handlerApiCapacity(){
        this.ramlRef.beforeRefresh();
        axios.get(MICRO_DOMAIN+MICRO_DEVICE + '/api/capacity/list/')
        .then(result => {
            this.capacitylRef.refreshList(result);
        })
        .catch(() => {
            this.capacitylRef.afterRefresh();
        })
    }
    handlerApiColor(){
        this.ramlRef.beforeRefresh();
        axios.get(MICRO_DOMAIN+MICRO_DEVICE + '/api/color/list/')
        .then(result => {
            this.colorlRef.refreshList(result);
        })
        .catch(error => {
            this.colorlRef.afterRefresh();
        })
    }
    handlerApiModel(){
        this.modelRef.beforeRefresh();
        axios.get(MICRO_DOMAIN+MICRO_DEVICE + '/api/model/list/'+this.category+'/'+this.brand)
        .then(result => {
            this.modelRef.refreshList(result);
        })
        .catch(error => {
            this.modelRef.afterRefresh();
        })
    }
    handlerChangeParentModel(fieldName, data, input){
        input.onChange(Number(data.id));
        this[fieldName] = Number(data.id);
        if(this.brand && this.category){
            this.modelRef.clearAll();
            this.handlerApiModel();
        }
    }
    renderMode(){
        let renderFields = null;
        renderFields = (
            <React.Fragment>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="category">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="category">Category</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <Select2 ref={instance => this.categoryRef = instance} {...input} 
                                    placeholder="Choose Category" id="category"
                                    onChange={data => this.handlerChangeParentModel('category', data, input)}
                                    />
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="brand">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="brand">Brand</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <Select2 ref={instance => this.brandRef = instance} {...input} 
                                    placeholder="Choose Brand" id="brand"
                                    onChange={data => this.handlerChangeParentModel('brand', data, input)}/>
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="model">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="model">Model</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <Select2 ref={instance => this.modelRef = instance} {...input} 
                                    placeholder="Choose Model" id="model"
                                    onChange={data => {
                                        input.onChange(Number(data.id))}
                                    }/>
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="capacity">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="capacity">Capacity</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <Select2 ref={instance => this.capacitylRef = instance} {...input} 
                                    placeholder="Choose Capacity" id="capacity"
                                    onChange={data => {
                                        input.onChange(Number(data.id))}
                                    }/>
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="color">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="color">Color</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <Select2 ref={instance => this.colorlRef = instance} {...input} 
                                    placeholder="Choose Color" id="color"
                                    onChange={data => {
                                        input.onChange(Number(data.id))}
                                    }/>
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin-remove">
                    <Field name="ram">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="ram">Ram</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <Select2 ref={instance => this.ramlRef = instance} {...input} 
                                    placeholder="Choose Ram" id="ram"
                                    onChange={data => {
                                        input.onChange(Number(data.id))}
                                    }/>
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
            </React.Fragment>
        )
        return (
            <div>
                <Form
                    onSubmit={this.handlerSubmit}
                    validate={values => {
                        const errors = {};
                            if(!values.category)
                                errors.category = "Category must be choose";
                            if(!values.brand)
                                errors.brand = "Brand must be choose";
                            if(!values.model)
                                errors.model = "Model must be choose";
                            if(!values.ram)
                                errors.ram = "Ram must be choose";
                            if(!values.capacity)
                                errors.capacity = "Capacity must be choose";
                            if(!values.color)
                                errors.color = "Color must be choose";
                        
                        return errors;
                    }}
                    render={({ submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                        this.form = form;
                        return <form onSubmit={handleSubmit}>
                            <div className="uk-inline uk-width-1-1">
                                {submitting && <Cover/>}
                                {submitting && <Spinner/>}
                                <div className="uk-grid" uk-grid="true">
                                    {renderFields}
                                    <div className="uk-width-1-1 uk-margin uk-flex uk-flex-right ">    
                                        <Button className="uk-width-auto" type="submit"  disabled={submitting || validating} color="primary">
                                            Save
                                        </Button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    }}/>
            </div>
        )
    }
    render(){
        return (
            <div>
                <div className="uk-container ">
                    <h3 className="uk-margin-top">Find device exactly</h3>
                    {this.renderMode()}
                    <div className="uk-inline uk-width-1-1">
                    {this.state.isLoading && <Cover/>}
                    {this.state.isLoading && <Spinner/>}
                        <div className="uk-grid uk-grid-small uk-overflow-auto uk-margin" uk-grid="true" style={{maxHeight:'230px'}}>
                        {
                            this.state.handlerListSearch.map((item, key) => {
                                return (
                            <div key={key}>
                                <div className={"uk-xsmall-padding uk-text-white uk-background-secondary uk-inline uk-text-small "+(this.state.isActive == item.id ? 'uk-active-wishlist': '')} 
                                item={item} style={{paddingRight:'20px'}}>
                                    <a className="uk-position-top-right uk-background-close uk-link-reset" uk-icon="icon: close" onClick={() => this.handlerDeleteSearchItem(item)}></a>
                                    <div onClick={() => this.handlerSearchWishList(item)}>
                                        <span>{item.category_name}</span> - <span>{item.brand_name}</span> -  
                                        <span> {item.model_name}</span> - <span>{item.capacity_name}</span> - <span>{item.color_name}</span> - <span>{item.ram_name}</span> 
                                    </div>
                                </div>
                            </div>
                                )
                            })
                        }
                        </div>
                    </div>
                </div>
                {
                    this.state.isActive > 0
                    ?
                    <div>
                        <div className="uk-box-shadow-medium uk-text-uppercase uk-height-small uk-flex uk-flex-middle uk-margin-bottom">
                            <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Result Search Wishlist</div>
                        </div>
                        <div className="uk-inline uk-width-1-1">
                            {this.state.isLoading && <Cover/>}
                            {this.state.isLoading && <Spinner/>}
                            {
                                this.state.listWL.length > 0
                                ?
                                <div className="uk-position-relative" uk-slider="true">
                                    <ul className="uk-slider-items uk-grid uk-grid-match">
                                        {
                                            this.state.listWL.map((item, key) => {
                                                return (
                                                    <li key={key} className="uk-width-1-4@l uk-width-1-3@m uk-width-1-2@s">
                                                        <Item item={item} onAddCart={() => this.handlerSearchWishList(item)}/>
                                                    </li>
                                                )
                                            })
                                        }
                                    </ul>
                                </div>
                                :
                                <div className="uk-placeholder uk-text-center">There are no items.</div>
                            }
                        </div>
                    </div>
                    :
                    null
                }
                <div className="uk-box-shadow-medium uk-text-uppercase uk-height-small uk-flex uk-flex-middle uk-margin-bottom">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Wishlist Devices</div>
                </div>
                <div className="uk-inline uk-width-1-1">
                    {this.state.isLoading && <Cover/>}
                    {this.state.isLoading && <Spinner/>}
                    {
                        this.state.list.length > 0
                        ?
                        <div className="uk-position-relative" uk-slider="true">
                            <ul className="uk-slider-items uk-grid uk-grid-match">
                                {
                                    this.state.list.map((item, key) => {
                                        return (
                                            <li key={key} className="uk-width-1-4@l uk-width-1-3@m uk-width-1-2@s">
                                                <Item item={item} onAddCart={() => this.refreshList()}/>
                                            </li>
                                        )
                                    })
                                }
                            </ul>
                        </div>
                        :
                        <div className="uk-placeholder uk-text-center">There are no items.</div>
                    }
                </div>
            </div>
        )
    }
}
function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const ConnectWishlistDevices = connect(mapStateToProps)(WishlistDevices);

export default withStorage(ConnectWishlistDevices);