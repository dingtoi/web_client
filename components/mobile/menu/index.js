import React from "react";
import {withRouter} from "react-router-dom";
import withStorage from "../../../hoc/storage";
import DeviceModel from "../../../models/device";
import {addCart} from "../../../src/containers/home/actions";
import {connect} from "react-redux";

import CartModel from "../../../models/cart";

class Menu extends React.Component{
    constructor(props){
        super(props);
        this.navigateToHome = this.navigateToHome.bind(this);
        this.navigateToLogin = this.navigateToLogin.bind(this);
        this.navigateToRegistration = this.navigateToRegistration.bind(this);
        this.navigateToAccount = this.navigateToAccount.bind(this);
        this.navigateToAccountInfo = this.navigateToAccountInfo.bind(this);
        this.navigateToChangePassword = this.navigateToChangePassword.bind(this);
        this.navigateToOrder = this.navigateToOrder.bind(this);
        this.navigateToTransaction = this.navigateToTransaction.bind(this);
        this.navigateToCart = this.navigateToCart.bind(this);
        this.handlerLogout = this.handlerLogout.bind(this);
        this.navigateToCategory = this.navigateToCategory.bind(this);
        this.navigateToSupport = this.navigateToSupport.bind(this);
        this.navigateToType = this.navigateToType.bind(this);
        this.state = {
            count: 0,
            isLoading :false,
            list:[],
            device_name: ''
        }
        this.__unmount = false;
        this.searchObj = null;
    }
    componentDidMount(){
        this.__unmount = false;
        this.refreshListCart();

        const $element = $('#toggle');
        UIkit.toggle($element, {
            target: '#toggle-submenu'
        }).toggle();
        const $icon = $('.icon-toggle');
        $('#toggle-submenu').on('show', ()=>{
            $icon.removeClass('fa-chevron-down').addClass('fa-chevron-left');
        });
        $('#toggle-submenu').on('hide', ()=>{
            $icon.removeClass('fa-chevron-left').addClass('fa-chevron-down');
        });

        if(!this.props.auth || !(this.props.auth && this.props.anonymous))
        {
            const $element2 = $('#toggle2');
            UIkit.toggle($element2, {
                target: '#toggle-submenu2'
            }).toggle();
            const $icon2 = $('.icon-toggle2');
            $('#toggle-submenu2').on('show', ()=>{
                $icon2.removeClass('fa-chevron-down').addClass('fa-chevron-left');
            });
            $('#toggle-submenu2').on('hide', ()=>{
                $icon2.removeClass('fa-chevron-left').addClass('fa-chevron-down');
            });
        }
        this.handlerAutocomplete();
        $('#offcanvas-slide').on('hidden', () => {
            this.handlerHideSearch();
           
        });
    }
    handlerShowSearch(){
        $(this.showSearch).show();
        $(this.hideMenu).hide(); 
        $(this.search).focus();
    }
    handlerHideSearch(){
        $(this.showSearch).hide();
        $(this.hideMenu).show();
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.count){
            this.refreshListCart();
        }
    }
    componentWillUnmount(){
        this.__unmount = true;
        $('#offcanvas-slide').remove();
        this.searchObj = null;
        this.reset();
    }
    handlerAutocomplete(){
        this.searchObj = $(this.search).flexdatalist({
            cache: false
        });
       
        this.handlerChangeAuto();
    }
    handlerChangeAuto(){
        $(this.search).on('change:flexdatalist', (event, set, options) => {
            this.setState({device_name: set.value}, () => {
                if(set.value.length > 1){
                    this.setState({isLoading:true});
                    DeviceModel.getSearchByName({device_name:this.state.device_name, email: this.props.auth})   
                    .then((list)=>{
                        this.setState({isLoading:false,list:list},()=>{
                        });
                    })
                    .catch(() => {
                        this.setState({ isLoading: true });
                    })
                }
            });
        },
        );
        $(this.search).on('select:flexdatalist', (event, set, options) => {
           this.props.history.push('/device/'+set.value);
           UIkit.offcanvas($('#offcanvas-slide')).hide();
           this.reset();
        });
    }
    reset(){
        this.setState({list: [], device_name: ''}, () => {
            $(this.search).flexdatalist('reset');
            this.handlerChangeAuto();
            $(this.search).val('');
           
        });
    }
    handlerSearch(){
        this.setState({isLoading: true});
        setTimeout(() => {
            if(is.not.undefined(this.props.onClose))
                this.props.onClose();
            if($(this.search).val()==''){
                this.props.history.push('/');
                UIkit.offcanvas($('#offcanvas-slide')).hide();
            }
            else{
                this.props.history.push('/search/'+this.state.device_name);
                UIkit.offcanvas($('#offcanvas-slide')).hide();
                this.reset();
            }
        }, 300);
        setTimeout(() => {
            this.setState({isLoading: false});
        }, 700);
    }
    refreshListCart(){
        setTimeout(() => {
            if(!this.__unmount){
                if(this.props.auth){
                    CartModel.getListCartByUser({email: this.props.auth})
                    .then((list) => {
                        this.setState({count: list.length});
                    })
                    .catch(() => {
                        this.setState({count: 0});
                    })
                }else{
                    this.setState({count: 0});
                }
            }
        }, 500)
    }
    navigateToHome(){
        UIkit.offcanvas($('#offcanvas-slide')).hide();
        setTimeout(() => {
            this.props.history.push('/');
        }, 100);
    }
    navigateToType(type){
        UIkit.offcanvas($('#offcanvas-slide')).hide();
        setTimeout(() => {
            this.props.history.push('/category?type='+type);
        }, 100);
    }
    navigateToLogin(){
        UIkit.offcanvas($('#offcanvas-slide')).hide();
        setTimeout(() => {
            this.props.history.push('/login');
        }, 100);
    }
    navigateToRegistration(){
        UIkit.offcanvas($('#offcanvas-slide')).hide();
        setTimeout(() => {
            this.props.history.push('/register');
        }, 100);
    }
    navigateToAccount(){
        UIkit.offcanvas($('#offcanvas-slide')).hide();
        setTimeout(() => {
            this.props.history.push('/account/device');
        }, 100);
        
    }
    navigateToAccountInfo(){
        UIkit.offcanvas($('#offcanvas-slide')).hide();
        setTimeout(() => {
            this.props.history.push('/account/information');
        }, 100);
        
    }
    navigateToChangePassword(){
        UIkit.offcanvas($('#offcanvas-slide')).hide();
        setTimeout(() => {
            this.props.history.push('/account/change_password');
        }, 100);
        
    }
    navigateToOrder(){
        UIkit.offcanvas($('#offcanvas-slide')).hide();
        setTimeout(() => {
            this.props.history.push('/account/order');
        }, 100);
        
    }
    navigateToSupport(){
        UIkit.offcanvas($('#offcanvas-slide')).hide();
        setTimeout(() => {
            this.props.history.push('/how-it-work');
        }, 100);
        
    }
    navigateToTransaction(){
        UIkit.offcanvas($('#offcanvas-slide')).hide();
        setTimeout(() => {
            this.props.history.push('/account/transactions');
        }, 100);
        
    }
    navigateToCart(){
        this.props.history.push('/cart');
    }
    handlerLogout(){
        localStorage.removeItem('email');
        localStorage.removeItem('anonymous');
        this.props.addCart();
        UIkit.offcanvas($('#offcanvas-slide')).hide();
        setTimeout(() => {
            this.props.history.push('/login');
            UIkit.notification({message: 'Logout Successfully !', status: 'primary', pos: 'top-left'});
            this.props.app.setState({isMenu: false}, () => {
                this.props.app.setState({isMenu: true}, () => {
                });
            });
        }, 500);
    }
    navigateToCategory(type){
        UIkit.offcanvas($('#offcanvas-slide')).hide();
        setTimeout(() => {
            this.props.history.push('/category?device='+type);
        }, 500);
    }
    render(){
        return (
            <nav uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky;" className="uk-height-small uk-navbar-container" uk-navbar="true">
                <div className="uk-navbar-left">
                    <a uk-toggle="target: #offcanvas-slide" className="uk-navbar-toggle uk-padding-small" uk-navbar-toggle-icon=""></a>
                    <div id="offcanvas-slide" uk-offcanvas="overlay: true">
                        <div className="uk-offcanvas-bar uk-padding-remove">
                            <div className="uk-flex uk-flex-column" ref={instance => this.hideMenu = instance}>
                                <div className="uk-height-small"/>
                                <a className="uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding"
                                    onClick={this.navigateToHome}>
                                    <i className="uk-margin-right fa fa-home" aria-hidden="true"></i>
                                    <span className="">Home</span>
                                </a>
                                <div>
                                    <a className="uk-inline uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding toggle-submenu" id="toggle">
                                        <i className="uk-margin-right fa fa-check-circle" aria-hidden="true"></i>
                                        <span>Your purpose</span>
                                        <i className="uk-position-center-right uk-margin-right fa fa-chevron-down icon-toggle" aria-hidden="true"></i>
                                    </a>
                                    <div className="uk-padding uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-right" id="toggle-submenu">
                                        <a className="uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding"
                                            onClick={() => this.navigateToType(1)}>
                                            <i className="uk-margin-right fa fa-tag" aria-hidden="true"></i>
                                            <span>Sale</span>
                                        </a>
                                        <a className="uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding"
                                            onClick={() => this.navigateToType(1)}>
                                            <i className="uk-margin-right fa fa-shopping-cart" aria-hidden="true"></i>
                                            <span>Buy</span>
                                        </a>
                                        <a className="uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding"
                                            onClick={() => this.navigateToType(2)}>
                                            <i className="uk-margin-right fa fa-exchange" aria-hidden="true"></i>
                                            <span>Exchange</span>
                                        </a>
                                    </div>
                                </div>
                                <a className="uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding"
                                    onClick={() => this.navigateToCategory(1)}>
                                    <i className="uk-margin-right fa fa-mobile-phone" aria-hidden="true"></i>
                                    <span>Smart Phones</span>
                                </a>
                                <a className="uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding"
                                    onClick={() => this.navigateToCategory(2)}>
                                    <i className="uk-margin-right fa fa-tablet" aria-hidden="true"></i>
                                    <span>Tablets</span>
                                </a>
                                <a className="uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding"
                                    onClick={() => this.navigateToCategory(3)}>
                                    <i className="uk-margin-right fa fa-clock-o" aria-hidden="true"></i>
                                    <span>Smart Watches</span>
                                </a>
                                <a className="uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding" onClick={this.navigateToSupport}>
                                    <i className="uk-margin-right fa fa-question-circle-o" aria-hidden="true"></i>
                                    <span>Support</span>
                                </a>
                                {
                                    !this.props.auth || (this.props.auth && this.props.anonymous)
                                    ? <a className="uk-inline uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding"
                                        onClick={this.navigateToRegistration}>
                                        <i className="uk-margin-right fa fa-user-plus" aria-hidden="true"></i>
                                        <span>Registration</span>
                                        <i className="uk-position-center-right uk-margin-right fa fa-star" aria-hidden="true"></i>
                                    </a>
                                    : null
                                }
                                {
                                    !this.props.auth || (this.props.auth && this.props.anonymous)
                                    ? <a className="uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding"
                                        onClick={this.navigateToLogin}>
                                        <i className="uk-margin-right fa fa-sign-out" aria-hidden="true"></i>
                                        <span>Login</span>
                                    </a>
                                    : null
                                }
                                {
                                    this.props.auth && !this.props.anonymous
                                    ? 
                                    <div>
                                    <a className="uk-inline uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding toggle-submenu" id="toggle2">
                                        <i className="uk-margin-right fa fa-user" aria-hidden="true"></i>
                                        <span>My Account</span>
                                        <i className="uk-position-center-right uk-margin-right fa fa-chevron-down icon-toggle2" aria-hidden="true"></i>
                                    </a>
                                    
                                    <div className="uk-padding uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-right" id="toggle-submenu2">
                                        <a className="uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding" onClick={this.navigateToAccount}>
                                            <i className="uk-margin-right fa fa-list-ul" aria-hidden="true"></i>
                                            <span>Device List</span>
                                        </a>
                                        <a className="uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding" onClick={this.navigateToAccountInfo}>
                                            <i className="uk-margin-right fa fa-info-circle" aria-hidden="true"></i>
                                            <span>Account Information</span>
                                        </a>
                                        <a className="uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding" onClick={this.navigateToChangePassword}>
                                            <i className="uk-margin-right fa fa-arrows-h" aria-hidden="true"></i>
                                            <span>Change Password</span>
                                        </a>
                                        <a className="uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding" onClick={this.navigateToOrder}>
                                            <i className="uk-margin-right fa fa-history" aria-hidden="true"></i>
                                            <span>Order History</span>
                                        </a>
                                        <a className="uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding" onClick={this.navigateToTransaction}>
                                            <i className="uk-margin-right fa fa-retweet" aria-hidden="true"></i>
                                            <span>Transaction</span>
                                        </a>
                                    </div>
                                    </div>
                                    
                                    : null
                                }
                                {
                                    this.props.auth && !this.props.anonymous
                                    ? <a className="uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding"
                                        onClick={this.handlerLogout}>
                                        <i className="uk-margin-right fa fa-sign-out" aria-hidden="true"></i>
                                        <span>Logout</span>
                                    </a>
                                    : null
                                }
                                <div className="uk-height-small uk-flex uk-flex-middle uk-link-reset uk-padding">
                                    <div className="uk-inline uk-width-1-1">
                                    <span className="uk-form-icon" uk-icon="icon: search"/>
                                        <input onClick={this.handlerShowSearch.bind(this)}  className="uk-input" type="search" placeholder="Search..."/>
                                    </div>
                                        
						        </div>
                            </div>
                            <div ref={instance => this.showSearch = instance} style={{display:'none'}} className="uk-padding-small uk-padding-remove-top uk-padding-remove-bottom">
                                <div onClick={this.handlerHideSearch.bind(this)} className="uk-flex uk-flex-center uk-height-small">
                                    <span uk-icon="icon: chevron-up;ratio:2" className="uk-text-white"></span>
                                </div>
                                <div>
                                    <div className="uk-flex">
                                        <input className="uk-input" autoFocus ref={instance => this.search = instance}  type="search" placeholder="Search..."  list="languages"/>
                                        <button className="uk-button uk-button-default uk-button-search uk-padding-small uk-padding-remove-top uk-padding-remove-bottom" type="submit" 
                                        onClick={this.handlerSearch.bind(this)}><span uk-icon="icon:search"></span></button>
                                        <datalist id="languages">
                                        {
                                            this.state.list.map((item, key) => {
                                                return (
                                                    <option key={key} value={item.id} >{item.device_name} - {item.ram_name} - {item.color_name}</option>
                                                )
                                            })
                                        }
                                        </datalist>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="uk-navbar-center">
                    <a onClick={this.navigateToHome} className="uk-padding-small uk-navbar-item uk-logo"><i className="uk-text-white fa fa-refresh" aria-hidden="true"></i></a>
                </div>
                <div className="uk-navbar-right">
                    <a onClick={this.navigateToCart} className=" uk-padding-small uk-navbar-item uk-logo">
                        <i className="uk-text-white uk-inline fa fa-shopping-bag no-ripple" aria-hidden="true">
                            {
                                this.state.count > 0
                                ?
                                <span className="uk-badge uk-position-top-right">{this.state.count}</span>
                                : null
                            }
                        </i>
                    </a>
                </div>
            </nav>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const mapDispatchToProps = {
    addCart
}

const ConnectMenu = connect(mapStateToProps, mapDispatchToProps)(Menu);

export default withRouter(withStorage(ConnectMenu));