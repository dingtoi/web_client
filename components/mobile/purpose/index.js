import React from "react";
import {SERVER_IMG} from "../../../config";
import {withRouter} from "react-router-dom";
class Purpose extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div className="browse-by-propose uk-margin-bottom">
                <div className="uk-box-shadow-medium uk-height-small uk-flex uk-flex-middle">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Browse by propose</div>
                </div>
                <div className="uk-child-width-1-3 uk-grid uk-grid-small uk-text-center uk-padding-small" uk-grid="">
                    <div className="uk-card">
                        <div className="uk-border-propose" onClick={() => this.props.history.push('/category?type=1')}>
                            <img data-src={SERVER_IMG+"2.jpg"}  alt="" uk-img="true"/>
                            <span className="uk-text-primary uk-text-large">Buy</span>
                        </div>
                    </div>
                    <div className="uk-card">
                        <div className="uk-border-propose" onClick={() => this.props.history.push('/category?type=1')}>
                            <img data-src={SERVER_IMG+"3.jpg"}  alt="" uk-img="true"/>
                            <span className="uk-text-primary uk-text-large">Sale</span>
                        </div>
                    </div>
                    <div className="uk-card">
                        <div className="uk-border-propose" onClick={() => this.props.history.push('/category?type=2')}>
                            <img data-src={SERVER_IMG+"1.jpg"}  alt="" uk-img="true"/>
                            <span className="uk-text-primary uk-text-large">Exchange</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Purpose);