import React from "react";
import { Cover, Spinner, Radio } from "../../../components/common";
import { SERVER_DOMAIN, MICRO_DEVICE, MICRO_DOMAIN } from "../../../config";
import DeviceModel from "../../../models/device";

import withStorage from "../../../hoc/storage";

class MyDevices extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            list: [],
            exchange_price: '',
            selectedItem: {}
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.selectedDevice)
            this.setState({selectedItem: nextProps.selectedDevice});
    }
    componentDidMount(){
        this.refreshList();
    }
    refreshList(){
        this.setState({isLoading: true});
        DeviceModel.getDeviceListByUser({email: this.props.auth})
        .then((result) => {
            this.setState({isLoading: false, list: result.list});
        })
        .catch(() => this.setState({isLoading: false}))
    }
    render(){
        return (
            <div className="uk-container uk-margin-large-top uk-margin-large-bottom">
                <h3>My Devices</h3>
                <div className="uk-inline uk-width-1-1 uk-margin-top uk-position-relative">
                    {this.state.isLoading && <Cover/>}
                    {this.state.isLoading && <Spinner/>}
                    {
                        this.state.list.length > 0
                        ? <div className="uk-position-relative" uk-slider="true">
                            <ul className="uk-slider-items uk-grid uk-grid-match">
                                {
                                    this.state.list.map((item, key) => {
                                        return (
                                            <li key={key} className="uk-width-1-4@l uk-width-1-3@m uk-width-1-2@s">
                                                <div className={
                                                    `uk-card uk-card-small uk-card-body uk-inline uk-cursor${this.state.selectedItem.id === item.id ? ' uk-background-secondary uk-light' : ''}`
                                                    }
                                                    onClick={() => {
                                                        this.setState({selectedItem: item}, () => {
                                                            this.props.onSelectItem(item)
                                                        })
                                                    }}>
                                                    <div className="uk-flex-center">
                                                        {
                                                            item.thumb
                                                            ?
                                                            <a>
                                                                <img className="uk-img uk-cursor" src={MICRO_DOMAIN+MICRO_DEVICE + '/devices/'+item.thumb} uk-img="true"/>
                                                            </a>
                                                            :
                                                            <a>
                                                                <img src={SERVER_DOMAIN+'no-image.png'}/>
                                                            </a>
                                                        }
                                                    </div>
                                                    <h4 className="uk-cursor uk-text-truncate">
                                                        {item.model_name}
                                                    </h4>
                                                </div>
                                            </li>
                                        )
                                    })
                                }
                            </ul>
                        </div>
                        : (
                            <div className="uk-placeholder uk-text-center">There is no items.</div>
                        )
                    }
                </div>
            </div>
        )
    }
}

export default withStorage(MyDevices);