import React from "react";
import { Cover, Spinner, InputNumber } from "../../../components/common";
import { SERVER_DOMAIN , MICRO_DEVICE, MICRO_DOMAIN, MICRO_EVENT} from "../../../config";
import CartModel from "../../../models/cart";
import axios from "axios";

import withStorage from "../../../hoc/storage";

import MyDevices from "../../../components/mobile/proposal/myDevices";
import Functions from "../../../functions";

class ExchangeProposal extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            detail: {},
            exchange_price: '',
            device: {}
        }
    }
    componentDidMount(){
        this.refreshDetail();
    }
    refreshDetail(){
        this.setState({isLoading: true});
        CartModel.getItemCart({id: this.props.selectedCart.id})
        .then((result) => {
            this.setState({isLoading: false, detail: result});
        })
        .catch(() => this.setState({isLoading: false}))
    }
    handlerSubmit(){
        if(is.empty(this.state.exchange_price))
            UIkit.notification({message: 'Must Choose Exchange Price', status: 'danger', pos: 'top-left'});
        else if(!is.number(Number(this.state.exchange_price)))
            UIkit.notification({message: 'Exchange Price Must Be Number', status: 'danger', pos: 'top-left'});
        else if(is.null(this.state.device))
            UIkit.notification({message: 'Must Select Exchange Device', status: 'danger', pos: 'top-left'});
        else{
            this.setState({isLoading: true});
            axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/add',{name: 'Add proposal', key: 'ADD_PROPOSAL', desc: ''})
            .then((created) => {
                CartModel.addProposal({email: this.props.auth, cart_id: this.state.detail.id, type: 2,
                    sale_proposal_price: null, exchange_proposal_device: this.state.device.id, exchange_proposal_price: this.state.exchange_price})
                .then(() => {
                    UIkit.notification({message: 'Add Proposal Successful', status: 'primary', pos: 'top-left'});
                    Functions.socketSendProposal(this.props.socket, this.props.selectedCart.received_email);
                    this.props.onSuccess();
                })
                .then(()=>{
                    axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/changeStatus',{id: created})
                    .then(() => { })
                    .catch(() => {})
                })
                .catch(() => {
                    this.setState({isLoading: false});
                })
            })
            .catch(() => {}) 
        }
    }
    handlerSelectExchange(item){
        this.setState({device: item});
    }
    render(){
        return (
            <div className="uk-container uk-margin-large-top uk-margin-large-bottom">
                <h3>Create Exchange Proposal</h3>
                <div className="uk-inline uk-width-1-1 uk-margin-top">
                    {this.state.isLoading && <Cover/>}
                    {this.state.isLoading && <Spinner/>}
                    <div className="uk-grid uk-flex uk-flex-top" uk-grid="true">
                        <div className="uk-width-1-3@l uk-width-1-2@m uk-with-1-1@s">
                            <div className="uk-flex uk-flex-column">
                                {
                                    this.state.detail.thumb
                                    ?
                                    <div className="uk-flex uk-flex-center uk-margin-bottom">
                                        <div>
                                            <img className="uk-img uk-cursor" src={MICRO_DOMAIN+MICRO_DEVICE + '/devices/'+this.state.detail.thumb} uk-img="true"
                                                style={{maxHeight: '120px'}}/>
                                        </div>
                                    </div>
                                    :
                                    <div className="uk-flex uk-flex-center uk-margin-bottom">
                                        <div>
                                            <img src={SERVER_DOMAIN+'no-image.png'} style={{maxHeight: '120px'}}/>
                                        </div>
                                    </div>
                                }
                                <div className="uk-margin-top uk-margin-bottom uk-flex uk-flex-center uk-text-meta">Exchange With</div>
                                {
                                    this.state.device.thumb
                                    ?
                                    <div className="uk-flex uk-flex-center">
                                        <div>
                                            <img src={MICRO_DOMAIN+MICRO_DEVICE + '/devices/'+this.state.device.thumb} style={{maxHeight: '120px'}}/>
                                        </div>
                                    </div>
                                    :
                                    <div className="uk-flex uk-flex-center">
                                        <div>
                                            <img src={SERVER_DOMAIN+'no-image.png'} style={{maxHeight: '120px'}}/>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                        <div className="uk-width-2-3@l uk-width-1-2@m uk-with-1-1@s">
                            <h2>{this.state.detail.device_name}</h2>
                            <div className="uk-grid uk-margin" uk-grid="true">
                                <div className="uk-width-1-1">
                                    <ul className="uk-list">
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Brand:</div>
                                                <span>{this.state.detail.brand_name}</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Color:</div>
                                                <span>{this.state.detail.color_name}</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Capacity:</div>
                                                <span>{this.state.detail.capacity_name}</span>
                                            </div>
                                        </li>
                                    </ul>
                                    <div className="uk-flex uk-flex-column uk-margin uk-width-medium">
                                        <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="proposal_exchange_price">Proposal Exchange Price</label>
                                        <InputNumber value={this.state.exchange_price} icon={"credit-card"}
                                            onChange={event => this.setState({exchange_price: event.target.value})}/>
                                    </div>
                                    <div>
                                        <button className="uk-button uk-button-primary"
                                            onClick={this.handlerSubmit.bind(this)}>Create Exchange Proposal</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <MyDevices onSelectItem={this.handlerSelectExchange.bind(this)}/>
                </div>
            </div>
        )
    }
}

export default withStorage(ExchangeProposal);