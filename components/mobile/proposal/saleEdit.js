import React from "react";
import { Cover, Spinner, InputNumber } from "../../../components/common";
import { SERVER_DOMAIN, MICRO_DEVICE, MICRO_DOMAIN, MICRO_EVENT } from "../../../config";
import CartModel from "../../../models/cart";
import axios from "axios";

import withStorage from "../../../hoc/storage";
import Functions from "../../../functions";

class SaleEditProposal extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            detail: {},
            sale_price: ''
        }
    }
    componentDidMount(){
        this.refreshDetail();
    }
    refreshDetail(){
        this.setState({isLoading: true});
        CartModel.getItemCart({id: this.props.selectedCart.id})
        .then((result) => {
            this.setState({isLoading: false, detail: result}, () => {
                setTimeout(() => {
                    this.setState({sale_price: this.props.selectedCart.sale_proposal_price});
                }, 500)
            });
        })
        .catch(() => this.setState({isLoading: false}))
    }
    handlerSubmit(){
        if(is.empty(this.state.sale_price))
            UIkit.notification({message: 'Must Choose Price', status: 'danger', pos: 'top-left'});
        else{
            this.setState({isLoading: true});
            axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/add',{name: 'Edit Proposal', key: 'EDIT_PROPOSAL', desc: ''})
            .then((created) => {
                CartModel.editProposal({email: this.props.auth, cart_id: this.state.detail.id, type: 1,
                    sale_proposal_price: this.state.sale_price, exchange_proposal_device: null, exchange_proposal_price: null,
                    id: this.props.selectedCart.proposal_id})
                .then(() => {
                    UIkit.notification({message: 'Edit Proposal Successful', status: 'primary', pos: 'top-left'});
                    Functions.socketSendProposal(this.props.socket, this.props.selectedCart.received_email);
                    this.props.onSuccess();
                })
                .then(()=>{
                    axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/changeStatus',{id: created})
                    .then(() => {
                    })
                    .catch(() => {
                })
                })
                .catch(() => {
                    this.setState({isLoading: false});
                })
            })
            .catch(() => {}) 
        }
    }
    render(){
        return (
            <div className="uk-container uk-margin-large-top uk-margin-large-bottom">
                <h3>Edit Sale Proposal</h3>
                <div className="uk-inline uk-width-1-1 uk-margin-top">
                    {this.state.isLoading && <Cover/>}
                    {this.state.isLoading && <Spinner/>}
                    <div className="uk-grid uk-flex uk-flex-top" uk-grid="true">
                        <div className="uk-width-1-2">
                        {
                            this.state.detail.thumb
                            ?

                                <div>
                                    <img className="uk-img uk-cursor" src={MICRO_DOMAIN+MICRO_DEVICE + '/devices/'+this.state.detail.thumb} uk-img="true"
                                        style={{maxHeight: '290px'}}/>
                                </div>

                            :

                                <div>
                                    <img src={SERVER_DOMAIN+'no-image.png'} style={{maxHeight: '290px'}}/>
                                </div>

                        }
                        </div>
                        <div className="uk-width-1-2">
                            <h2>{this.state.detail.device_name}</h2>
                            <div className="uk-grid uk-margin" uk-grid="true">
                                <div className="uk-width-1-1">
                                    <ul className="uk-list">
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Brand:</div>
                                                <span>{this.state.detail.brand_name}</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Color:</div>
                                                <span>{this.state.detail.color_name}</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Capacity:</div>
                                                <span>{this.state.detail.capacity_name}</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="uk-flex uk-flex-column uk-margin">
                        <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="proposal_sale_price">Proposal Sale Price</label>
                        <InputNumber value={this.state.sale_price} icon={"credit-card"}
                            onChange={event => this.setState({sale_price: event.target.value})}/>
                    </div>
                    <div>
                        <button className="uk-button uk-button-primary"
                            onClick={this.handlerSubmit.bind(this)}>Edit Sale Proposal</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default withStorage(SaleEditProposal);