import React from "react";
import {withRouter} from "react-router-dom"; 
import withStorage from "../../../hoc/storage";

import {addCart} from "../../../src/containers/home/actions";
import {connect} from "react-redux";

class QuickAccess extends React.Component{
    render(){
        return (
            <div>
                <div className="uk-box-shadow-medium uk-text-uppercase uk-height-small uk-flex uk-flex-middle">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Quick Access</div>
                </div>
                <div className="uk-child-width-1-2 uk-grid uk-grid-small uk-text-center uk-padding-small" uk-grid="">
                    <div className="uk-card">
                        <a className="uk-link-reset" onClick={() => this.props.history.push('/category')}>
                            <div className="uk-quick-access-item uk-text-muted uk-text-small">
                                <div><span uk-icon="icon:phone;ratio:3.5"></span></div>
                                <span>Product</span>
                            </div>
                        </a>
                    </div>
                   
                    {
                        !this.props.auth || (this.props.auth && this.props.anonymous)
                        ?
                        <div className="uk-card">
                        <a className="uk-link-reset" onClick={() => this.props.history.push('/login')}>
                            <div className="uk-quick-access-item uk-text-muted uk-text-small">
                                <div><span uk-icon="icon:user;ratio:3.5"></span></div>
                                <span>My Account</span>
                            </div>
                        </a>
                    </div>
                        : 
                        <div className="uk-card">
                        <a className="uk-link-reset" onClick={() => this.props.history.push('/account/device')}>
                            <div className="uk-quick-access-item uk-text-muted uk-text-small">
                                <div><span uk-icon="icon:user;ratio:3.5"></span></div>
                                <span>My Account</span>
                            </div>
                        </a>
                    </div>
                    }
                    <div className="uk-card">
                        <a className="uk-link-reset" onClick={() => this.props.history.push('/about-us')}>
                            <div className="uk-quick-access-item uk-text-muted uk-text-small">
                                <div><span uk-icon="icon:users;ratio:3.5"></span></div>
                                <span>About Dingtoi</span>
                            </div>
                        </a>
                    </div>
                    <div className="uk-card">
                        <a className="uk-link-reset" onClick={() => this.props.history.push('/how-it-work')}>
                            <div className="uk-quick-access-item uk-text-muted uk-text-small">
                                <div><span uk-icon="icon:info;ratio:3.5"></span></div>
                                <span>How it work</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}
const mapDispatchToProps = {
    addCart
}
const ConnectQuickAccess = connect(mapStateToProps, mapDispatchToProps)(withRouter(withStorage(QuickAccess)));

export default ConnectQuickAccess;