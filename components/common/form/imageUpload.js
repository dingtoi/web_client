import React from "react";
import {Alert, Cover, Spinner, Radio} from "../index";
import {SERVER_IMG, SERVER_DOMAIN} from "../../../config";

export class ImageUpload extends React.Component{
    constructor(props){
        super(props);
        this.dropzoneRef = null;
        this.dropzone = null;
        this.files = [];
        this.handlerChangeFile = this.handlerChangeFile.bind(this);
        this.handlerDeleteFile = this.handlerDeleteFile.bind(this);
        this.imagesPreview = this.imagesPreview.bind(this);
        this.state = {
            list: [],
            error: false,
            errorMessage: '',
            isLoading: false
        }
        this.prevId = null;
    }
    componentWillUnmount(){
        this.dropzoneRef = null;
        this.dropzone = null;
        this.files = null;
        this.prevId = null;
    }
    componentDidMount(){
        this.handlerChangeFile();
    }
    beforeRefresh(){
        this.setState({isLoading: true});
    }
    afterRefresh(){
        this.setState({isLoading: false});
    }
    refreshList(result){
        this.setState({list: result}, () => {
            this.afterRefresh();
        });
    }
    handlerChangeFile(){
        const self = this;
        $(this.dropzoneRef).on('change', function(){
            self.imagesPreview(this);
        });
    }
    handlerDeleteFile(item, key){
        this.props.onDelete(item);
    }
    imagesPreview(input){
        if(input.files){
            const self = this;
            const filesAmount = input.files.length;
            for(let i = 0; i < filesAmount; i++){
                let reader = new FileReader();
                reader.onload = function(event){
                    self.props.onChange(input.files);
                    self.dropzoneRef.value = '';
                    if(!/safari/i.test(navigator.userAgent)){
                        self.dropzoneRef.type = ''
                        self.dropzoneRef.type = 'file'
                    }
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }
    render(){
        const imageName = this.props.imageName ? this.props.imageName : 'image';
        const domainName = this.props.domainName ? this.props.domainName : SERVER_DOMAIN;
        return (
            <div>
                {
                    this.state.error
                    ? 
                    <div className="uk-margin-bottom">
                        <Alert message={this.state.errorMessage}
                            onClose={() => this.setState({error: false, errorMessage: ''})}/>
                    </div>
                    : null
                }
                <div>
                    <div className="uk-inline uk-flex uk-flex-wrap uk-flex-middle">
                        {this.state.isLoading && <Cover/>}
                        {this.state.isLoading && <Spinner/>}
                        <div className="uk-position-relative uk-margin-bottom uk-margin-right">
                            <a className="uk-icon-link" uk-icon="icon: plus-circle; ratio: 6"></a>
                            <div className="js-upload uk-position-absolute uk-position-z-index
                                uk-position-top-left uk-box" uk-form-custom="true">
                                <input type="file" ref={instance => this.dropzoneRef = instance}
                                    accept="image/*" style={{width: '120px', height: '120px'}}/>
                            </div>
                        </div>
                        {
                            this.state.list.map((item, key) => {
                                if(item.checked) this.prevId = item.id;
                                return (
                                    <div key={key} className="uk-box uk-inline uk-cart uk-margin-bottom uk-margin-small-right">
                                        {!this.state.isLoading && !item.checked && item[imageName] && <a className="uk-position-top-right uk-badge uk-border-circle"
                                            style={{witdh: '25px', height: '25px'}}
                                            onClick={() => this.handlerDeleteFile(item, key)}>
                                            <span uk-icon="icon: close; ratio: 0.8"/>
                                        </a>}
                                        {
                                            item[imageName]
                                            &&
                                            <div>
                                                <div className="uk-flex uk-flex-column uk-flex-middle">
                                                    <div className="uk-margin">
                                                        <img className="uk-responsive-height" src={domainName+"/"+this.props.folder+'/'+item[imageName]} style={{height: '120px'}} uk-img="true"/>
                                                    </div>
                                                </div>
                                                <div>
                                                    <Radio name="checked" checked={item.checked}
                                                        onChange={event => this.props.onSelectChecked(this.prevId, event.target.value)}
                                                        value={item.id} fontSize={30}/>
                                                </div>
                                            </div>
                                        }
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        )
    }
}