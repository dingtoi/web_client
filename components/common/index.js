import React from "react";

export class Cover extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div className="uk-overlay-default uk-position-cover uk-zindex-nearhighest"/>
        )
    }
}

export class Spinner extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        const ratio = this.props.ratio ? this.props.ratio : 2.5;
        return (
            <div className="uk-position-center uk-zindex-nearhighest">
                <div uk-spinner={"ratio: "+ratio}/>
            </div>
        )
    }
}
export class Spinner2 extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div className="uk-position-center uk-zindex-nearhighest uk-spinner-landingpage">
                <div className="object" id="object_four"></div>
                <div className="object" id="object_three"></div>
                <div className="object" id="object_two"></div>
                <div className="object" id="object_one"></div>
            </div>
        )
    }
}
export class Button extends React.Component{
    constructor(props){
        super(props);
        this.handlerClass = this.handlerClass.bind(this);
        this.state = {
            disabled: false
        }
    }
    handlerClass(){
        let {className, color} = this.props;
        let classList = 'uk-button';
        is.existy(className) ? classList += ' '+className: classList;
        is.existy(color) ? classList += ' uk-button-'+color: classList;
        return classList;
    }
    toggleDisabled(disabled){
        this.setState({disabled: !disabled});
    }
    render(){
        let type = is.existy(this.props.type) ? this.props.type : 'button';
        return (
            <button className={this.handlerClass()} type={type}
                disabled={this.state.disabled}
                onClick={this.props.onClick}>
                {this.props.children}
            </button>
        )
    }
}

export class Alert extends React.Component{
    constructor(props){
        super(props);
        this.instanceRef = null;
        this.timeout = null;
    }
    componentDidMount(){
        UIkit.alert(this.instanceRef);
        this.timeout = setTimeout(() => {
            UIkit.alert(this.instanceRef).close();
        }, 3000);
        $(this.instanceRef).on('beforehide', () => {
            if(this.timeout)
                clearTimeout(this.timeout);
            if(is.not.undefined(this.props.onBeforeHide))
                this.props.onBeforeHide();
        });
    }
    componentWillUnmount(){
        this.instanceRef = null;
        $(this.instanceRef).off('beforehide');
        clearTimeout(this.timeout);
    }
    render(){
        return (
            <div className="uk-background-danger uk-light uk-width-1-1 uk-margin-remove uk-flex uk-flex-between uk-padding-remove-right" 
            ref={instance => this.instanceRef = instance}>
                <div>{this.props.message}</div>
                <a className="uk-margin-xsmall-right" uk-icon="icon: close"
                    onClick={() => UIkit.alert(this.instanceRef).close()}></a>
            </div>
        )
    }
}

export {InputText, InputNumber, InputPassword, InputRange, Select2, Radio, Checkbox} from "./form/input";
export {ImageUpload} from "./form/imageUpload";
export {Table} from "./list/table";
export {Modal} from "./modal";
export {ModalDefault} from "./modal/default";