import React from "react";

export class Modal extends React.Component{
    constructor(props){
        super(props);
    }
    componentDidMount(){
        this.modal = UIkit.modal(this.modalRef);
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.isShow){
            UIkit.modal(this.modalRef).show();
            $(this.modalRef).on('hidden', () => {
                this.props.onHidden();
            });
        }else
            UIkit.modal(this.modalRef).hide();
    }
    componentWillUnmount(){
        this.modal.$destroy(true);
    }
    render(){
        return (
            <div className="uk-flex-top" ref={instance => this.modalRef = instance}>
                <div className="uk-modal-dialog uk-margin-auto-vertical">
                    <button className="uk-modal-close-default" type="button" uk-close="true"></button>
                    {
                        this.props.isShow
                        ?
                        React.cloneElement(this.props.children, { ...this.props, modal: this.modalRef })
                        : null
                    }
                </div>
            </div>
        )
    }
}