import React from "react";

class NotFound extends React.Component{
    render(){
        return (
            <div>
                <div className="uk-container uk-flex uk-flex-center uk-flex-middle uk-flex-column" uk-height-viewport="true">
                    <h2 className="uk-text-muted uk-text-largest uk-margin-remove">4<span className="uk-text-emphasis">0</span>4</h2>
                    <h3 className="uk-text-muted uk-margin-remove-top uk-margin-bottom">Sorry! The page you were looking for could not be found</h3>
                    <h4 className="uk-text-muted uk-margin-remove-top uk-margin-bottom">Please try one of the following pages: </h4>
                    <div>
                        <a className="uk-button uk-button-default uk-text-bold uk-text-emphasis uk-border-quick-comapre">Go to home</a>
                    </div>
                    
                </div>
            </div>
        )
    }
}

export default NotFound; 