import React from "react";
import {Cover, Spinner} from "../index";

export class Table extends React.Component{
    constructor(props){
        super(props);
        this.instanceRef = null;
        this.table = null;
        this.state = {
            list: []
        }
        this.autoReload = this.autoReload.bind(this);
    }
    componentDidMount(){
        this.table = $(this.instanceRef).DataTable();
    }
    componentWillReceiveProps(nextProps){
        this.setState({list: nextProps.list}, () => {
            this.autoReload(this.state.list);
        });
    }
    componentWillUnmount(){
        this.instanceRef = null;
        this.table.destroy();
        this.table = null;
    }
    autoReload(list){
        this.table.clear().draw();
        for(let i = 0; i < list.length; i++){
            let json = [];
            for(let j = 0; j < this.props.header.length; j++){
                if(is.existy(this.props.header[j].action))
                    json.push(
                        '<div>'+
                            '<a className="uk-margin-xsmall-right" uk-icon="icon: pencil"></a>'+
                            '<a uk-icon="icon: trash"></a>'+
                        '</div>'
                    );
                else
                    json.push(list[i][this.props.header[j].field]);
            }
            this.table.row.add(json).draw();
        }
    }
    render(){
        return (
            <div className="uk-inline uk-width-1-1">

                <table ref={(instance) => this.instanceRef = instance} className="uk-table uk-table-hover uk-table-striped uk-width-1-1">
                    <thead>
                        <tr>
                            {
                                this.props.header.map((item, key) => {
                                    return (
                                        <th key={key}>{item.title}</th>
                                    )
                                })
                            }
                        </tr>
                    </thead>
                        <tbody>
                        </tbody>
                </table>
            </div>
        )
    }
}