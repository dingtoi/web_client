import React from "react";

export class GridInner extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div className="uk-container uk-container-small uk-margin-large-top uk-margin-large-bottom">
                {this.props.children}
            </div>
        )
    }
}

export class GridHorizontal extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div className="uk-flex uk-flex uk-flex-center uk-flex-middle uk-height-viewport uk-height-viewport">
                <div className="uk-card uk-card-default uk-card-body uk-width-xxlarge">
                    <div className="uk-grid-divider uk-child-width-expand@s" uk-grid="true">
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}

export class GridCenter extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div className="uk-flex uk-flex uk-flex-center uk-flex-middle uk-height-viewport" uk-height-viewport="true">
                {this.props.children}
            </div>
        )
    }
}

export class Card extends React.Component{
    constructor(props){
        super(props);
        this.handlerClass = this.handlerClass.bind(this);
    }
    handlerClass(){
        let {className, color} = this.props;
        let classList = 'uk-card uk-card-default uk-card-body';
        is.existy(className) ? classList += ' '+className: classList;
        return classList;
    }
    render(){
        return (
            <div className={this.handlerClass()}>
                {this.props.children}
            </div>
        )
    }
}