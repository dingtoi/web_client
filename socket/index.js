var express = require('express');
var app = express();
var cors = require('cors');

app.use(cors());
var server = app.listen(8091);
var io = require('socket.io').listen(server);

io.set('transports', ['websocket']);

io.on('connection', function(socket){
    console.log(`Socket ${socket.id} connected.`);
    socket.on('disconnect', function(){
        console.log(`Socket ${socket.id} disconnected.`);
    });
    socket.on('proposal', function(email){
        io.emit('proposal_success', email);
    });
});