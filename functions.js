import { Observable } from 'rxjs';

const functions = {
    getListBeforePreview: (list) => {
        let arr = [];
        let arrPrev = [];
        let total = 0;
        for(let i = 0; i < list.length; i++){
            let item = list[i];
            //has proposal
            if(is.not.null(item.proposal_id)){
                if(Number(item.cart_type) === 1){ // sale
                    if(item.proposal_status !== 'accepted')
                        item.real_price = item.sale_price;
                    else
                        item.real_price = item.sale_proposal_price;
                    total += Number(item.real_price);
                    arr.push(item);
                }else{
                    if(item.proposal_status !== 'accepted'){
                        item.prevent = true;
                        item.error = item.device_name + ' not have proposal accepted';
                        arrPrev.push(item);
                    }else{
                        item.real_price = item.exchange_proposal_price;
                        total += Number(item.real_price);
                        arr.push(item);
                    }
                }
            }else{ //no proposal
                if(Number(item.cart_type) === 1){ // sale
                    item.real_price = item.sale_price;
                    total += Number(item.real_price);
                    arr.push(item);
                }else{
                    item.error = item.device_name + ' have no proposal';
                    item.prevent = true;
                    arrPrev.push(item);
                }
            }
        }
        return {list: arr, listPrev: arrPrev, originalList: list, total: total};
    },
    socketSendProposal: (socket, received_email) => {
        if(received_email){
            socket.emit('proposal', received_email);
        }
    },
    socketReceivedProposal: (socket, component) => {
        return new Observable((observer) => {
            socket.on('proposal_success', (email) => {
                const auth = localStorage.getItem('email') ? localStorage.getItem('email'): '';
                if(email === auth){
                    observer.next(observer);
                }
            });
        });
    }
}

export default functions;