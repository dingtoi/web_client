import axios from "axios";
import { SERVER_API, MICRO_DEVICE,MICRO_DOMAIN} from "../config";

export default {
    getListMyDevice:({email, offset, limit})=>{
        return axios.post(MICRO_DOMAIN+MICRO_DEVICE + '/api/device/list',{email, offset, limit});
    },
    searchWishList: ({email,id}) => {
        return axios.post(MICRO_DOMAIN+MICRO_DEVICE + '/api/device/searchWishList', {email,id});
    },
    addSearchDevice: ({category_id, brand_id, model_id, capacity_id, color_id, ram_id, email}) => {
        return axios.post(MICRO_DOMAIN+MICRO_DEVICE + '/api/device/search/add', {category_id, brand_id, model_id, capacity_id, color_id, ram_id, email});
    },
    deleteSearchItem: ({id}) => {
        return axios.delete(MICRO_DOMAIN+MICRO_DEVICE + '/api/device/search/delete',  { data: { id: id } });
    },
    getSearchList: ({email}) => {
        return axios.post(MICRO_DOMAIN+MICRO_DEVICE + '/api/device/list/search', {email: email});
    },
    checkExitsSearch: ({model_id,  capacity_id, color_id, ram_id, email}) => {
        return axios.post(MICRO_DOMAIN+MICRO_DEVICE + '/api/device/search/exists', {model_id, capacity_id, color_id, ram_id, email});
    },
    getSearchByName:({device_name, email}) => {
        return axios.post(MICRO_DOMAIN+MICRO_DEVICE + '/api/device/searchByDeviceName',{device_name, email});
    },
    getListImage:({device_id}) => {
        return axios.post(MICRO_DOMAIN+MICRO_DEVICE + '/api/device/list/image',{device_id});
    },
    getDeviceWithType: ({email, type, offset, limit}) => {
        return axios.post(MICRO_DOMAIN+MICRO_DEVICE + '/api/device/list/type', {email, type, offset, limit});
    },
    getDeviceWithTypeMobile: ({email, type}) => {
        return axios.post(MICRO_DOMAIN+MICRO_DEVICE + '/api/deviceMobile/list/type', {email: email, type: type});
    },
    getDeviceFeatured: ({email}) => {
        return axios.post(MICRO_DOMAIN+MICRO_DEVICE + '/api/device/list/featured', {email: email});
    },
    getDeviceDetail: ({id, email}) => {
        return axios.get(MICRO_DOMAIN+MICRO_DEVICE + '/api/device/detail/'+id+'/'+email);
    },
    getDeviceRelated: ({id, email}) => {
        return axios.get(MICRO_DOMAIN+MICRO_DEVICE + '/api/device/related/'+id+'/'+email);
    },
    getDeviceWishlist: ({email}) => {
        return axios.post(MICRO_DOMAIN+MICRO_DEVICE + '/api/device/list/wishlist', {email: email});
    },
    addWishlist: ({ email, device_id }) => {
        return axios.post(MICRO_DOMAIN+MICRO_DEVICE + '/api/user/wishlist', { email: email, device_id: device_id });
    },
    removeWishlist: ({ id }) => {
        return axios.delete(MICRO_DOMAIN+MICRO_DEVICE + '/api/user/wishlist', { data: { id: id } });
    },
    getDeviceListByUser: ({email}) => {
        return axios.post(MICRO_DOMAIN+MICRO_DEVICE + '/api/device/list', {email: email});
    },
    getBrandList: () => {
        return axios.get(MICRO_DOMAIN+MICRO_DEVICE + '/api/brand/list');
    },
    getCategoryList: () => {
        return axios.get(MICRO_DOMAIN+MICRO_DEVICE + '/api/category/list');
    },
    getColorList: () => {
        return axios.get(MICRO_DOMAIN+MICRO_DEVICE + '/api/color/list');
    },
    getCapacityList: () => {
        return axios.get(MICRO_DOMAIN+MICRO_DEVICE + '/api/capacity/list');
    },
    getRamList: () => {
        return axios.get(MICRO_DOMAIN+MICRO_DEVICE + '/api/ram/list');
    },
    getDeviceCategory: ({email, filter, offset,limit}) => {
        return axios.post(MICRO_DOMAIN+MICRO_DEVICE + '/api/device/category', {email, filter, offset,limit});
    },
    getDeviceCompare: ({id, email}) => {
        return axios.post(MICRO_DOMAIN+MICRO_DEVICE + '/api/device/compare', {email, id});
    },
    getSlideList: () => {
        return axios.get(MICRO_DOMAIN+MICRO_DEVICE + '/api/slide/list');
    },
   
}