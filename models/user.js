import axios from "axios";
import { SERVER_API, MICRO_AUTH,MICRO_DOMAIN} from "../config";

export default {
    checkPassword: ({ email, password }) => {
        return axios.post(MICRO_DOMAIN+MICRO_AUTH + '/api/user/checkPassword', { email, password });
    },
    changePassword: ({ email, password }) => {
        return axios.post(MICRO_DOMAIN+MICRO_AUTH + '/api/user/changePassword', { email, password });
    },
    checkExists: ({ email }) => {
        return axios.post(MICRO_DOMAIN+MICRO_AUTH + '/api/user/exists', { email });
    },
    registerUser: ({ email, password }) => {
        return axios.post(MICRO_DOMAIN+MICRO_AUTH + '/api/user/register', { email, password });
    },
    editUser: ({ email, billing_name, city, country, address1, address2, postal_code }) => {
        return axios.post(MICRO_DOMAIN+MICRO_AUTH + '/api/user/edit', { email, billing_name, city, country, address1, address2, postal_code });
    },
    getUserDetail: ({ email }) => {
        return axios.post(MICRO_DOMAIN+MICRO_AUTH + '/api/user/detail', { email: email });
    },
    loginAnonymous: ({ ip }) => {
        return axios.post(MICRO_DOMAIN+MICRO_AUTH + '/api/user/login/anonymous', { ip });
    },
    syncCart: ({ prev_email, email }) => {
        return axios.post(MICRO_DOMAIN+MICRO_AUTH + '/api/user/cart/sync', { prev_email, email });
    },
    userAvatar: ({ email }) => {
        return axios.post(MICRO_DOMAIN+MICRO_AUTH + '/api/user/avatar', { email });
    },
    deleteUserAvatar: ({ email }) => {
        return axios.delete(MICRO_DOMAIN+MICRO_AUTH + '/api/user/delete/image', { data: {email} });
    },
}