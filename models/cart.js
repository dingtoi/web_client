import axios from "axios";
import { SERVER_API, MICRO_CART,MICRO_DOMAIN} from "../config";

export default {
    postItemToCart: ({ email, device_id, available_id, type }) => {
        return axios.post(MICRO_DOMAIN+MICRO_CART + '/api/user/cart', { email: email, device_id: device_id, available_id: available_id, type });
    },
    getListCartByUser: ({ email }) => {
        return axios.post(MICRO_DOMAIN+MICRO_CART + '/api/user/cart/email', { email: email });
    },
    deleteItemCart: ({ id }) => {
        return axios.delete(MICRO_DOMAIN+MICRO_CART + '/api/user/cart', { data: { id: id } });
    },
    getItemCart: ({ id }) => {
        return axios.get(MICRO_DOMAIN+MICRO_CART + '/api/user/cart/' + id);
    },
    addProposal: ({ email, cart_id, type, sale_proposal_price, exchange_proposal_price, exchange_proposal_device }) => {
        return axios.post(MICRO_DOMAIN+MICRO_CART + '/api/proposal/add', { email, cart_id, type, sale_proposal_price, exchange_proposal_device, exchange_proposal_price, status });
    },
    cancelProposal: ({ id }) => {
        return axios.delete(MICRO_DOMAIN+MICRO_CART + '/api/proposal/cancel', { data: { id: id } });
    },
    editProposal: ({ email, cart_id, type, sale_proposal_price, exchange_proposal_price, exchange_proposal_device, id, status = "updated" }) => {
        return axios.post(MICRO_DOMAIN+MICRO_CART + '/api/proposal/edit', { id, email, cart_id: cart_id, type, sale_proposal_price, exchange_proposal_device, exchange_proposal_price, status });
    },
    getListReceivedProposal: ({ email }) => {
        return axios.post(MICRO_DOMAIN+MICRO_CART + '/api/proposal/received/list', { email: email });
    },
    getDetailReceivedProposal: ({ email, id }) => {
        return axios.get(MICRO_DOMAIN+MICRO_CART + '/api/proposal/received/detail/' + id + '/' + email);
    },
    recommendDevice:({id, recommend_device})=>{
        return axios.post(MICRO_DOMAIN+MICRO_CART + '/api/proposal/recommend/add',{id, recommend_device});
    },
    recommendDeviceEmpty:({id, recommend_device})=>{
        return axios.post(MICRO_DOMAIN+MICRO_CART + '/api/proposal/recommend/delete',{id, recommend_device});
    },
}