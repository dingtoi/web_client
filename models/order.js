import axios from "axios";
import {MICRO_ORDER,MICRO_DOMAIN} from "../config";

export default {
    getOrderList: ({ email }) => {
        return axios.post(MICRO_DOMAIN+MICRO_ORDER + '/api/user/order/list', { email });
    },
    addOrder: ({ email, list, total }) => {
        return axios.post(MICRO_DOMAIN+MICRO_ORDER + '/api/user/order', { email, list, total });
    },
    getTransactionList: ({ email }) => {
        return axios.post(MICRO_DOMAIN+MICRO_ORDER + '/api/user/transaction/list', { email });
    },
}