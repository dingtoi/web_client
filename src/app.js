import "babel-polyfill";

import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import axios from "axios";

import io from 'socket.io-client';

import createHistory from "history/createBrowserHistory";
import { Provider } from "react-redux";
import configureStore from "./configureStore";

import Home from "./containers/home";
import Login from "./containers/login";
import Register from "./containers/register";
import Cart from "./containers/cart";
import Account from "./containers/account";
import AccountTransaction from "./containers/account/transaction";
import AccountDevice from "./containers/account/device";
import AccountWishlist from "./containers/account/wishlist";
import AccountReceivedProposal from "./containers/account/receivedProposal";
import AccountReceivedProposalSale from "./containers/account/receivedProposalSale";
import AccountReceivedProposalExchange from "./containers/account/receivedProposalExchange";
import AccountDeviceAdd from "./containers/account/deviceAdd";
import AccountDeviceEdit from "./containers/account/deviceEdit";
import AccountDeviceImages from "./containers/account/deviceImages";
import AvailableAdd from "./containers/account/availableAdd";
import AvailableEdit from "./containers/account/availableEdit";
import AccountOrder from "./containers/account/order";
import ChangePassword from "./containers/account/changePassword";
import AccountInfo from "./containers/account/accountInfo";
import AccountAvatar from "./containers/account/accoutAvatar";

import Investors from "./containers/investors";
import Contact from "./containers/contact";
import jobOpportunities from "./containers/job";
import Newsroom from "./containers/newsroom";
import Leadership from "./containers/leadership";
import Support from "./containers/support";

import Privacy from "./containers/link/privacy";
import Term from "./containers/link/term";
import SaleRetund from "./containers/link/saleRetund";
import Legal from "./containers/link/legal";
import SiteMap from "./containers/link/siteMap";

import DeviceDetail from "./containers/device";
import Category from "./containers/category";
import Preview from "./containers/preview";
import Shipping from "./containers/shipping";
import Payment from "./containers/payment";

import Search from "./containers/search";
import Menu from "../components/desktop/menu";
import Footer from "../components/desktop/footer";

import {SERVER_SOCKET} from "../config";
import Functions from "../functions";

const initialState = {};
const history = createHistory();
const store = configureStore(initialState, history);

import withStorage from "../hoc/storage";
import ScrollToTop from "../hoc/scrollToTop";

class App extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isMenu: true
        }
        this.observer = null;
    }
    componentWillMount(){
        accounting.settings = {
            currency: {
                symbol : "$",   // default currency symbol is '$'
                format: "%s%v", // controls output: %s = symbol, %v = value/number (can be object: see below)
                decimal : ".",  // decimal point separator
                thousand: ",",  // thousands separator
                precision : 0   // decimal places
            },
            number: {
                precision : 0,  // default precision on numbers is 0
                thousand: ",",
                decimal : "."
            }
        }
        axios.interceptors.response.use(function (response) {
            const data = response.data;
            if(is.undefined(data)){
                UIkit.notification({message: 'Server are on construction !', status: 'danger', pos: 'top-left'});
                return Promise.reject({status: 500});
            }
            else if(data.status !== 200)
                return Promise.reject(data);
            else
                return data.data;
        }, function(error) {
            UIkit.notification({message: 'Server are on construction !', status: 'danger', pos: 'top-left'});
            return Promise.reject({status: 500});
        });

        const connectionOptions = {
            'force new connection': true,
            'reconnectionAttemps': 'Infinity',
            transports: ['websocket']
        }

        this.socket = io.connect(SERVER_SOCKET, connectionOptions);
        Functions.socketReceivedProposal(this.socket)
        .subscribe((observer) => {
            this.observer = observer;
            UIkit.notification({message: 'Your Proposal List Has Changed !', status: 'primary', pos: 'top-left'});
        })
    }
    componentWillUnmount(){
        if(this.observer)
            this.observer.complete();
    }
    setMenu(){
        this.setState({isMenu: false}, () => {
            this.setState({isMenu: true});
        });
    }
    render(){
        return (
            <Provider store={store}>
                <Router>
                    <ScrollToTop>
                        <div>
                            <div className="uk-height-viewport" uk-height-viewport="expand: true">
                                {this.state.isMenu && <Menu app={this}/>}
                                <Route exact path="/" component={Home}/>
                                <Route exact path="/login" render={
                                    (props) => (
                                        <Login {...props} app={this}/>
                                    )
                                }/>
                                <Route exact path="/register" component={Register}/>
                                <Route exact path="/bag" render={
                                    (props) => (
                                        <Cart {...props} socket={this.socket}/>
                                    )
                                }/>
                                <Route exact path="/preview" component={withStorage(Preview)}/>
                                <Route exact path="/shipping" component={Shipping}/>
                                <Route exact path="/payment" render={
                                    (props) => (
                                        <Payment {...props} app={this}/>
                                    )
                                }/>
                                <Route path="/account"
                                    render={
                                        props => (
                                            <Account {...props} socket={this.socket} routes={
                                                [
                                                    {path: '/account/device', component: AccountDevice},
                                                    {path: '/account/device/add', component: AccountDeviceAdd},
                                                    {path: '/account/device/edit/:id', component: AccountDeviceEdit},
                                                    {path: '/account/device/images/:id', component: AccountDeviceImages},
                                                    {path: '/account/device/:id/available/add', component: AvailableAdd},
                                                    {path: '/account/device/:id/available/edit/:available_id', component: AvailableEdit},
                                                    {path: '/account/wishlist', component: AccountWishlist},
                                                    {path: '/account/received_proposal', component: AccountReceivedProposal},
                                                    {path: '/account/received_proposal/sale/:id', component: AccountReceivedProposalSale},
                                                    {path: '/account/received_proposal/exchange/:id', component: AccountReceivedProposalExchange},
                                                    {path: '/account/order', component: AccountOrder},
                                                    {path: '/account/transactions', component: AccountTransaction},
                                                    {path: '/account/change_password', component: ChangePassword},
                                                    {path: '/account/information', component: AccountInfo},
                                                    {path: '/account/avatar', component: AccountAvatar}
                                                ]
                                            }/>
                                        )
                                }/>
                                <Route exact path="/device/:id" render={
                                    (props) => (
                                        <DeviceDetail key={props.match.params.id} {...props}/>
                                    )
                                }/>
                                <Route exact path="/category" render={
                                    (props) => (
                                        <Category key={new Date().getUTCMilliseconds()} {...props}/>
                                    )
                                }/>
                                <Route exact path="/investors" component={Investors}/>
                                <Route exact path="/contact" component={Contact}/>
                                <Route exact path="/job-opportunities" component={jobOpportunities}/>
                                <Route exact path="/newsroom" component={Newsroom}/>
                                <Route exact path="/leadership" component={Leadership}/>
                                <Route exact path="/support" component={Support}/>
                                <Route exact path="/privacy" component={Privacy}/>
                                <Route exact path="/term-of-use" component={Term}/>
                                <Route exact path="/sale-and-retunds" component={SaleRetund}/>
                                <Route exact path="/legal" component={Legal}/>
                                <Route exact path="/sitemap" component={SiteMap}/>
                                <Route exact path="/search/:search" render={
                                    (props) => (
                                        <Search key={props.match.params.search} {...props}/>
                                    )
                                }/>

                            </div>
                            <div>
                                {this.state.isMenu && <Footer/>}
                            </div>
                        </div>
                    </ScrollToTop>
                </Router>
            </Provider>
        )
    }
}

export default App;