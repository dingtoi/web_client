import {ADD_CART} from './constants';

const initialState = {
    count: 0
};

function homeReducer(state = initialState, action){
    switch(action.type){
        case ADD_CART:
            return Object.assign({}, state, {
                count: state.count+1
            });
        default:
            return state;
    }
}

export default homeReducer;