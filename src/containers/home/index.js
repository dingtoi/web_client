import React from "react";

import ListType from "../../../components/desktop/listType";
import Newsletter from "../../../components/desktop/newsletter";
import Ad from "../../../components/desktop/ad";
import FeatureDevices from "../../../components/desktop/featureDevices";
import {withRouter} from "react-router-dom";
import {SERVER_IMG,MICRO_DOMAIN,MICRO_DEVICE} from "../../../config";
import DeviceModel from "../../../models/device";

class Home extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            listSlide: []
        }
    }
    componentDidMount(){
        DeviceModel.getSlideList()
        .then(result =>{
            this.setState({listSlide: result});
        })
    }
    render(){
        return (
            <div>
                <div className="uk-position-relative uk-visible-toggle uk-light" uk-slideshow="animation: scale; autoplay: true; ratio: 7:3">
				    <ul className="uk-slideshow-items" id="dingtoi_slides">
                    {
                        this.state.listSlide.map((item, key)=>{
                            return (
                                <li key={key}>
                                    <div item={item} className="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-center-left">
                                        <img src={MICRO_DOMAIN+MICRO_DEVICE+"/slides/"+item.image} alt="" uk-cover="true"/>
                                    </div>
                                </li>
                            )
                        })
                    }
				    </ul>
				</div>
                <div className="uk-container uk-margin-large-top uk-margin-large-bottom">
                    <div className="uk-grid uk-grid-small" uk-grid="true">
                        <div className="uk-width-1-3@l uk-cursor"
                            onClick={() => this.props.history.push('/category?type=1')}>
                            <div className="uk-animation-toggle">
                                <div className="uk-background-top-right uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-middle uk-flex-center
                                    uk-animation-fade dingtoi_banners"
                                    style={{backgroundImage: `url(`+SERVER_IMG+`purpose-1.jpg)`}}>
                                    <p className="uk-h3 uk-text-white uk-text-uppercase">Sale</p>
                                </div>
                            </div>
                        </div>
                        <div className="uk-width-1-3@l uk-cursor"
                            onClick={() => this.props.history.push('/category?type=1')}>
                            <div className="uk-animation-toggle">
                                <div className="uk-background-top-right uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-middle uk-flex-center
                                    uk-animation-fade dingtoi_banners"
                                    style={{backgroundImage: `url(`+SERVER_IMG+`purpose-2.jpg)`}}>
                                    <p className="uk-h3 uk-text-white uk-text-uppercase">Buy</p>
                                </div>
                            </div>
                        </div>
                        <div className="uk-width-1-3@l uk-cursor"
                            onClick={() => this.props.history.push('/category?type=2')}>
                            <div className="uk-animation-toggle">
                                <div className="uk-background-top-right uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-middle uk-flex-center
                                    uk-animation-fade dingtoi_banners"
                                    style={{backgroundImage: `url(`+SERVER_IMG+`purpose-3.png)`}}>
                                    <p className="uk-h3 uk-text-white uk-text-uppercase">Exchange</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="uk-container uk-margin-large">
                    <FeatureDevices/>
                </div>
                <div className="uk-container uk-margin-large">
                    <Ad/>
                </div>
                <div className="uk-container uk-margin-large">
                    <ListType/>
                </div>
                <div>
                    <Newsletter/>
                </div>
            </div>
            
        )
    }
}

export default withRouter(Home);