import React from 'react';
import queryString from "query-string";

import CategoryMenu from "./menu";
import withCategory from "../../../hoc/category";

import DeviceModel from "../../../models/device";
import {Cover, Spinner} from "../../../components/common";
import Item from '../../../components/desktop/item';

import withStorage from "../../../hoc/storage";
import {connect} from "react-redux";

import ReactPaginate from 'react-paginate';

class Category extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            list: [],
            isLoading: false,
            categoryModel: {},
            offset: 0,
            total: 0
        }
        this.pageDisplay = 4;
    }
    handlePageClick = data => {
        let selected = data.selected;
        let offset = Math.ceil(selected * this.pageDisplay);
    
        this.setState({ offset: offset }, () => {
            this.refreshList();
        });
    };
    componentWillReceiveProps(nextProps){
        if(nextProps.count){
            this.refreshList();
        }
    }
    componentWillMount(){
        this.filter = this.props.filter;
        this.queryString = queryString.parse(this.props.location.search);
    }
    refreshList(){
        this.setState({isLoading: true});
        DeviceModel.getDeviceCategory({email: this.props.auth, filter: this.filter, offset:this.state.offset, limit:this.pageDisplay})
        .then((result) => {
            this.setState({list: result.list, isLoading: false, total: Math.ceil(result.total / this.pageDisplay)});
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    render(){
        return (
            <div>
                <CategoryMenu {...this.props} queryString={this.queryString} categoryModel={this.state.categoryModel} onTriggerList={(filter) => {
                    this.filter = filter;
                    this.refreshList();
                }} ref={instance => this.menuRef = instance}>
                    <div className="uk-inline uk-width-1-1">
                        {this.state.isLoading && <Cover/>}
                        {this.state.isLoading && <Spinner/>}
                        {
                            this.state.list.length > 0
                            ?
                            <div className="uk-grid" uk-grid="true">
                                {
                                    this.state.list.map((item, key) => {
                                        return (
                                            <div className="uk-width-1-4@l uk-width-1-3@m uk-width-1-2@s" key={key}>
                                                <Item item={item}
                                                    onAddCart={() => this.refreshList()}/>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                            :
                            <div className="uk-placeholder uk-text-center">There is no items.</div>
                        }
                    </div>
                    {
                        this.state.list.length > 0
                        ?
                        <div >
                            <ReactPaginate
                            previousLabel={'Previous'}
                            nextLabel={'Next'}
                            breakLabel={'...'}
                            breakClassName={'break-me'}
                            pageCount={this.state.total}
                            onPageChange={this.handlePageClick}
                            containerClassName={'pagination uk-flex uk-flex-center uk-padding-remove'}
                            subContainerClassName={'pages pagination'}
                            activeClassName={'active'}
                            />
                        </div>
                        :
                        null
                    }
                   
                </CategoryMenu>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const ConnectCategory = connect(mapStateToProps, null)(withStorage(withCategory(Category)));

export default ConnectCategory;