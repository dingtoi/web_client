import React from 'react';
import Item from "../../../components/desktop/item";
import DeviceModel from "../../../models/device";
import withStorage from "../../../hoc/storage";
import {Cover, Spinner} from "../../../components/common";
import {connect} from "react-redux";

class Search extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading :false,
            list: []
        }
        this.search = '';
    }
    componentWillMount(){
        this.search = this.props.match.params.search;
    }
    componentDidMount(){
        this.refreshList();
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.count)
            this.refreshList();
    }
    refreshList(){
        this.setState({isLoading: true});
        DeviceModel.getSearchByName({device_name: this.search, email: this.props.auth})   
        .then((list)=>{
            this.setState({isLoading:false,list:list},()=>{
            });
        })
        .catch(() => {
            this.setState({ isLoading: false });
        })            
    }
    render(){
        return (
            <div>
                <div className="border-breadcrumb uk-padding uk-padding-remove-bottom uk-padding-remove-left uk-padding-remove-right uk-background-default" uk-sticky="top: 150; bottom: #animation">
                    <div className="uk-container uk-flex uk-flex-middle uk-padding-small">
                        <div>
                            <ul className="uk-breadcrumb uk-margin-remove">
                                <li><a onClick={() => this.props.history.push('/')}>Home</a></li>
                                <li className="uk-active"><span>Search</span></li>
                            </ul>
                        </div>
                    </div>
                </div>	
                <div className="uk-container uk-padding-small ">
                    <h3>Search results for "{this.search}"</h3>
                    <div className="uk-inline uk-width-1-1">
                        {this.state.isLoading && <Cover/>}
                        {this.state.isLoading && <Spinner/>}
                        {
                            this.state.list.length > 0
                            ?
                            <div className="uk-grid" uk-grid="true">
                            {
                                this.state.list.map((item,key)=>{
                                    return(
                                        <div className="uk-width-1-4@l uk-width-1-3@m uk-width-1-2@s" key={key}>
                                            <Item item={item} onAddCart={() => this.refreshList()}/>
                                        </div>
                                    )
                                })
                            }
                            </div>
                            :
                            <div className="uk-placeholder uk-text-center">There is no items.</div>
                        }
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const ConnectSearch = connect(mapStateToProps, null)(withStorage(Search));

export default ConnectSearch;