import React from "react";
import {SERVER_IMG} from "../../../config";
import { Form, Field } from 'react-final-form';
import { InputText, InputPassword, Cover, Spinner, Button, Alert } from "../../../components/common";

class Contact extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading:false
        }
        this.handlerSubmit = this.handlerSubmit.bind(this);       
        this.resetForm = null;
    }
    handlerSubmit(values){
        this.setState({isLoading:true});
        if(values){
            UIkit.notification({message: 'Send Successfully !', status: 'primary', pos: 'top-left'});
            this.setState({isLoading:false});
            this.resetForm();
        }
    }
    render(){
        return (
            <div>
                <div className="uk-background-cover uk-height-large uk-panel uk-flex uk-flex-center uk-flex-middle uk-light" style={{backgroundImage:'url('+SERVER_IMG+'banner-contact.jpg)'}}>
                    <p className="uk-h1">Contact</p>
                </div>
                <div className="uk-container uk-padding-large">
                    <h2>Contact to Swap-ez</h2>
                    <div className="uk-child-width-1-1@s uk-child-width-1-1@m uk-child-width-1-2@l" uk-grid="true">
                        <div>
                            <div className="uk-card">
                                <p>
                                Swap-ez is a fast service provider and ensures customer service with the highest quality. 
                                With all the passion for the profession, we believe that we can capture the needs of the market 
                                accurately and solve the most flexible problem. Contact to Swap-ez if you want to know more and more.
                                </p>
                                <h4 className="uk-margin-small-top">Address</h4>
                                <p>248 Avenue, Quebec, Canada</p>
                                <h4 className="uk-margin-small-top">Email</h4>
                                <p>email@swap*ez.com</p>
                                <h4 className="uk-margin-small-top">Phone</h4>
                                <p>(098)-686-666888</p>
                            </div>
                        </div>
                        <div>
                            <div className="uk-card">
                                <h4>Questions for the Investor Relations department can be submitted below.</h4>
                                <Form
                                onSubmit={this.handlerSubmit}
                                validate={values => {
                                        const errors = {};
                                        if(!values.name)
                                            errors.name = "Full Name must be required";
                                        if(!values.email)
                                            errors.email = "Email must be required";
                                        else if(is.not.email(values.email))
                                            errors.email = "Invalid Email";
                                        if(!values.subject)
                                            errors.subject = "Subject must be required";
                                        if(!values.comment)
                                            errors.comment = "Comment must be required";

                                        return errors;
                                    }
                                }
                                render={({submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                                    this.resetForm = form.reset;
                                    return <form onSubmit={handleSubmit}>
                                        <div className="uk-grid-small" uk-grid="true">
                                            {submitting && <Cover/>}
                                            {submitting && <Spinner/>}
                                            <Field name="name">
                                                {({ input, meta }) => (
                                                <div className="uk-width-1-1">
                                                    <div>
                                                        <InputText {...input} type="text" placeholder="Full Name"/>
                                                    </div>
                                                    {
                                                        meta.touched && 
                                                        <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                            {meta.error || meta.submitError}
                                                        </span>
                                                    }
                                                </div>
                                                )}
                                            </Field>
                                            <Field name="email">
                                                {({ input, meta }) => (
                                                <div className="uk-width-1-2@s">
                                                     <InputText {...input} type="text" placeholder="Email"/>
                                                    {
                                                        meta.error && meta.touched && 
                                                        <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                            {meta.error}
                                                        </span>
                                                    }
                                                </div>
                                                )}
                                            </Field>
                                            <Field name="phone">
                                                {({ input, meta }) => (
                                                <div className="uk-width-1-2@s">
                                                     <InputText {...input} type="number" placeholder="Phone"/>
                                                    {
                                                        meta.error && meta.touched && 
                                                        <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                            {meta.error}
                                                        </span>
                                                    }
                                                </div>
                                                )}
                                            </Field>
                                            <Field name="subject">
                                                {({ input, meta }) => (
                                                <div className="uk-width-1-1">
                                                    <div>
                                                        <InputText {...input} type="text" placeholder="Subject"/>
                                                    </div>
                                                    {
                                                        meta.touched && 
                                                        <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                            {meta.error || meta.submitError}
                                                        </span>
                                                    }
                                                </div>
                                                )}
                                            </Field>
                                            <Field name="comment">
                                                {({ input, meta }) => (
                                                <div className="uk-width-1-1">
                                                    <div>
                                                        <textarea {...input} className="uk-input" placeholder="Comment"/>
                                                    </div>
                                                    {
                                                        meta.touched && 
                                                        <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                            {meta.error || meta.submitError}
                                                        </span>
                                                    }
                                                </div>
                                                )}
                                            </Field>
                                            <div className="uk-flex uk-margin-large-top">
                                                <Button className="uk-width-1-1 " type="submit" disabled={submitting || validating} color="primary">
                                                    Send
                                                </Button>
                                            </div>
                                        </div>
                                    </form>
                                }}
                            >
                            </Form>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        )
    }
}

export default Contact;