import React from "react";
import {addCart} from "../../../src/containers/home/actions";
import {connect} from "react-redux";

import { Cover, Spinner, Modal } from "../../../components/common";
import { SERVER_DOMAIN, MICRO_DOMAIN, MICRO_DEVICE } from "../../../config";

import withStorage from "../../../hoc/storage";
import Functions from "../../../functions";

class Preview extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            selectedCart: {},
            list: [],
            total: 0
        }
        this.renderMoney = this.renderMoney.bind(this);
        this.renderExchangeWith = this.renderExchangeWith.bind(this);
        this.listItemInstance = [];
    }
    componentDidMount(){
        if(is.existy(localStorage.getItem('listPreview')) && is.existy(localStorage.getItem('listPreviewTotal'))){
            const list = JSON.parse(localStorage.getItem('listPreview'));
            const total = Number(localStorage.getItem('listPreviewTotal'));
            this.setState({list: list, total: total});
        }else
            this.props.history.push('/bag');
    }
    handlerGoToDevice(l){
        this.props.history.push('/device/'+l.device_id);
    }
    renderMoney(l){
        return <b>{accounting.formatMoney(l.real_price)}</b>
    }
    renderExchangeWith(l){
        if(!l.proposal_id)
            return (
                <div className="uk-text-break">
                    Exchange with <b>{l.exchange_name}</b>
                </div>
            )
        else
            return (
                <div className="uk-text-break">
                    Exchange with <b>{l.exchange_proposal_real_device}</b>
                </div>
            )
    }
    renderCartList(){
        return (
            <div>
                <div className="uk-flex uk-padding-small uk-background-muted uk-text-white uk-text-center uk-visible@m">
                    <div className="uk-width-1-4"></div>
                    <div className="uk-width-1-4">ITEM IN YOUR BAG</div>
                    <div className="uk-width-1-4">TYPE</div>
                    <div className="uk-width-1-4">PRICE</div>
                </div>
                {
                    this.state.list.map((l, key) => {
                        return (
                            <div className={"uk-flex uk-flex-middle uk-padding-small uk-text-center uk-border-propose "+(
                                l.prevent ? 'uk-background-danger uk-light': ''
                            )}
                                key={key}
                            >
                                <div className="uk-width-1-4@m uk-width-1-3@s">
                                    {
                                        l.thumb
                                        ?
                                        <a onClick={() => this.handlerGoToDevice(l)}>
                                            <img className="uk-img uk-cursor" src={MICRO_DOMAIN+MICRO_DEVICE+'/devices/'+l.thumb} width="105px" uk-img="true"/>
                                        </a>
                                        :
                                        <a onClick={() => this.handlerGoToDevice(l)}>
                                            <img src={SERVER_DOMAIN+'no-image.png'} width="105px"/>
                                        </a>
                                    }
                                </div>
                                <div className="uk-width-3-4@m uk-flex uk-flex-between uk-flex uk-flex-row@m uk-flex-column@s uk-flex-middle uk-text-center">
                                    <div className="uk-width-1-4@m">
                                        <div className="uk-text-primary uk-margin-xsmall-bottom uk-cursor"
                                            onClick={() => this.handlerGoToDevice(l)}>{l.device_name}</div>
                                        <div className="uk-margin-xsmall-bottom">Brand: {l.brand_name}</div>
                                        <div className="uk-margin-xsmall-bottom">Color: {l.color_name}</div>
                                        <div className="uk-margin-xsmall-bottom">Capacity: {l.capacity_name}</div>
                                    </div>
            
                                    <div className="uk-width-1-4@m uk-padding-small">
                                        {
                                            Number(l.cart_type) === 1
                                            ? <div>Sale</div>
                                            :
                                            this.renderExchangeWith(l)
                                        }
                                    </div>
                                    <div className="uk-width-1-4@m">
                                        {this.renderMoney(l)}
                                    </div>

                                </div>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
    navigateToPreview(list){
        localStorage.setItem('listPreview', JSON.stringify(list));
        setTimeout(() => {
            this.props.history.push('/preview');
        }, 500);
    }
    render(){
        return (
            <div className="uk-container uk-margin-large">
                <div className="uk-inline uk-width-1-1 uk-margin">
                    {this.state.isLoading && <Cover/>}
                    {this.state.isLoading && <Spinner/>}
                    {
                        this.state.list.length > 0
                        ? this.renderCartList()
                        : <div className="uk-placeholder uk-text-center">There is no item in cart.</div>
                    }
                </div>
                <div className="uk-clearfix">
                    <div className="uk-float-right">
                        <p><b>Total:</b> {accounting.formatMoney(this.state.total)}</p>
                    </div>
                </div>
                <div className="uk-clearfix">
                    <div className="uk-float-right">
                        <a className="uk-button uk-button-primary uk-text-normal"
                            onClick={() => this.props.history.push('/bag')}><i className="fa fa-shopping-cart"></i> Back To Cart</a>
                        {
                            this.state.list.length > 0 && !this.props.anonymous
                            ?
                            <a className="uk-button uk-button-secondary uk-margin-xsmall-left uk-text-normal"
                                onClick={() => this.props.history.push('/shipping')}>
                                <i className="fa fa-check"/> Confirm
                            </a>
                            : null
                        }
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const mapDispatchToProps = {
    addCart
}

const ConnectPreview = connect(mapStateToProps, mapDispatchToProps)(withStorage(Preview));

export default ConnectPreview;