import React from "react";
import {SERVER_IMG} from "../../../config";

class jobOpportunities extends React.Component{
    constructor(props){
        super(props);
    }
   
    render(){
        return (
            <div>
                <div className="uk-background-cover uk-height-large uk-panel uk-flex uk-flex-center uk-flex-middle uk-light" style={{backgroundImage:'url('+SERVER_IMG+'banner-job.jpg)'}}>
                    <p className="uk-h1">Job Opportunities</p>
                </div>
                <div className="uk-container uk-padding-large">
                    <div className="uk-flex-middle uk-flex uk-flex-column uk-text-center">
                        <p className="uk-text-xlarge uk-width-1-2">This is where some of the world’s smartest, most passionate people create the world’s most innovative products and experiences. Join us and you’ll do the best work of your life — and make a difference in other people’s lives.</p>
                        <a className="uk-text-xlarge uk-margin-large-bottom">Learn more about working at Swap-ez</a>
                    </div>
                    <div className="uk-grid-collapse uk-child-width-1-2 uk-margin-large-top" uk-grid="true">
                        <div>
                            <a className="uk-link-reset">
                                <div className="uk-margin-xsmall-right uk-margin-xsmall-bottom uk-inline">
                                    <img data-src={SERVER_IMG+"job-1.jpg"} uk-img="true"/>
                                    <div className="uk-padding uk-position-top-left uk-text-primary">
                                        <div className="uk-text-xlarge">About Swap-ez</div>
                                        <div className="uk-h3 uk-margin-remove">See what driver us</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div>
                        <a className="uk-link-reset">
                                <div className="uk-margin-xsmall-right uk-margin-xsmall-bottom uk-inline">
                                    <img data-src={SERVER_IMG+"job-2.jpg"} uk-img="true"/>
                                    <div className="uk-padding uk-position-top-left uk-text-primary">
                                        <div className="uk-text-xlarge">Team</div>
                                        <div className="uk-h3 uk-margin-remove">Find your calling</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div>
                            <a className="uk-link-reset">
                                <div className="uk-margin-xsmall-right uk-margin-xsmall-bottom uk-inline">
                                    <img data-src={SERVER_IMG+"job-3.jpg"} uk-img="true"/>
                                    <div className="uk-padding uk-position-top-left uk-text-primary">
                                        <div className="uk-text-xlarge">Swap-ez Retail</div>
                                        <div className="uk-h3 uk-margin-remove">Share Swap-ez with your community</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div>
                        <a className="uk-link-reset">
                                <div className="uk-margin-xsmall-right uk-margin-xsmall-bottom uk-inline">
                                    <img data-src={SERVER_IMG+"job-4.jpg"} uk-img="true"/>
                                    <div className="uk-padding uk-position-top-left uk-text-primary">
                                        <div className="uk-text-xlarge">Student</div>
                                        <div className="uk-h3 uk-margin-remove">Get your start here</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div className="uk-padding-large uk-padding-remove-bottom">
                        <div className="uk-flex-middle uk-flex uk-flex-column uk-text-center">
                            <p className="uk-text-xlarge uk-width-1-2 uk-margin-remove"><b>Get discovered.</b> Introduce yourself, and we’ll get in touch if there’s a role that seems like a good match.</p>
                            <a className="uk-text-xlarge">Get started</a>
                        </div>
                        <div className="uk-flex-middle uk-flex uk-flex-column uk-text-center uk-margin-top">
                            <p className="uk-text-xlarge uk-width-1-2 uk-margin-remove"><b>Swap-ez is open.</b> We believe humanity is plural, not singular.
The best way the world works is everybody in. Nobody out.</p>
                            <a className="uk-text-xlarge">Learn more</a>
                        </div>
                    </div>
                </div> 
            </div>
        )
    }
}

export default jobOpportunities;