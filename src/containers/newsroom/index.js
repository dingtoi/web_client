import React from "react";
import {SERVER_IMG} from "../../../config";

class Newsroom extends React.Component{
    constructor(props){
        super(props);
    }
   
    render(){
        return (
            <div>
                <div className="uk-background-cover uk-height-large uk-panel uk-flex uk-flex-center uk-flex-middle uk-light" style={{backgroundImage: 'url('+SERVER_IMG+'banner-newsroom.jpg)'}}>
                    <p className="uk-h1">Newsroom</p>
                </div>
                <div className="uk-container uk-padding-large">
                    <div className="uk-child-width-1-2@m uk-grid" uk-grid="true">
                        <div>
                            <article className="uk-article">
                                <div className="uk-text-center uk-margin-top">
                                    <img data-src={SERVER_IMG+"news-1.jpeg"} uk-img="true"/>
                                    <h2 className="uk-margin-medium-top uk-margin-remove-bottom uk-text-center uk-article-title">
                                        <a className="uk-link-reset">What is Horizon?</a>
                                    </h2>
                                    <p className="uk-margin-top uk-margin-remove-bottom uk-article-meta uk-text-center">
                                        Written by <a>admin</a> 
                                        on November 29, 2018.               
                                        Posted in <a rel="category">News</a>.                
                                        <a>5 Comments</a>                
                                    </p>
                                    <div className="uk-margin-medium-top">
                                        Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.
                                        Quis aute iure reprehenderit...                            
                                    </div>
                                    <p className="uk-text-center uk-margin-medium">
                                        <a className="uk-button uk-button-text">Continue reading</a>
                                    </p>
                                </div>
                            </article>
                        </div>
                        <div>
                            <article className="uk-article">
                                <div className="uk-text-center uk-margin-top">
                                    <img data-src={SERVER_IMG+"news-2.jpeg"} uk-img="true"/>
                                    <h2 className="uk-margin-medium-top uk-margin-remove-bottom uk-text-center uk-article-title">
                                        <a className="uk-link-reset">8 reasons why you need Horizon</a>
                                    </h2>
                                    <p className="uk-margin-top uk-margin-remove-bottom uk-article-meta uk-text-center">
                                        Written by <a>admin</a> 
                                        on November 29, 2018.               
                                        Posted in <a rel="category">News</a>.                
                                        <a>5 Comments</a>                
                                    </p>
                                    <div className="uk-margin-medium-top">
                                        Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.
                                        Quis aute iure reprehenderit...                            
                                    </div>
                                    <p className="uk-text-center uk-margin-medium">
                                        <a className="uk-button uk-button-text">Continue reading</a>
                                    </p>
                                </div>
                            </article>
                        </div>
                        <div>
                            <article className="uk-article">
                                <div className="uk-text-center uk-margin-top">
                                    <img data-src={SERVER_IMG+"news-3.jpeg"} uk-img="true"/>
                                    <h2 className="uk-margin-medium-top uk-margin-remove-bottom uk-text-center uk-article-title">
                                        <a className="uk-link-reset">A new home for Horizon</a>
                                    </h2>
                                    <p className="uk-margin-top uk-margin-remove-bottom uk-article-meta uk-text-center">
                                        Written by <a>admin</a> 
                                        on November 29, 2018.               
                                        Posted in <a rel="category">News</a>.                
                                        <a>5 Comments</a>                
                                    </p>
                                    <div className="uk-margin-medium-top">
                                        Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.
                                        Quis aute iure reprehenderit...                            
                                    </div>
                                    <p className="uk-text-center uk-margin-medium">
                                        <a className="uk-button uk-button-text">Continue reading</a>
                                    </p>
                                </div>
                            </article>
                        </div>
                        <div>
                            <article className="uk-article">
                                <div className="uk-text-center uk-margin-top">
                                    <img data-src={SERVER_IMG+"news-4.jpeg"} uk-img="true"/>
                                    <h2 className="uk-margin-medium-top uk-margin-remove-bottom uk-text-center uk-article-title">
                                        <a className="uk-link-reset">A brief history of the Horizon team</a>
                                    </h2>
                                    <p className="uk-margin-top uk-margin-remove-bottom uk-article-meta uk-text-center">
                                        Written by <a>admin</a> 
                                        on November 29, 2018.               
                                        Posted in <a rel="category">News</a>.                
                                        <a>5 Comments</a>                
                                    </p>
                                    <div className="uk-margin-medium-top">
                                        Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.
                                        Quis aute iure reprehenderit...                            
                                    </div>
                                    <p className="uk-text-center uk-margin-medium">
                                        <a className="uk-button uk-button-text">Continue reading</a>
                                    </p>
                                </div>
                            </article>
                        </div>
                        <div>
                            <article className="uk-article">
                                <div className="uk-text-center uk-margin-top">
                                    <img data-src={SERVER_IMG+"news-5.jpeg"} uk-img="true"/>
                                    <h2 className="uk-margin-medium-top uk-margin-remove-bottom uk-text-center uk-article-title">
                                        <a className="uk-link-reset">Keep the good work going</a>
                                    </h2>
                                    <p className="uk-margin-top uk-margin-remove-bottom uk-article-meta uk-text-center">
                                        Written by <a>admin</a> 
                                        on November 29, 2018.               
                                        Posted in <a rel="category">News</a>.                
                                        <a>5 Comments</a>                
                                    </p>
                                    <div className="uk-margin-medium-top">
                                        Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.
                                        Quis aute iure reprehenderit...                            
                                    </div>
                                    <p className="uk-text-center uk-margin-medium">
                                        <a className="uk-button uk-button-text">Continue reading</a>
                                    </p>
                                </div>
                            </article>
                        </div>
                        <div>
                            <article className="uk-article">
                                <div className="uk-text-center uk-margin-top">
                                    <img data-src={SERVER_IMG+"news-6.jpeg"} uk-img="true"/>
                                    <h2 className="uk-margin-medium-top uk-margin-remove-bottom uk-text-center uk-article-title">
                                        <a className="uk-link-reset">For the first time, an app really solves all problems</a>
                                    </h2>
                                    <p className="uk-margin-top uk-margin-remove-bottom uk-article-meta uk-text-center">
                                        Written by <a>admin</a> 
                                        on November 29, 2018.               
                                        Posted in <a rel="category">News</a>.                
                                        <a>5 Comments</a>                
                                    </p>
                                    <div className="uk-margin-medium-top">
                                        Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.
                                        Quis aute iure reprehenderit...                            
                                    </div>
                                    <p className="uk-text-center uk-margin-medium">
                                        <a className="uk-button uk-button-text">Continue reading</a>
                                    </p>
                                </div>
                            </article>
                        </div>
                        
                    </div>
                </div> 
            </div>
        )
    }
}

export default Newsroom;