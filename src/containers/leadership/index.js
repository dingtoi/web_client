import React from "react";
import {SERVER_IMG} from "../../../config";

class Leadership extends React.Component{
    constructor(props){
        super(props);
    }
   
    render(){
        return (
            <div>
                <div className="uk-background-cover uk-height-large uk-panel uk-flex uk-flex-center uk-flex-middle uk-light" 
                style={{backgroundImage:'url('+SERVER_IMG+'banner-leadership.jpg)'}}>
                    <p className="uk-h1">Leadership</p>
                </div>
                <div className="uk-container uk-padding-large">
                    <div className="uk-grid uk-grid-margin" uk-grid="true">
                        <div className="uk-width-expand@m uk-width-1-2@s">
                            <div className="uk-margin uk-panel uk-scrollspy-inview uk-animation-slide-bottom-medium" uk-scrollspy-class="true">
                                <a>
                                    <img className="el-image uk-box-shadow-large uk-box-shadow-hover-large" alt="" data-src={SERVER_IMG+"leader-1.jpg"} uk-img="true"/>
                                </a>
                                <h3 className="el-title uk-margin uk-margin-remove-adjacent uk-margin-remove-bottom">Emily Jeffreys</h3>
                                <div className="el-meta uk-margin uk-text-muted">Web Designer at Sonify</div>
                            </div>
                        </div>
                        <div className="uk-width-expand@m uk-width-1-2@s">
                            <div className="uk-margin uk-panel uk-scrollspy-inview uk-animation-slide-bottom-medium" uk-scrollspy-class="true">
                                <a>
                                    <img className="el-image uk-box-shadow-large uk-box-shadow-hover-large" alt="" data-src={SERVER_IMG+"leader-2.jpg"} uk-img="true"/>
                                </a>
                                <h3 className="el-title uk-margin uk-margin-remove-adjacent uk-margin-remove-bottom">Steven Lee</h3>
                                <div className="el-meta uk-margin uk-text-muted">Founder of Newton </div>
                            </div>
                        </div>
                        <div className="uk-width-expand@m uk-width-1-2@s">
                            <div className="uk-margin uk-panel uk-scrollspy-inview uk-animation-slide-bottom-medium" uk-scrollspy-class="true">
                                <a>
                                    <img className="el-image uk-box-shadow-large uk-box-shadow-hover-large" alt="" data-src={SERVER_IMG+"leader-3.jpg"} uk-img="true"/>
                                </a>
                                <h3 className="el-title uk-margin uk-margin-remove-adjacent uk-margin-remove-bottom">Steven Lee</h3>
                                <div className="el-meta uk-margin uk-text-muted">Founder of Newton </div>
                            </div>
                        </div>
                        <div className="uk-width-expand@m uk-width-1-2@s">
                            <div className="uk-margin uk-panel uk-scrollspy-inview uk-animation-slide-bottom-medium" uk-scrollspy-class="true">
                                <a>
                                    <img className="el-image uk-box-shadow-large uk-box-shadow-hover-large" alt="" data-src={SERVER_IMG+"leader-4.jpg"} uk-img="true"/>
                                </a>
                                <h3 className="el-title uk-margin uk-margin-remove-adjacent uk-margin-remove-bottom">Steven Lee</h3>
                                <div className="el-meta uk-margin uk-text-muted">Founder of Newton </div>
                            </div>
                        </div>
                    </div>
                    <div className="uk-grid uk-grid-margin" uk-grid="true">
                        <div className="uk-width-expand@m uk-width-1-2@s">
                            <div className="uk-margin uk-panel uk-scrollspy-inview uk-animation-slide-bottom-medium" uk-scrollspy-class="true">
                                <a>
                                    <img className="el-image uk-box-shadow-large uk-box-shadow-hover-large" alt="" data-src={SERVER_IMG+"leader-5.jpg"} uk-img="true"/>
                                </a>
                                <h3 className="el-title uk-margin uk-margin-remove-adjacent uk-margin-remove-bottom">Emily Jeffreys</h3>
                                <div className="el-meta uk-margin uk-text-muted">Web Designer at Sonify</div>
                            </div>
                        </div>
                        <div className="uk-width-expand@m uk-width-1-2@s">
                            <div className="uk-margin uk-panel uk-scrollspy-inview uk-animation-slide-bottom-medium" uk-scrollspy-class="true">
                                <a>
                                    <img className="el-image uk-box-shadow-large uk-box-shadow-hover-large" alt="" data-src={SERVER_IMG+"leader-6.jpg"} uk-img="true"/>
                                </a>
                                <h3 className="el-title uk-margin uk-margin-remove-adjacent uk-margin-remove-bottom">Steven Lee</h3>
                                <div className="el-meta uk-margin uk-text-muted">Founder of Newton </div>
                            </div>
                        </div>
                        <div className="uk-width-expand@m uk-width-1-2@s">
                            <div className="uk-margin uk-panel uk-scrollspy-inview uk-animation-slide-bottom-medium" uk-scrollspy-class="true">
                                <a>
                                    <img className="el-image uk-box-shadow-large uk-box-shadow-hover-large" alt="" data-src={SERVER_IMG+"leader-7.jpg"} uk-img="true"/>
                                </a>
                                <h3 className="el-title uk-margin uk-margin-remove-adjacent uk-margin-remove-bottom">Steven Lee</h3>
                                <div className="el-meta uk-margin uk-text-muted">Founder of Newton </div>
                            </div>
                        </div>
                        <div className="uk-width-expand@m uk-width-1-2@s">
                            <div className="uk-margin uk-panel uk-scrollspy-inview uk-animation-slide-bottom-medium" uk-scrollspy-class="true">
                                <a>
                                    <img className="el-image uk-box-shadow-large uk-box-shadow-hover-large" alt="" data-src={SERVER_IMG+"leader-8.jpg"} uk-img="true"/>
                                </a>
                                <h3 className="el-title uk-margin uk-margin-remove-adjacent uk-margin-remove-bottom">Steven Lee</h3>
                                <div className="el-meta uk-margin uk-text-muted">Founder of Newton </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        )
    }
}

export default Leadership;