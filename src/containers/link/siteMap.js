import React from "react";
import {SERVER_IMG} from "../../../config";

class SiteMap extends React.Component{
    render(){
        return(
            <div>
                <div className="uk-background-cover uk-height-large uk-panel uk-flex uk-flex-column uk-flex-center uk-flex-middle" 
                    style={{backgroundImage: 'url('+SERVER_IMG+'banner.jpg)'}}>
                    <h1 className="uk-text-white">Site map</h1>
                </div>
                <div className="uk-container uk-padding">
                    <div className="uk-grid uk-child-width-1-3" uk-grid="true">
                        <div>
                            <p className="uk-text-bold uk-margin-xsmall-bottom">Account</p>
                            <ul className="uk-list uk-margin-remove">
                                <li><a>Login</a></li>
                                <li><a>Register</a></li>
                                <li><a>My account</a></li>
                                <li><a>My wishlist</a></li>
                            </ul>
                        </div>
                        <div>
                            <p className="uk-text-bold uk-margin-xsmall-bottom">Category</p>
                            <ul className="uk-list uk-margin-remove">
                                <li><a>Smart phones</a></li>
                                <li><a>Tablets</a></li>
                                <li><a>Smart Watches</a></li>
                            </ul>
                        </div>
                        <div>
                            <p className="uk-text-bold uk-margin-xsmall-bottom">Purpose</p>
                            <ul className="uk-list uk-margin-remove">
                                <li><a>Sale</a></li>
                                <li><a>Buy</a></li>
                                <li><a>Exchange</a></li>
                            </ul>
                        </div>
                        <div>
                            <p className="uk-text-bold uk-margin-xsmall-bottom">About Dingtoi</p>
                            <ul className="uk-list uk-margin-remove">
                                <li><a>Newroom</a></li>
                                <li><a>Leadership</a></li>
                                <li><a>Job Opportunities</a></li>
                                <li><a>Investors</a></li>
                                <li><a>Contact Dingtoi</a></li>
                            </ul>
                        </div>
                        <div>
                            <p className="uk-text-bold uk-margin-xsmall-bottom">Product</p>
                            <ul className="uk-list uk-margin-remove">
                                <li><a>Smart Phones</a></li>
                                <li><a>Tablets</a></li>
                                <li><a>Smart Watches</a></li>
                                <li><a>Sell Products</a></li>
                                <li><a>Contact Dingtoi</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default SiteMap;