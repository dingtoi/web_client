import React from "react";

import DeviceModel from "../../../models/device";
import { SERVER_DOMAIN, MICRO_DEVICE, MICRO_DOMAIN, MICRO_EVENT } from "../../../config";
import { Cover, Spinner, Radio, Checkbox } from "../../../components/common";
import RelatedDevices from "../../../components/desktop/relatedDevices";
import axios from "axios";

import {connect} from "react-redux";
import {addCart} from "../../../src/containers/home/actions";
import CartModel from "../../../models/cart";

import withStorage from "../../../hoc/storage";
import { is } from "redux-saga/utils";

class DeviceDetail extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            detail: {},
            list: []
        }
        this.renderPrice = this.renderPrice.bind(this);
        this.radioValue = '';
    }
    handerImageList(){
        this.setState({isLoading:true});
        DeviceModel.getListImage({device_id:this.id})
        .then((list)=>{
            this.setState({isLoading:false,list:list},()=>{
                UIkit.slideshow($(this.slideshow));
            });
        })
        .catch(() => {
            this.setState({ isLoading: true });
        })
    }
    componentWillMount(){
        this.id = this.props.match.params.id;
    }
    componentDidMount(){
        this.refreshDetail();
        this.handerImageList();
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.count){
            this.setState({isLoading: true});
            DeviceModel.getDeviceDetail({id: this.id, email: this.props.auth})
            .then((result) => {
                this.setState({isLoading: false, detail: result}, () => {
                    if(result.cart_type == null){
                        this.setState({
                            sale: 0,
                            exchange: 0
                        }, () => {
                            this.radioValue = '';
                        });
                    }else{
                        if(result.cart_type === 1) this.setState({sale: 1});
                        else if(result.cart_type === 2) this.setState({exchange: 2});
                    }
                });
            })
            .catch(() => this.setState({isLoading: false}))
        }
    }
    refreshDetail(){
        this.setState({isLoading: true});
        DeviceModel.getDeviceDetail({id: this.id, email: this.props.auth})
        .then((result) => {
            this.setState({isLoading: false, detail: result}, () => {
                if(result.cart_type){
                    if(result.cart_type === 1) this.setState({sale: 1});
                    else if(result.cart_type === 2) this.setState({exchange: 2});
                }
            });
        })
        .catch(() => this.setState({isLoading: false}))
    }
    handlerAddToCart(){
        const type = this.state.detail.type;
        if(type < 3){
           this.handlerActionType(type);
        }else if(type === 3){
            if(this.radioValue === ''){
                UIkit.notification({message: 'Must Choose Sale Or Exchange !!!', status: 'danger', pos: 'top-left'});
                return;   
            }
            if(this.radioValue === 'sale')
                this.handlerActionType(1);
            else if(this.radioValue === 'exchange')
                this.handlerActionType(2);
        }
    }
    handlerActionType(type){
        this.setState({isLoading: true});
        this.handlerActionCart(type);
    }
    handlerActionCart(type){
        const {available_id, id} = this.state.detail;
        axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/add',{name: 'Add to cart', key: 'ADD_TO_CART', desc: ''})
        .then((created) => {
            CartModel.postItemToCart({available_id, device_id: id, email: this.props.auth, type})
            .then(() => {
                this.props.addCart();
                this.refreshDetail();
            })
            .then(()=>{
                axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/changeStatus',{id: created})
                .then(() => {
                })
                .catch(() => {
            })
            })
            .catch(() => {})
        })
        .catch(() => {}) 
    }
    handlerChangeRadioSale(event){
        const value = event.target.value;
        this.setState({
            sale: value
        });
        this.radioValue = 'sale';
    }
    handlerChangeRadioExchange(event){
        const value = event.target.value;
        this.setState({
            exchange: value
        });
        this.radioValue = 'exchange';
    }
    handlerWishlist(){
        const {isWishlist, id} = this.state.detail;
        this.setState({isLoading: true});
        if(!isWishlist){
            DeviceModel.addWishlist({email: this.props.auth, device_id: id})
            .then(() => {
                this.refreshDetail();
            })
            .catch(() => {
                this.setState({isLoading: false});
            })
        }else if(isWishlist){
            DeviceModel.removeWishlist({id: isWishlist})
            .then(() => {
                this.refreshDetail();
            })
            .catch(() => {
                this.setState({isLoading: false});
            })
        }
    }
    renderPrice(){
        const type = this.state.detail.type;
        if(type === 1){
            return (
                <li>
                    <div className="uk-width-medium uk-flex-middle uk-flex uk-flex-between">
                        <div>
                            <Radio label={'Sale'} mode="basic" fontSize={'16px'} value="sale" name="detail_available"
                                disabled={this.state.detail.cart_id ? true: false}
                                defaultChecked={true}/>
                        </div>
                        <span className="uk-text-large">{accounting.formatMoney(this.state.detail.sale_price)}</span>
                    </div>
                </li>
            )
        }else if(type === 2){
            return (
                <li>
                    <div className="uk-width-medium uk-flex-middle uk-flex uk-flex-between">
                        <div>
                            <Radio label={'Exchange'} mode="basic" fontSize={'16px'} value="exchange" name="detail_available"
                                disabled={this.state.detail.cart_id ? true: false}
                                defaultChecked={true}/>
                            <div className="uk-flex uk-text-meta uk-flex-middle uk-margin-xsmall-top">
                                <i className="fa fa-exchange"/>
                                <div className="uk-margin-xsmall-left uk-text-meta uk-text-truncate">Exchange With: {this.state.detail.exchange_name}</div>
                            </div>
                        </div>
                        <span className="uk-text-large">{accounting.formatMoney(this.state.detail.exchange_price)}</span>
                    </div>
                </li>
            )
        }else if(type === 3){
            return (
                <React.Fragment>
                    <li>
                        <div className="uk-width-medium uk-flex-middle uk-flex uk-flex-between">
                            <div>
                                <Radio label={'Sale'} mode="basic" fontSize={'16px'} value="1" name="detail_available"
                                    checked={this.state.sale == 1}
                                    disabled={this.state.detail.cart_id ? true: false}
                                    onChange={this.handlerChangeRadioSale.bind(this)}/>
                            </div>
                            <span className="uk-text-large">{accounting.formatMoney(this.state.detail.sale_price)}</span>
                        </div>
                    </li>
                    <li>
                        <div className="uk-width-medium uk-flex-middle uk-flex uk-flex-between">
                            <div>
                                <Radio label={'Exchange'} mode="basic" fontSize={'16px'} value="2" name="detail_available"
                                    checked={this.state.exchange == 2}
                                    disabled={this.state.detail.cart_id ? true: false}
                                    onChange={this.handlerChangeRadioExchange.bind(this)}/>
                                <div className="uk-flex uk-text-meta uk-flex-middle uk-margin-xsmall-top">
                                    <i className="fa fa-exchange"/>
                                    <div className="uk-margin-xsmall-left uk-text-meta uk-text-truncate">Exchange With: {this.state.detail.exchange_name}</div>
                                </div>
                            </div>
                            <span className="uk-text-large">{accounting.formatMoney(this.state.detail.exchange_price)}</span>
                        </div>
                    </li>
                </React.Fragment>
            )
        }
    }
    render(){
        return (
            <div className="uk-container">
                <ul className="uk-breadcrumb uk-margin-top uk-margin-large-bottom">
                    <li><a onClick={() => this.props.history.push('/')}>Home</a></li>
                    <li><span>{this.state.detail.device_name}</span></li>
                </ul>
                <div className="uk-inline uk-width-1-1">
                    {this.state.isLoading && <Cover/>}
                    {this.state.isLoading && <Spinner/>}
                    <div className="uk-grid uk-margin-large-bottom uk-flex uk-flex-middle" uk-grid="true">
                        <div className="uk-width-1-3@l uk-width-1-2@m uk-with-1-1@s">
                        {
                            this.state.list.length > 0
                            ?
                            <div className="uk-position-relative uk-visible-toggle" data-uk-slideshow>
                                <ul className="uk-slideshow-items uk-height-img-device@l uk-height-img-device@m uk-height-img-device@s" data-uk-lightbox="animation: slide">
                                {
                                    this.state.list.map((l,key)=>{
                                        return(
                                            <li key={key}>
                                                <a className="uk-inline" href={MICRO_DOMAIN+MICRO_DEVICE + '/devices/'+l.url} alt={this.state.detail.device_name}
                                                    ref={instance => {
                                                        if(key === 0)
                                                            this.image = instance
                                                    }}>
                                                    <img data-src={MICRO_DOMAIN+MICRO_DEVICE + '/devices/'+l.url} alt={this.state.detail.device_name} uk-img="true"/>
                                                </a>
                                            </li>     
                                        )
                                    })
                                    
                                }
                                </ul>
                            </div>
                            :
                            <img data-src={SERVER_DOMAIN+'no-image.png'} uk-img="true"
                                ref={instance => {
                                    this.image = instance
                                }}/>
                        }
                        </div>
                        <div className="uk-width-2-3@l uk-width-1-2@m uk-with-1-1@s">
                            <h2>{this.state.detail.device_name}</h2>
                            <div className="uk-grid uk-margin" uk-grid="true">
                                <div className="uk-width-1-2@l uk-width-1-1@s">
                                    <ul className="uk-list">
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Category:</div>
                                                <span>{this.state.detail.category_name}</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Brand:</div>
                                                <span>{this.state.detail.brand_name}</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Color:</div>
                                                <span>{this.state.detail.color_name}</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div className="uk-width-1-2@l uk-width-1-1@s">
                                    <ul className="uk-list">
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">RAM:</div>
                                                <span>{this.state.detail.ram_name}</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Capacity:</div>
                                                <span>{this.state.detail.capacity_name}</span>
                                            </div>
                                        </li>
                                        <li className="uk-flex">
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Condition:</div>
                                                <span>{this.state.detail.device_condition} %</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <hr/>
                            <div>
                                <ul className="uk-list uk-list-large">
                                    {this.renderPrice()}
                                </ul>
                                <div className="uk-flex uk-flex-middle">
                                    {
                                        this.state.detail.cart_id 
                                        ?
                                        <button className="uk-button uk-button-secondary" disabled={true}>Product Is In Your Cart</button>
                                        :
                                        <button className="uk-button uk-button-primary"
                                            onClick={this.handlerAddToCart.bind(this)} ref={instance => this.atcRef = instance}>Add To Cart</button>
                                    }
                                    {
                                        !this.state.detail.cart_id
                                        ?
                                        <span className="uk-margin-left">
                                            {
                                                this.state.detail.isWishlist
                                                ?
                                                <Checkbox mode="heart" fontSize={20} defaultChecked={true}
                                                    onClick={this.handlerWishlist.bind(this)}/>
                                                :
                                                <Checkbox mode="heart" fontSize={20}
                                                    onClick={this.handlerWishlist.bind(this)}/>
                                            }
                                        </span>
                                        : null
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <RelatedDevices device_id={this.id}/>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const mapDispatchToProps = {
    addCart
}

const ConnectDeviceDetail = connect(mapStateToProps, mapDispatchToProps)(withStorage(DeviceDetail));

export default ConnectDeviceDetail;