import React from "react";
import UserModel from "../../../models/user";
import axios from "axios";
import {MICRO_AUTH, MICRO_DOMAIN} from "../../../config";
import {ImageUpload} from "../../../components/common";

import withStorage from "../../../hoc/storage";

class AccountAvatar extends React.Component{
    constructor(props){
        super(props);
        this.imageUpload = null;
    }

    componentDidMount(){
        this.handlerApiImages();
    }
    handlerApiImages(){
        this.imageUpload.beforeRefresh();
        UserModel.userAvatar({email: this.props.auth})
        .then(result => {
            this.imageUpload.refreshList(result);
        })
        .catch(error => {
            this.imageUpload.afterRefresh();
        })
    }
    handlerApiDelete(){
        this.imageUpload.beforeRefresh();
        UserModel.deleteUserAvatar({email: this.props.auth})
        .then(result => {
            this.handlerApiImages();
            this.props.root.handlerAvatar();
        })
        .catch(error => {
            this.imageUpload.afterRefresh();
        })
    }
    handlerApiUploadImage(formData){
        return axios({
            method: 'post',
            url: MICRO_DOMAIN+MICRO_AUTH+'/api/user/imageUpload',
            data: formData
        })
    }
    handlerServerUpload(files){
        const formData = new FormData();
        formData.append('photo', files[0]);
        formData.append('email', this.props.auth);
        this.handlerApiUploadImage(formData)
        .then((rs) => {
            this.handlerApiImages();
            this.props.root.handlerAvatar();
        })
        .catch(error => {
            if(error.status === 400)
                UIkit.notification({message: 'This Image Cannot Upload', status: 'error', pos: 'top-left'});
        })
    }
    render(){
        return(
            <div className="uk-padding-small">
               
                <div className="uk-container uk-margin-top">
                    <ImageUpload maxFiles={1}
                        folder="users"
                        imageName="image"
                        domainName={MICRO_DOMAIN+MICRO_AUTH}
                        ref={instance => this.imageUpload = instance}
                        onChange={this.handlerServerUpload.bind(this)}
                        onDelete={this.handlerApiDelete.bind(this)}
                    />
                </div>
            </div>
        )
    }
}
export default withStorage(AccountAvatar);



