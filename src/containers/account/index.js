import React from "react";
import withStorage from "../../../hoc/storage";
import { Route } from "react-router-dom";
import { MICRO_AUTH, MICRO_DOMAIN} from "../../../config";
import UserModel from "../../../models/user";


class Account extends React.Component{
    constructor(props){
        super(props);
        this.state={
            avatar: null,
            billing_name:null
        }
    }
    componentDidMount(){
        this.handlerAvatar();
    }
    handlerAvatar(){
        UserModel.userAvatar({email: this.props.auth})
        .then((rs) => {
            this.setState({avatar:rs[0].image, billing_name:rs[0].billing_name})
        })
        .catch(error => {
        })
    }
    render(){
        return (
            <div>
                <div className="uk-background-primary">
                    <div className="uk-container uk-height-medium uk-flex uk-flex-middle">
                        <div className="uk-cursor" onClick={() => this.props.history.push('/account/avatar')}> 
                        {
                            this.state.avatar
                            ?
                            <img className="uk-border-circle" src={MICRO_DOMAIN+MICRO_AUTH+"/users/"+this.state.avatar}
                                width="80" height="80"/>
                            :
                            <img className="uk-border-circle" src={MICRO_DOMAIN+MICRO_AUTH+"/no-image.png"}
                                width="80" height="80"/>
                        }
                            

                        </div>
                        <div className="uk-light uk-margin-small-left">
                            <h3 className="uk-margin-xsmall-bottom">{this.props.auth}</h3>
                            <div className="uk-margin-xsmall-bottom uk-text-small">Swap*Ez Dear {this.state.billing_name}</div>
                            <a className="uk-text-meta uk-link-reset" onClick={() => this.props.history.push('/account/information')}>Edit Profile</a>
                        </div>
                    </div>
                </div>
                <div className="uk-container uk-margin-top uk-margin-bottom">
                    <ul className="uk-tab">
                        <li onClick={() => this.props.history.push('/account/device')}
                            className={this.props.location.pathname === '/account/device' ? 'uk-active': null}>
                            <a>My Devices</a>
                        </li>
                        <li onClick={() => this.props.history.push('/account/wishlist')}
                            className={this.props.location.pathname === '/account/wishlist' ? 'uk-active': null}>
                            <a>My Wishlist</a>
                        </li>
                        <li onClick={() => this.props.history.push('/account/received_proposal')}
                            className={this.props.location.pathname === '/account/received_proposal' ? 'uk-active': null}>
                            <a>Received Proposal</a>
                        </li>
                        <li>
                            <a>More <span className="uk-margin-small-left" uk-icon="icon: triangle-down"></span></a>
                            <div uk-dropdown="mode: click">
                                <ul className="uk-nav uk-dropdown-nav">
                                    <li><a href="#" onClick={() => this.props.history.push('/account/change_password')}>Change Password</a></li>
                                    <li><a href="#" onClick={() => this.props.history.push('/account/information')}>Account Information</a></li>
                                    <li><a href="#" onClick={() => this.props.history.push('/account/order')}>Order History</a></li>
                                    <li><a href="#" onClick={() => this.props.history.push('/account/transactions')}>Transaction Management</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    {
                        this.props.routes.map((route, i) => (
                            <Route exact path={route.path} key={route.path}
                                render={
                                    props => (
                                        <route.component {...props} root={this} socket={this.props.socket}/>
                                    )
                                }/>
                        ))
                    }
                </div>
            </div>
        )
    }
}

export default withStorage(Account);