import React from "react";
import { Cover, Spinner, InputNumber } from "../../../components/common";
import axios from "axios";
import CartModel from "../../../models/cart";
import Functions from "../../../functions";
import withStorage from "../../../hoc/storage";
import { SERVER_DOMAIN, MICRO_DOMAIN, MICRO_DEVICE, MICRO_EVENT } from "../../../config";

class ReceivedProposalSale extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            detail: {
            },
            sale_price: '',
            isLoading: false
        }
    }
    componentWillMount(){
        this.id = this.props.match.params.id;
        this.available_id = this.props.match.params.available_id;
    }
    componentDidMount(){
        this.refreshDetail();
    }
    refreshDetail(){
        this.setState({isLoading: true});
        CartModel.getDetailReceivedProposal({email: this.props.auth, id: this.id})
        .then((result) => {
            this.setState({isLoading: false, detail: result, sale_price: result.sale_proposal_price});
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    handlerSubmit(){
        if(is.empty(this.state.sale_price))
            UIkit.notification({message: 'Must Choose Price', status: 'danger', pos: 'top-left'});
        else{
            this.setState({isLoading: true});
            axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/add',{name: 'Reply proposal', key: 'REPLY_PROPOSAL', desc: ''})
            .then((created) => {
                CartModel.editProposal({email: this.state.detail.user_id, cart_id: this.state.detail.cart_id, type: 1,
                    sale_proposal_price: this.state.sale_price, exchange_proposal_device: null, exchange_proposal_price: null,
                    id: this.id, status: 'submitted'})
                .then(() => {
                    UIkit.notification({message: 'Reply Proposal Successful', status: 'primary', pos: 'top-left'});
                    Functions.socketSendProposal(this.props.socket, this.state.detail.received_email);
                    this.props.history.push('/account/received_proposal');
                })
                .then(()=>{
                    axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/changeStatus',{id: created})
                    .then(() => { })
                    .catch(() => {})
                })
                .catch(() => {
                    this.setState({isLoading: false});
                })
            })
            .catch(() => {}) 
        }
    }
    handlerCancel(){
        axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/add',{name: 'Remove proposal', key: 'REMOVE_PROPOSAL', desc: ''})
        .then((created) => {
            CartModel.editProposal({email: this.state.detail.user_id, cart_id: this.state.detail.cart_id, type: 1,
                sale_proposal_price: this.state.detail.sale_proposal_price, exchange_proposal_device: null, exchange_proposal_price: null,
                id: this.id, status: 'cancelled'})
            .then(() => {
                UIkit.notification({message: 'Cancel Proposal Successful', status: 'primary', pos: 'top-left'});
                Functions.socketSendProposal(this.props.socket, this.state.detail.received_email);
                this.props.history.push('/account/received_proposal');
            })
            .then(()=>{
                axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/changeStatus',{id: created})
                .then(() => { })
                .catch(() => {})
            })
            .catch(() => {
                this.setState({isLoading: false});
            })
        })
        .catch(() => {}) 
    }
    handlerAccept(){
        axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/add',{name: 'Accepted proposal', key: 'ACCEPTED_PROPOSAL', desc: ''})
        .then((created) => {
            CartModel.editProposal({email: this.state.detail.user_id, cart_id: this.state.detail.cart_id, type: 1,
                sale_proposal_price: this.state.detail.sale_proposal_price, exchange_proposal_device: null, exchange_proposal_price: null,
                id: this.id, status: 'accepted'})
            .then(() => {
                UIkit.notification({message: 'Accept Proposal Successful', status: 'primary', pos: 'top-left'});
                Functions.socketSendProposal(this.props.socket, this.state.detail.received_email);
                this.props.history.push('/account/received_proposal');
            })
            .then(()=>{
                axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/changeStatus',{id: created})
                .then(() => { })
                .catch(() => {})
            })
            .catch(() => {
                this.setState({isLoading: false});
            })
        })
        .catch(() => {})
    }
    render(){
        return (
            <div>
                <ul className="uk-breadcrumb">
                    <li><a onClick={() => this.props.history.push('/account/received_proposal')}>List Proposal</a></li>
                    <li><span>{this.state.detail.device_name}</span></li>
                </ul>
                <div className="uk-position-relative uk-margin-large-bottom">
                    <h3>Proposal For {this.state.detail.device_name}</h3>
                    <div className="uk-grid uk-flex uk-flex-top" uk-grid="true">
                        {this.state.isLoading && <Cover/>}
                        {this.state.isLoading && <Spinner/>}
                        <div className="uk-width-1-3@l uk-width-1-2@m uk-with-1-1@s">
                        {
                            this.state.detail.thumb
                            ?
                            <div className="uk-flex uk-flex-center">
                                <div>
                                    <img className="uk-img uk-cursor" src={MICRO_DOMAIN+MICRO_DEVICE+'/devices/'+this.state.detail.thumb} uk-img="true"
                                        style={{maxHeight: '290px'}}/>
                                </div>
                            </div>
                            :
                            <div className="uk-flex uk-flex-center">
                                <div>
                                    <img src={SERVER_DOMAIN+'no-image.png'} style={{maxHeight: '290px'}}/>
                                </div>
                            </div>
                        }
                        </div>
                        <div className="uk-width-2-3@l uk-width-1-2@m uk-with-1-1@s">
                            <h2>{this.state.detail.device_name}</h2>
                            <div className="uk-grid uk-margin" uk-grid="true">
                                <div className="uk-width-1-1">
                                    <ul className="uk-list">
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Brand:</div>
                                                <span>{this.state.detail.brand_name}</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Color:</div>
                                                <span>{this.state.detail.color_name}</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Capacity:</div>
                                                <span>{this.state.detail.capacity_name}</span>
                                            </div>
                                        </li>
                                    </ul>
                                    <div className="uk-flex uk-flex-column uk-margin uk-width-medium">
                                        <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="proposal_sale_price">Proposal Sale Price</label>
                                        <InputNumber value={this.state.sale_price} icon={"credit-card"}
                                            onChange={event => this.setState({sale_price: event.target.value})}/>
                                    </div>
                                    <div>
                                        {
                                            this.state.detail.proposal_status !== 'cancelled' && this.state.detail.proposal_status !== 'accepted'
                                            ?
                                            <button className="uk-button uk-button-default"
                                                onClick={this.handlerCancel.bind(this)}>Cancel</button>
                                            : null
                                        }
                                        {
                                            this.state.detail.proposal_status !== 'accepted'
                                            ?
                                            <button className="uk-button uk-button-secondary uk-margin-xsmall-left"
                                                onClick={this.handlerSubmit.bind(this)}>Reply</button>
                                            : null
                                        }
                                        {
                                            this.state.detail.proposal_status !== 'cancelled' && this.state.detail.proposal_status !== 'accepted'
                                            ?
                                            <button className="uk-button uk-button-primary uk-margin-xsmall-left"
                                                onClick={this.handlerAccept.bind(this)}>Accept</button>
                                            : null
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withStorage(ReceivedProposalSale);