import React from "react";
import DeviceWishlist from "../../../components/desktop/wishlistDevices";

class Wishlist extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div>
                <ul className="uk-breadcrumb">
                    <li><a onClick={() => this.props.history.push('/account/device')}>List Device</a></li>
                    <li><span>Wishlist</span></li>
                </ul>
                
                <DeviceWishlist/>
            </div>
        )
    }
}

export default Wishlist;