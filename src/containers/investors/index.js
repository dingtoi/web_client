import React from "react";
import {SERVER_IMG} from "../../../config";

class Investors extends React.Component{
    constructor(props){
        super(props);
    }
   
    render(){
        return (
            <div>
                <div className="uk-background-cover uk-height-large uk-panel uk-flex uk-flex-center uk-flex-middle uk-light" style={{backgroundImage: 'url('+SERVER_IMG+'banner-investors.jpg)'}}>
                    <p className="uk-h1">Swap-ez Investor News</p>
                </div>
                <div className="uk-container uk-padding-large">
                    <div className="uk-text-center uk-padding-large uk-padding-remove-top">
                        <h2 className="uk-margin-small-bottom">FY 18 Fourth Quarter Results</h2>
                        <p className="uk-margin-small-bottom uk-margin-remove-top uk-text-xlarge">Swap-ez announced results for the quarter ended September 29, 2018.</p>
                        <a>View the press release</a>
                    </div>
                    <hr/>
                    <div className="uk-text-center uk-padding-large">
                        <h2 className="uk-margin-small-bottom">FY 18 Annual Results</h2>
                        <div className="uk-margin-small-bottom"><a>View the Form 10-K</a></div>
                        <div><a>Reclassification of net sales by category</a></div>
                    </div>
                    <div className="uk-grid-collapse uk-child-width-1-2 uk-text-center" uk-grid="true">
                        <div>
                            <a>
                                <div className="uk-margin-xsmall-right uk-margin-xsmall-bottom">
                                    <img data-src={SERVER_IMG+"investor-1.jpg"} uk-img="true"/>
                                </div>
                            </a>
                            
                        </div>
                        <div>
                            <a>
                                <div className="uk-margin-xsmall-right uk-margin-xsmall-bottom">
                                    <img data-src={SERVER_IMG+"investor-2.jpg"} uk-img="true"/>
                                </div>
                            </a>
                        </div>
                        <div>
                            <a>
                                <div className="uk-margin-xsmall-right uk-margin-xsmall-bottom">
                                    <img data-src={SERVER_IMG+"investor-3.jpg"} uk-img="true"/>
                                </div>
                            </a>
                            
                        </div>
                        <div>
                            <a>
                                <div className="uk-margin-xsmall-right uk-margin-xsmall-bottom">
                                    <img data-src={SERVER_IMG+"investor-4.jpg"} uk-img="true"/>
                                </div>
                            </a>
                        </div>
                        
                    </div>
               </div> 
            </div>
        )
    }
}

export default Investors;