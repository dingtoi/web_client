import React from "react";
import {addCart} from "../../../src/containers/home/actions";
import {connect} from "react-redux";

import withStorage from "../../../hoc/storage";

import CartModel from "../../../models/cart";
import axios from "axios";

import { Cover, Spinner, Modal, Alert } from "../../../components/common";
import { SERVER_DOMAIN, MICRO_DEVICE, MICRO_DOMAIN, MICRO_EVENT, MICRO_CART } from "../../../config";
import Functions from "../../../functions";

import SaleProposal from "../../../components/desktop/proposal/sale";
import SaleEditProposal from "../../../components/desktop/proposal/saleEdit";

import ExchangeProposal from "../../../components/desktop/proposal/exchange";
import ExchangeEditProposal from "../../../components/desktop/proposal/exchangeEdit";

class Cart extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            isModalSaleProposal: false,
            isModalExchangeProposal: false,
            isModalEditSaleProposal: false,
            isModalEditExchangeProposal: false,
            selectedCart: {},
            list: [],
            
        }
        this.renderCartList = this.renderCartList.bind(this);
        this.renderMoney = this.renderMoney.bind(this);
        this.renderBtnProposal = this.renderBtnProposal.bind(this);
        this.renderExchangeWith = this.renderExchangeWith.bind(this);
        this.handlerDeleteCartItem = this.handlerDeleteCartItem.bind(this);
        this.observer = null;
    }
    componentDidMount(){
        Functions.socketReceivedProposal(this.props.socket)
        .subscribe((observer) => {
            this.observer = observer;
            this.refreshListCart();
        })
        this.refreshListCart();
        
    }
    componentWillUnmount(){
        if(this.observer)
            this.observer.complete();
    }
    handlerGoToDevice(l){
        this.props.history.push('/device/'+l.device_id);
    }
 
    handlerDeleteCartItem(l){
        this.setState({isLoading: true});
        axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/add',{name: 'Remove item cart', key: 'REMOVE_ITEM_CART', desc: ''})
        .then((created) => {
            CartModel.deleteItemCart({id: l.id})
            .then(() => {
                this.props.addCart();
                this.refreshListCart();
            })
            .then(()=>{
                axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/changeStatus',{id: created})
                .then(() => {
                })
                .catch(() => {
            })
            })
            .catch(() => {
                this.setState({isLoading: false});
            })
        })
        .catch(() => {}) 
    }
    refreshListCart(){
        this.setState({isLoading: true});
        setTimeout(() => {
            if(this.props.auth){
                CartModel.getListCartByUser({email: this.props.auth})
                .then((result) => {
                    this.setState({list: result, isLoading: false});
                })
                .catch(() => {
                    this.setState({isLoading: false});
                })
            }
        }, 500)
    }
    renderMoney(l){
        if(Number(l.cart_type) === 1){
            if(l.proposal_id)
                return <div className="uk-flex uk-flex-column">
                    <div><b>Proposal Price</b> {accounting.formatMoney(l.sale_proposal_price)}</div>
                    <div><b>Sale Price</b> {accounting.formatMoney(l.sale_price)}</div>
                </div>
            else
                return <b>{accounting.formatMoney(l.sale_price)}</b>
        }else if(Number(l.cart_type) === 2)
            if(l.proposal_id)
                return <div className="uk-flex uk-flex-column">
                        <div><b>Proposal Price</b> {accounting.formatMoney(l.exchange_proposal_price)}</div>
                    </div>
            else
                return <b>{accounting.formatMoney(l.exchange_price)}</b>
    }
    renderExchangeWith(l){
        if(!l.proposal_id)
            return (
                <div className="uk-text-break">
                    Exchange with <b>{l.exchange_name}</b>
                </div>
            )
        else return (
            <div className="uk-text-break">
                Exchange with <b>{l.exchange_proposal_real_device}</b>
            </div>
        )
    }
    renderBtnProposal(l){
        if(Number(l.cart_type) === 1){
            if(l.proposal_id){
                if(l.proposal_status === 'accepted' || l.proposal_status === 'cancelled')
                    return null;
                else return <div>
                    <a className="uk-text-primary" onClick={() => this.setState({isModalEditSaleProposal: true, selectedCart: l})}>Edit</a>
                    &nbsp; | &nbsp;
                    <a className="uk-text-primary" onClick={() => {
                        this.setState({isLoading: true})
                        axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/add',{name: 'Cancel Proposal', key: 'CANCEL_PROPOSAL', desc: ''})
                        .then((created) => {
                            CartModel.cancelProposal({id: l.proposal_id})
                            .then(() => {
                                Functions.socketSendProposal(this.props.socket, l.received_email);
                                this.refreshListCart();
                            })
                            .then(()=>{
                                axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/changeStatus',{id: created})
                                .then(() => {
                                })
                                .catch(() => {
                            })
                            })
                            .catch(() => {
                                this.setState({isLoading: false});
                            })
                        })
                        .catch(() => {}) 
                    }}>Remove</a>
                </div>
            }else
                return <a className="uk-text-primary" onClick={() => this.setState({isModalSaleProposal: true, selectedCart: l})}>Create</a>
        }
        else if(Number(l.cart_type) === 2)
        if(l.proposal_id){
            if(l.proposal_status === 'accepted' || l.proposal_status === 'cancelled')
                return null;
            else return <div>
                <a className="uk-text-primary" onClick={() => this.setState({isModalEditExchangeProposal: true, selectedCart: l})}>Edit</a>
                &nbsp; | &nbsp;
                <a className="uk-text-primary" onClick={() => {
                    this.setState({isLoading: true})
                    axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/add',{name: 'Cancel Proposal', key: 'CANCEL_PROPOSAL', desc: ''})
                    .then((created) => {
                        CartModel.cancelProposal({id: l.proposal_id})
                        .then(() => {
                            Functions.socketSendProposal(this.props.socket, l.received_email);
                            this.refreshListCart();
                        })
                        .then(()=>{
                            axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/changeStatus',{id: created})
                            .then(() => {
                            })
                            .catch(() => {
                        })
                        })
                        .catch(() => {
                            this.setState({isLoading: false});
                        })
                    })
                    .catch(() => {}) 
                }}>Remove</a>
            </div>
        }else
            return <a className="uk-text-primary" onClick={() => this.setState({isModalExchangeProposal: true, selectedCart: l})}>Create</a>
    }
    navigateToPreview(list, total){
        localStorage.setItem('listPreview', JSON.stringify(list));
        localStorage.setItem('listPreviewTotal', total);
        setTimeout(() => {
            this.props.history.push('/preview');
        }, 500);
    }
    handlerCheckout(){
        const obj = Functions.getListBeforePreview(this.state.list);
        if(obj.listPrev.length === 0){
            this.navigateToPreview(obj.list, obj.total);
        }else if(obj.listPrev.length > 0){
            if(obj.list.length > 0){
                UIkit.modal.confirm('You have some device has exchange not accepted, do you want to continue!').then(() => {
                    this.navigateToPreview(obj.list, obj.total);
                }, () => {
                    this.setState({listErr: obj.listPrev, list: obj.originalList});
                })
            }else{
                UIkit.modal.alert('Your Cart has error, please make proposal !!!')
                .then(() => {
                    this.setState({listErr: obj.listPrev, list: obj.originalList});
                })
            }
        }
    }
   
    renderCartList(){
        return (
            <div>
                <div className="uk-flex uk-padding-small uk-background-muted uk-text-white uk-text-center uk-visible@m">
                    <div className="uk-width-1-6"></div>
                    <div className="uk-width-1-6">ITEM IN YOUR BAG</div>
                    <div className="uk-width-1-6">TYPE</div>
                    <div className="uk-width-1-6">PRICE</div>
                    <div className="uk-width-1-6">STATUS</div>
                    <div className="uk-width-1-6">PROPOSAL</div>
                </div>
                {
                    this.state.list.map((l, key) => {
                        return (
                            <div className={"uk-flex uk-flex-middle uk-padding-small uk-text-center uk-border-propose "+(
                                l.prevent ? 'uk-background-danger uk-light': '' )} key={key} >
                                <div className="uk-width-1-6@m uk-width-1-3@s">
                                    <div className="uk-flex uk-flex-center uk-grid-divider uk-grid-small" uk-grid="true">
                                        <div>
                                        {
                                            l.thumb
                                            ?
                                            <a onClick={() => this.handlerGoToDevice(l)}>
                                                <img className="uk-img uk-cursor" src={MICRO_DOMAIN+MICRO_DEVICE+'/devices/'+l.thumb} width="105px" uk-img="true"/>
                                            </a>
                                            :
                                            <a onClick={() => this.handlerGoToDevice(l)}>
                                                <img src={SERVER_DOMAIN+'no-image.png'} width="105px"/>
                                            </a>
                                        }
                                        </div>
                                        {   
                                            Number(l.cart_type) === 2
                                            ?
                                                l.proposal_id 
                                                ?
                                                    l.exchange_proposal_real_device_thumb 
                                                    ?
                                                    <div>
                                                        <a >
                                                            <img src={MICRO_DOMAIN+MICRO_DEVICE+'/devices/'+l.exchange_proposal_real_device_thumb} width="105px"/>
                                                        </a>
                                                    </div>
                                                    :
                                                    <div>
                                                        <a >
                                                            <img src={SERVER_DOMAIN+'no-image.png'} width="105px"/>
                                                        </a>
                                                    </div>
                                                :
                                                null
                                            :
                                            null
                                        }   
                                    </div>
                                </div>
                                <div className="uk-width-5-6@m uk-flex uk-flex-between uk-flex uk-flex-row@m uk-flex-column@s uk-flex-middle uk-text-center">
                                    <div className="uk-width-1-5@m">
                                        <div className="uk-text-primary uk-margin-xsmall-bottom uk-cursor"
                                            onClick={() => this.handlerGoToDevice(l)}>{l.device_name}</div>
                                        <div className="uk-margin-xsmall-bottom">Brand: {l.brand_name}</div>
                                        <div className="uk-margin-xsmall-bottom">Color: {l.color_name}</div>
                                        <div className="uk-margin-xsmall-bottom">Capacity: {l.capacity_name}</div>
                                        <div className="uk-text-primary uk-text-underline uk-margin-xsmall-bottom uk-cursor"
                                            onClick={() => this.handlerDeleteCartItem(l)}>
                                            Remove
                                        </div>
                                    </div>
                                    <div className="uk-width-1-5@m uk-padding-small">
                                        {
                                            Number(l.cart_type) === 1
                                            ? <div>Sale</div>
                                            :
                                            this.renderExchangeWith(l)
                                        }
                                    </div>
                                    <div className="uk-width-1-5@m">
                                        {this.renderMoney(l)}
                                    </div>
                                    <div className="uk-width-1-5@m">
                                    {
                                        l.proposal_id
                                        ?
                                        <div>
                                            <div className="uk-text-capitalize">{l.proposal_status}</div>
                                            {
                                                l.proposal_status === 'submitted'
                                                ?
                                                <button className="uk-button uk-button-secondary uk-button-small uk-margin-xsmall-top"
                                                    onClick={() => {
                                                        axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/add',{name: 'Client Accepted Proposal', key: 'CLIENT_ACCEPTED_PROPOSAL', desc: ''})
                                                        .then((created) => {
                                                            axios.post(MICRO_DOMAIN+MICRO_CART+'/api/proposal/updateStatus',{id:l.proposal_id, status:'Client Accepted'})
                                                            .then(() => {
                                                                Functions.socketSendProposal(this.props.socket, l.received_email);
                                                                this.refreshListCart();
                                                            })
                                                            .then(()=>{
                                                                axios.post(MICRO_DOMAIN+MICRO_EVENT+'/api/event/changeStatus',{id: created})
                                                                .then(() => {})
                                                                .catch(() => {})
                                                            })
                                                            .catch(() => {
                                                                this.setState({isLoading: false});
                                                            })
                                                        })
                                                        .catch(() => {})
                                                    }}>
                                                ACCEPT</button>
                                                :
                                                null
                                            }
                                        </div>
                                        :<div>No Proposal</div>
                                    }
                                    </div>
                                    <div className="uk-width-1-5@m">
                                        {
                                            this.props.auth && !this.props.anonymous
                                            ?
                                            this.renderBtnProposal(l)
                                            :
                                            null
                                        }
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
    render(){
        return (
            <div className="uk-container uk-margin-large">
                <Modal isShow={this.state.isModalSaleProposal}
                    selectedCart={this.state.selectedCart}
                    socket={this.props.socket}
                    onHidden={() => this.setState({isModalSaleProposal: false})}>
                    <SaleProposal onSuccess={() => {
                        this.setState({isModalSaleProposal: false, selectedCart: {}}, () => {
                            this.refreshListCart();
                        })}
                    }/>
                </Modal>
                <Modal isShow={this.state.isModalEditSaleProposal}
                    selectedCart={this.state.selectedCart}
                    socket={this.props.socket}
                    onHidden={() => this.setState({isModalEditSaleProposal: false})}>
                    <SaleEditProposal onSuccess={() => {
                        this.setState({isModalEditSaleProposal: false, selectedCart: {}}, () => {
                            this.refreshListCart();
                        })}
                    }/>
                </Modal>
                <Modal isShow={this.state.isModalExchangeProposal}
                    selectedCart={this.state.selectedCart}
                    socket={this.props.socket}
                    onHidden={() => this.setState({isModalExchangeProposal: false})}>
                    <ExchangeProposal onSuccess={() => {
                        this.setState({isModalExchangeProposal: false, selectedCart: {}}, () => {
                            this.refreshListCart();
                        })}
                    }/>
                </Modal>
                <Modal isShow={this.state.isModalEditExchangeProposal}
                    selectedCart={this.state.selectedCart}
                    socket={this.props.socket}
                    onHidden={() => this.setState({isModalEditExchangeProposal: false})}>
                    <ExchangeEditProposal onSuccess={() => {
                        this.setState({isModalEditExchangeProposal: false, selectedCart: {}}, () => {
                            this.refreshListCart();
                        })}
                    }/>
                </Modal>
                <div className="uk-inline uk-width-1-1 uk-margin">
                    {this.state.isLoading && <Cover/>}
                    {this.state.isLoading && <Spinner/>}
                    {
                        this.state.list.length > 0
                        ? this.renderCartList()
                        : <div className="uk-placeholder uk-text-center">There is no item in cart.</div>
                    }
                </div>
                <div className="uk-clearfix">
                    <div className="uk-float-right">
                        <a className="uk-button uk-button-primary uk-text-normal"
                            onClick={() => this.props.history.push('/')}><i className="fa fa-shopping-cart"></i> Continue Shopping</a>
                        {
                            this.state.list.length > 0 && !this.props.anonymous
                            ?
                            <a className="uk-button uk-button-secondary uk-margin-xsmall-left uk-text-normal"
                                onClick={this.handlerCheckout.bind(this)}><i className="fa fa-money"></i> Checkout</a>
                            : null
                        }
                    </div>
                </div>
                
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const mapDispatchToProps = {
    addCart
}

const ConnectCart = connect(mapStateToProps, mapDispatchToProps)(withStorage(Cart));

export default ConnectCart;