let api = 'http://localhost';
let socket = 'http://localhost';

if(WEBPACK_MODE === 'production'){
    api = 'http://159.203.45.120';
    socket = 'http://159.203.45.120';
}else if(WEBPACK_MODE === 'thuy'){
    api = 'http://159.203.45.120';
    socket = 'http://159.203.45.120';
}

export const SERVER_API = api+':7000/api/';
export const SERVER_DOMAIN = api+':7000/';
export const SERVER_SOCKET = `${socket}:8091`;

export const SERVER_ANONYMOUS = 'http://ipinfo.io';
export const SERVER_IMG = 'http://159.203.45.120:7000/';

export const COUNTRY_LIST = [
    {id: 1, name: 'Canada'},
    {id: 2, name: 'England'},
    {id: 3, name: 'USA'}
];

export const QUESTION_LIST = [
    {id: 1, name: 'What is your parent name ?'},
    {id: 2, name: 'What is your favorite drink ?'},
    {id: 3, name: "What is your pet name ?"}
];

export const RECAPTCHA_SITE_KEY = '6Le1m3cUAAAAALV7RMhLQ35tgU_UlRm8XrIKReis';

export const MICRO_DOMAIN = api+':';
export const MICRO_AUTH = 3001;
export const MICRO_CART = 3002;
export const MICRO_DEVICE = 3003;
export const MICRO_ORDER = 3004;
export const MICRO_EVENT = 3005;
